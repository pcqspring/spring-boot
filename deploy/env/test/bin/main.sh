#!/bin/sh
. ~/.bash_profile
JAVA_HOME=/usr/local/java/jdk1.8.0_311
CLASSPATH=./../res:./../lib/*
echo -------------------------------------------------------------------
echo -------------------------------------------------------------------
echo ------------------- start SpringBootDemo --------------------------
PROVIDER=com.demo.SpringBootDemo
MEM_ARGS="-Xms128m -Xmx128m  -XX:MetaspaceSize=256m -XX:MaxMetaspaceSize=256m"
JAVA_OPTIONS="-Denv.name=SpringBootDemo -Dserver.port=$1"

${JAVA_HOME}/bin/java ${MEM_ARGS} ${JAVA_OPTIONS} -classpath ${CLASSPATH} ${PROVIDER}
