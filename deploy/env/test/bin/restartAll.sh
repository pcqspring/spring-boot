#!/bin/sh

#1.停止
./stop.sh > /dev/null &

#2.判断是否停止，停止则重新启动
while :
do
  PPROC=`ps -ef |grep $USER| grep "SpringBootDemo" | grep -v grep | awk '{print $2}'`
    if [ -n "$PPROC" ]
    then  echo "Also, the application process is stopped and the startup process is waiting!"
    else
      echo "Ready to start the startup process!"
      ./start.sh > /dev/null &
      echo "The background has started the process, please wait for the completion of the program!"
      break
    fi
    sleep 5
done
