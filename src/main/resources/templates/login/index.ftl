<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>首页面</title>
    <#include "../common/head.ftl">
    <script>
        $(function () {
            layui.use('element', function () {
                var element = layui.element;

                //一些事件触发
                element.on('tab(demo)', function (data) {
                });
            });

            layui.use('laydate', function () {
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                    elem: '#ctime' //指定元素
                });
            });

            layui.use('layer', function () {
                var layer = layui.layer;

            });

            $(".layui-nav li").click(function () {
                $(".layui-nav li").each(function (i) {
                    $(this).removeClass("layui-this");
                });
                $(this).addClass("layui-this");
            });


        })

        function showView(url) {
            if(url == '' || url ==undefined){
                return false;
            }
            $.ajax({
                type: "post",
                url: '${request.contextPath}' + url,
                data: {},
                dataType: "html",
                success: function (result) {
                    $("#ifrimeInfo").empty();
                    /* setTimeout(function () {
                         $("#ifrimeInfo").html(result);
                     }, 200);*/
                    $("#ifrimeInfo").html(result);
                }
            });
        }
        /*function getTableList() {
            layui.use('table', function () {
                var table = layui.table;
                //第一个实例
                table.render({
                    elem: '#tableList'
                    , height: 633
                    , url: '${request.contextPath}/index/sysUserList' //数据接口
                    , contentType: 'application/json'
                    , cellMinWidth: 20 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                    , where: {
                        "data": {
                            "userName": $("input[name='userName']").val(),
                            "address": $("input[name='address']").val(),
                            "phone": $("input[name='phone']").val(),
                            "loginName": $("input[name='loginName']").val(),
                            "ctime": $("input[name='ctime']").val()
                        }
                    }
                    , request: {
                        pageName: 'current' //页码的参数名称，默认：page
                        , limitName: 'size' //每页数据量的参数名，默认：limit
                    }
                    , response: {
                        statusName: 'code' //规定数据状态的字段名称，默认：code
                        , statusCode: 200 //规定成功的状态码，默认：0
                    }
                    , method: "post"
                    , parseData: function (result) {  //result 返回值的结果
                        if (result.code == 200) {
                            var data = result.obj;
                            var layuiDate = {
                                "code": result.code,
                                "msg": result.magess,
                                "count": data.total,
                                "data": data.records
                            }; //改成分页的数据
                            return layuiDate;
                        } else {
                            //解析接口状态  //解析提示文本  //解析数据长度  //解析数据列表
                            return {"code": result.code, "msg": result.magess, "count": 0, "data": []} //改成分页的数据
                        }
                    }
                    , page: true //开启分页
                    , cols: [[ // layui 表头
                        {field: '', style: "height:50px", title: '', type: "checkbox", sort: true}
                        , {field: 'userId', title: 'ID', sort: true, fixed: 'left', hide: true, align: "center"}
                        , {field: 'userName', title: '用户名', width: 100, sort: true, align: "center"}
                        , {field: 'sex', title: '性别', width: 80, sort: true, align: "center"}
                        , {field: 'address', title: '城市', align: "center"}
                        , {field: 'phone', title: '手机号', width: 140, align: "center"}
                        , {field: 'loginName', title: '账号', sort: true, align: "center"}
                        , {field: 'ctime', title: '创建时间', width: 200, sort: true, align: "center"}
                        , {field: 'right', title: '操作', width: 260, toolbar: '#barTableList', align: "center"}
                    ]]
                });

            });
        }*/

        //getTableList();

        function logOut() {
            // /logOut
            layer.confirm('确定退出登陆吗?', function (index) {
                //do something
                layer.close(index);
                $.ajax({
                    type: "post",
                    url: "${request.contextPath}/logOut",
                    data: {},
                    dataType: "json",
                    success: function (result) {
                        if (result.code == 200) {
                            layer.msg(result.magess);
                            setTimeout(function () {
                                window.location.reload();
                            }, 700);
                        } else {
                            layer.msg(result.magess);
                        }
                    }

                });
            });
        }

        $(function (){
            layui.use(['element', 'tree', 'util', 'jquery'], function () {
                var element = layui.element;
                var tree = layui.tree
                    , layer = layui.layer
                    , util = layui.util

                var layuiTree;
                //获取tree
                $.ajax({
                    type: "post",
                    url: "${request.contextPath}/authority/getUserAppTreeList", //这里调用的 登录注册页面的 添加 路径
                    data: {},
                    async: false,
                    dataType: "json",
                    success: function (result) {
                        if (result.code == 200) {
                            layuiTree = result.obj;
                        } else {
                            layer.msg(result.magess);
                        }
                    }
                });

                // 递归渲染树形结构
                function renderTree(data,idx, $parent) {
          /*          console.log(data);
                    console.log(data.length);
                    console.log("=======");*/
                    for (var i = 0; i < data.length; i++) {
                        var item = data[i];
                        var $li = $('<li class="layui-nav-item custom-collapse layui-colla-item" style=""></li>');
                        if(idx == 1){ //顶级目录 默认打开
                            $li.addClass("layui-nav-itemed");
                        }
                        var $a = $("<a href=\"javascript:showView('"+item.url+"');\" >" + item.title + "</a>");
                        $li.append($a);

                        if (item.children && item.children.length > 0) {
                            var $dl = $('<dl class="layui-nav-child"  ></dl>');
                            $li.append($dl);
                            renderTree(item.children,0, $dl);
                        }

                        $parent.append($li);
                    }
                }

                // 渲染树形组件
                function renderCustomTree() {
                    var $tree = $('#sysAppTree');
                    renderTree(new Array(layuiTree),1, $tree);

                    // 初始化Layui的nav组件
                    element.render('nav', 'treeNav');
                }

                // 初始化
                renderCustomTree();
            });

        });
    </script>
    <script type="text/html" id="barTableList">
        <a class="layui-btn layui-btn-normal layui-btn-xs sc-a" lay-event="detail">查看</a>
        <a class="layui-btn layui-btn-normal layui-btn-xs sc-a" lay-event="edit">编辑</a>
        <a class="layui-btn layui-btn-danger layui-btn-xs sc-a" lay-event="del">删除</a>
    </script>
    <style>
        .custom-collapse .layui-colla-item {
            margin-bottom: 10px;
            border: none;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
            /*background-color: #fff;*/
            transition: all 0.3s ease;
        }
        .custom-collapse .layui-colla-item:hover {
            transform: translateY(-2px);
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.2);
        }
        /*   tree 样式  */
        /* 自定义样式 */
        .custom-tree .layui-tree-entry {
            position: relative;
            padding-left: 20px;
        }
        .custom-tree .layui-tree-entry .layui-tree-main {
            position: relative;
            padding-left: 20px;
            cursor: pointer;
        }
        .custom-tree .layui-tree-entry .layui-tree-main:before {
            content: "";
            position: absolute;
            left: 0;
            top: 50%;
            transform: translateY(-50%);
            width: 6px;
            height: 6px;
            border-radius: 50%;
            background-color: #1E9FFF;
        }
        .custom-tree .layui-tree-entry .layui-tree-main:after {
            content: "";
            position: absolute;
            left: 5px;
            top: 50%;
            transform: translateY(-50%);
            width: 1px;
            height: 100%;
            background-color: #1E9FFF;
        }
        .custom-tree .layui-tree-entry:first-child .layui-tree-main:before {
            display: none;
        }
        .custom-tree .layui-tree-entry:last-child .layui-tree-main:after {
            display: none;
        }
        .custom-tree .layui-tree-entry .layui-tree-main .layui-icon {
            position: absolute;
            left: -20px;
            top: 50%;
            transform: translateY(-50%);
            font-size: 12px;
            color: #999;
        }
        .custom-tree .layui-tree-entry .layui-tree-main:hover {
            background-color: #f2f2f2;
        }

        .layui-nav-tree .layui-nav-item a {
            position: relative;
            height: 46px;
            line-height: 40px;
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap;
        }
        /*-------------*/
        /* 自定义样式 */
        .dark-search {
            display: flex;
            align-items: center;
        }
        .dark-search input[type="text"] {
            width: 74%;
            min-width: 308px;
            position: absolute;
            right: 90px;
            top: -18px;
            height: 36px;
            padding: 6px 12px;
            font-size: 14px;
            border: none;
            border-top-left-radius: 40px;
            border-bottom-left-radius: 40px;
            background-color: #343a40;
            color: #fff;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        }
        .dark-search button {
            width: 18%;
            min-width: 74px;
            position: absolute;
            right: 16px;
            top: -18px;
            height: 36px;
            padding: 0 16px;
            font-size: 14px;
            border: none;
            /*border-radius: 4px;*/
            border-top-right-radius: 40px;
            border-bottom-right-radius: 40px;
            background-color: #495057;
            color: #fff;
            cursor: pointer;
            transition: background-color 0.3s ease;
        }
        .dark-search button:hover {
            background-color: #343a40;
        }

        .ul-title-left {
            width: 57%;
            min-width: 908px;
            padding:  0px 0px 0px 17%;
            display: inline-block;
            border-top-right-radius: 0px;
            border-bottom-right-radius: 0px
        }

        .ul-title-right {
            width: 20%;
            min-width: 304px;
            display: inline-block;
            position: absolute;
            top: 0px;
            border-top-left-radius: 0px;
            border-bottom-left-radius: 0px
        }
        .icon-select{
            left: -6px;
            position: relative;
        }


    </style>
</head>
<!-- //Head -->

<!-- Body -->

<body>
<div class="body-div">
    <div class="body-div-top" style="background-color: #393D49;">
        <ul class="layui-nav ul-title-left"  lay-filter="">
            <li class="layui-nav-item layui-this"><a href="${request.contextPath}/index">首页面</a></li>
            <#if sysRoleList??>
                <#list sysRoleList as sysRole>
                    <#if sysRole.roleId?default('') ==  administratorsId?default('') >
                        <li class="layui-nav-item"><a href="${request.contextPath}/authority/index">权限配置</a></li>
                    </#if>
                </#list>
            </#if>
            <li class="layui-nav-item"><a href="#">大数据</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">解决方案</a>
                <dl class="layui-nav-child"> <!-- 二级菜单 -->
                    <dd><a href="">移动模块</a></dd>
                    <dd><a href="">后台模版</a></dd>
                    <dd><a href="">电商平台</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a href="#">社区</a></li>
        </ul>
        <div class="layui-nav dark-search" style="display: inline-block;width: 22%; ">
                <input type="text" placeholder="请输入关键字">
                <button type="button"><i class="layui-icon layui-icon-search icon-select"></i>搜索</button>
        </div>
        <ul class="layui-nav ul-title-right" >
            <li class="layui-nav-item">
                <a href="#">控制台<span class="layui-badge">9</span></a>
            </li>
            <li class="layui-nav-item">
                <a href="javascript:;">${userName?default('')}<span class="layui-badge-dot"></span></a>
                <dl class="layui-nav-child"> <!-- 二级菜单 -->
                    <dd><a href="">个人信息</a></dd>
                    <dd><a href="">我的收藏</a></dd>
                    <dd><a href="#" onclick="logOut()">退出登录 <i class="layui-icon layui-icon-logout"></i></a></dd>
                </dl>
            </li>
        </ul>
    </div>
    <div class="body-div-bottom">
        <div class="body-div-left">
            <div class="layui-panel" style="height: 100%;">
<#--                <div id="sysAppTree" class="demo-tree custom-tree"></div>-->
                <#--<div class="layui-side layui-bg-gray custom-tree" style="width: 200px;position: absolute;">
                    <div class="layui-side-scroll">
                        <ul class="layui-nav layui-nav-tree" lay-filter="treeNav" id="sysAppTree">
                        </ul>
                    </div>
                </div>-->
                <div class="custom-tree">
                    <ul class="layui-nav layui-nav-tree" style="width: auto;" lay-filter="treeNav" id="sysAppTree"></ul>
                </div>
               <#-- <form class="layui-form" id="addForm"> <!-- 提示：如果你不想用form，你可以换成div等任何一个普通元素 &ndash;&gt;
                    <div style="padding: 20px;" id="sysAppTree"></div>
                    <!-- 更多表单结构排版请移步文档左侧【页面元素-表单】一项阅览 &ndash;&gt;
                </form>-->
                <#--<ul class="layui-menu" id="docDemoMenu1">
                    <li class="layui-menu-item-group layui-menu-item-down" lay-options="{type: 'group'}">
                        <div class="layui-menu-body-title">
                            首页面 <i class="layui-icon layui-icon-up"></i>
                        </div>
                        <ul>
                            <li lay-options="{id: 103}">
                                <div class="layui-menu-item-checked"  onclick="showView('')">首页面 1-1</div>
                            </li>
                            <li class="layui-menu-item-group" lay-options="{type: 'group'}">
                                <div class="layui-menu-body-title">
                                    首页面 2 <i class="layui-icon layui-icon-up"></i>
                                </div>
                                <ul>
                                    <li class="layui-menu-body-title">
                                        <div class="layui-menu-body-title" onclick="showView('')">首页面 2-1</div>
                                    </li>
                                    <li><div class="layui-menu-body-title"  onclick="showView('')">首页面 2-2</div></li>
                                    <li class="layui-menu-item-group" lay-options="{type: 'group'}">
                                        <div class="layui-menu-body-title">
                                            首页面 2-3 <i class="layui-icon layui-icon-up"></i>
                                        </div>
                                        <ul>
                                            <li class="layui-menu-body-title">
                                                <div class="layui-menu-body-title"  onclick="showView('')">首页面 2-3-1</div>
                                            </li>
                                            <li><div class="layui-menu-body-title"  onclick="showView('')">首页面 2-3-2</div></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><div class="layui-menu-body-title"  onclick="showView('')">首页面 3</div></li>
                        </ul>
                    </li>
                </ul>-->
            </div>
        </div>
        <div id="ifrimeInfo" class="body-div-right">
            <#--            background-image: url(/static/img/4494.jpg)-->
            <div style="position: absolute;height:100%;width: 100%; text-align: center;">
                <h1 style="position:absolute;top: 380px;left: 36%;color: #999">Welcome to the home page...</h1>
            </div>
        </div>
    </div>

</div>

</body>
<!-- //Body -->

</html>