<!--
	Author: W3layouts
	Author URL: http://w3layouts.com
-->
<!DOCTYPE html>
<html lang="zxx">
<!-- Head -->
<head>
    <title>更新密码</title>
    <#include "../common/head.ftl">
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;500;700&display=swap" rel="stylesheet">

    <style>
        /*--
    Author: W3layouts
    Author URL: http://w3layouts.com
--*/

        /* reset code */
        html {
            scroll-behavior: smooth;
        }

        body,
        html {
            margin: 0;
            padding: 0;
            color: #585858;
        }

        * {
            box-sizing: border-box;
            font-family: 'Noto Sans JP', sans-serif;
        }

        /*  wrapper */
        .wrapper {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }

        @media (min-width: 576px) {
            .wrapper {
                max-width: 540px;
            }
        }

        @media (min-width: 768px) {
            .wrapper {
                max-width: 720px;
            }
        }

        @media (min-width: 992px) {
            .wrapper {
                max-width: 960px;
            }
        }

        @media (min-width: 1200px) {
            .wrapper {
                max-width: 1140px;
            }
        }

        /*  /wrapper */

        .d-grid {
            display: grid;
        }

        button,
        input,
        select {
            -webkit-appearance: none;
            outline: none;
        }

        button,
        .btn,
        select {
            cursor: pointer;
        }

        a {
            text-decoration: none;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        p,
        ul,
        ol {
            margin: 0;
            padding: 0;
        }

        body {
            background: #f1f1f1;
            margin: 0;
            padding: 0;
        }

        form,
        fieldset {
            border: 0;
            padding: 0;
            margin: 0;
        }

        body a:hover {
            text-decoration: none;
        }

        .clearfix {
            clear: both;
        }

        /* content */

        /*
          Responsive form elements
          Flexbox layout
        */

        /*/////////////// GLOBAL STYLES ////////////////////*/

        body {
            font-family: 'Noto Sans JP', sans-serif;
        }

        .main {
            background: url(${request.contextPath!}/static/img/bg.jpg) no-repeat bottom;
            background-size: cover;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            -ms-background-size: cover;
            min-height: 100vh;
            padding: 2em 0;
            position: relative;
            z-index: 1;
            justify-content: center;
            display: grid;
            align-items: center;
        }

        .main:before {
            position: absolute;
            content: '';
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            background: rgba(0, 0, 0, 0.6);
            z-index: -1;
        }

        .text-center {
            text-align: center;
        }

        /*/////////////// FONT STYLES ////////////////////*/

        .content-w3ls {
            margin: 2em auto;
            padding: 2em 2em;
        }

        .content-w3ls img {
            margin-bottom: 2em;
            border-radius: 50%;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            -ms-border-radius: 50%;
            -o-border-radius: 50%;
            border: 4px solid rgb(255 255 255 / 9%);
            display: inline-block;
            width: 64px;
            height: 64px;
            background: #fff;
            border-radius: 50%;
            padding: 14px;
        }

        .logo h1 a {
            font-size: 42px;
            color: #fff;
            text-transform: capitalize;
            font-weight: 700;
        }

        /*/////////////// FORM STYLES ////////////////////*/
        .wthree-field {
            margin-bottom: 14px;
        }

        .wthree-field input {
            padding: 14px 30px;
            font-size: 16px;
            color: #999;
            letter-spacing: 0.5px;
            border: 1px solid #e1e1e1;
            background: #fff;
            box-sizing: border-box;
            font-family: 'Noto Sans JP', sans-serif;
            width: 100%;
            border-radius: 35px;
            -webkit-border-radius: 35px;
            -moz-border-radius: 35px;
            -ms-border-radius: 35px;
            -o-border-radius: 35px;
        }

        ::-webkit-input-placeholder { /* Edge */
            color: #999;
        }

        :-ms-input-placeholder { /* Internet Explorer 10-11 */
            color: #999;
        }

        ::placeholder {
            color: #999;
        }

        .wthree-field button.btn {
            background: #EA4C89;
            border: none;
            color: #fff;
            padding: 14px 14px;
            text-transform: uppercase;
            font-family: 'Noto Sans JP', sans-serif;
            font-size: 16px;
            width: 100%;
            letter-spacing: 1px;
            outline: none;
            cursor: pointer;
            transition: 0.5s all;
            -webkit-transition: 0.5s all;
            -moz-transition: 0.5s all;
            -o-transition: 0.5s all;
            -ms-transition: 0.5s all;
            border-radius: 35px;
            -webkit-border-radius: 35px;
            -moz-border-radius: 35px;
            -ms-border-radius: 35px;
            -o-border-radius: 35px;

        }

        .wthree-field button.btn:hover {
            background: #de3d7b;
        }

        .login-bottom {
            margin-top: 3em;
        }

        .login-bottom a {
            font-size: 16px;
            color: #fff;
            font-weight: normal;
            letter-spacing: 1px;
            padding: 0 5px;
            text-transform: capitalize;
        }

        .copyright {
            padding: 0 20px;
        }

        .copyright p {

            color: #fff;
            font-size: 16px;
            line-height: 26px;
            text-align: center;
            letter-spacing: 1px;

        }

        .copyright p a {
            color: #fff;
        }

        .copyright p a:hover, .login-bottom a:hover {
            color: #EA4C89;
        }

        /* -- Responsive code -- */
        @media screen and (max-width: 1280px) {
        }

        @media screen and (max-width: 1080px) {
        }

        @media screen and (max-width: 900px) {
        }

        @media screen and (max-width: 800px) {
        }

        @media screen and (max-width: 768px) {
            .logo h1 a {
                font-size: 30px;
            }
        }

        @media screen and (max-width: 668px) {

        }

        @media screen and (max-width: 600px) {
        }

        @media screen and (max-width: 568px) {
        }

        @media screen and (max-width: 480px) {

        }

        @media screen and (max-width: 415px) {
            .logo {
                margin-bottom: 10px;
            }

            .content-w3ls {
                padding: 2em 2em;
            }
        }

        @media screen and (max-width: 384px) {
            .content-w3ls {
                padding: 1em 1em;
                margin: 1em auto;
            }

            .copyright p {
                letter-spacing: .5px;
            }
        }

        @media screen and (max-width: 375px) {

        }

        @media screen and (max-width: 320px) {

        }

        /* -- //Responsive code -- */
    </style>
    <script>
        layui.use('layer', function () {
            var layer = layui.layer;
        });
        //Demo
        layui.use('form', function () {
            var form = layui.form;
        });

        function registerSave() {
            var loginName = $("#loginName").val();
            if (loginName == undefined || loginName == "") {
                layer.msg("账号不能为空！");
                return false;
            }
            var password = $("#password").val();
            if (password == undefined || password == "") {
                layer.msg("密码不能为空！");
                return false;
            }
            if (password.length < 6) {
                layer.msg("密码不能小于6位！");
                return false;
            }
            var password2 = $("#password2").val();
            if (password2 == undefined || password2 == "") {
                layer.msg("确认密码不能为空！");
                return false;
            }
            if (password != password2) {
                layer.msg("密码和确认密码不一致！");
                return false;
            }
            var phone = $("#phone").val();
            if (phone == undefined || phone == "") {
                layer.msg("电话不能为空！");
                return false;
            }
            var emptyLoginName = loginName.indexOf(" ");
            if (emptyLoginName != -1) {
                layer.msg("账号不能有空格！");
                return false;
            }
            var emptyPassword = password.indexOf(" ");
            if (emptyPassword != -1) {
                layer.msg("密码不能有空格！");
                return false;
            }
            var emptyPhone = phone.indexOf(" ");
            if (emptyPhone != -1) {
                layer.msg("手机号不能有空格！");
                return false;
            }
        /*    var sex = 0;
            $("input[name='sex']").each(function (i) {
                if ($(this).attr("checked") == "checked") {
                    sex = $(this).val();
                }
            });*/
            $.ajax({
                type: "post",
                url: "${request.contextPath}/retrieveSave",
                data: {
                    "loginName": loginName,
                    "password": password,
                    "phone": phone
                    // ,"sex": sex
                },
                dataType: "json",
                success: function (result) {
                    console.log(result);
                    if (result.code == 200) {
                        layer.msg(result.magess);
                        setTimeout(function () {
                            window.location.href = "${request.contextPath}/login";
                        }, 1000);
                    } else {
                        layer.msg(result.magess);
                    }
                }

            });
        }

    </script>
</head>
<!-- //Head -->
<!-- Body -->
<body>
<form class="layui-form"> <!-- 提示：如果你不想用form，你可以换成div等任何一个普通元素 -->
    <section class="main">
        <div class="logo text-center">
            <h1><a href="index.html"> 更新密码 </a></h1>
        </div>
        <div class="content-w3ls text-center">
            <img src="${request.contextPath!}/static/img/admin.png" alt="" class="img-responsive">
            <form method="post">
                <div class="wthree-field">
                    <input name="loginName" id="loginName" type="text" value="" placeholder="请输入您的账户">
                </div>
                <div class="wthree-field">
                    <input name="password" id="password" type="password" placeholder="请输入新密码">
                </div>
                <div class="wthree-field">
                    <input name="password2" id="password2" type="password" placeholder="请输入确认新密码">
                </div>
                <div class="wthree-field">
                    <input name="phone" id="phone" type="text" placeholder="请输入绑定的手机号码">
                </div>
                <div class="wthree-field">
                    <button type="button" onclick="registerSave()" class="btn">修改密码</button>
                </div>


            </form>
        </div>
        <div class="copyright">
            <p>© 2022 这是彭朝群的项目 使用的 JDK1.8版本 | SpringBoot框架 | MyBatis-Plus框架 | Swagger |
                Maven工程 | Freemarker模板引擎 | layui组件 | mysql数据库 | Redis缓存数据库 技术栈</p>
        </div>
    </section>
</form>
</body>
<!-- //Body -->

</html>