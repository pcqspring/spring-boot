<!--统一引入公共样式-->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<link rel="stylesheet" href="${request.contextPath!}/static/css/style.css" type="text/css" media="all">
<link rel="stylesheet" href="${request.contextPath!}/static/layui/layui-v2.6.8/layui/css/layui.css">


<script src="${request.contextPath!}/static/jquery/jquery-1.9.0.min.js"></script>
<script src="${request.contextPath!}/static/layui/layui-v2.6.8/layui/layui.js"></script>