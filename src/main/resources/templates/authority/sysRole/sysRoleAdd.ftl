<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>首页面</title>
    <#include "../../common/head.ftl">
    <script>
        /*  $(function () {
              layui.use('element', function () {
                  var element = layui.element;
                  //一些事件触发
                  element.on('tab(demo)', function (data) {
                  });
              });
              //Demo
              layui.use('form', function () {
                  var form = layui.form;
              });
              layui.use('laydate', function () {
                  var laydate = layui.laydate;
                  //执行一个laydate实例
                  laydate.render({
                      elem: '#ctime' //指定元素
                  });
              });
              layui.use('layer', function () {
                  var layer = layui.layer;
              });
              $(".layui-nav li").click(function () {
                  $(".layui-nav li").each(function (i) {
                      $(this).removeClass("layui-this");
                  });
                  $(this).addClass("layui-this");
              });
          })*/
        //添加 保存数据的
        function addSave(parentIndex) {
            var roleName = $("#roleName").val();
            var remarkes = $("#remarkes").val();
            if (roleName == "" || roleName == undefined) {
                layer.msg("请输入角色名称")
                return false;
            }
            if (roleName.indexOf(" ") != -1) {
                layer.msg("角色名称不能有空格！");
                return false;
            }

            $.ajax({
                type: "post",
                url: "${request.contextPath}/authority/sysRoleSave",
                data: {
                    "roleName": roleName,
                    "remarkes": remarkes
                },
                dataType: "json",
                success: function (result) {
                    if (result.code == 200) {
                        parent.layer.close(parentIndex);
                        parent.layer.msg(result.magess);
                        parent.getTableList(); //刷新表单
                    } else {
                        layer.msg(result.magess);
                    }
                }
            });
        }

    </script>
    <style>
        body {
            background-color: #FFFFFF;
        }
    </style>
</head>
<!-- //Head -->

<!-- Body -->

<body>
<form class="layui-form" id="addForm"> <!-- 提示：如果你不想用form，你可以换成div等任何一个普通元素 -->
    <div style="padding-top: 30px;"></div>
    <div class="layui-form-item">
        <label class="layui-form-label form-div-left">角色名称:</label>
        <div class="layui-input-block form-div-left">
            <input type="text" id="roleName" name="roleName" placeholder="请输入角色名称" autocomplete="off"
                   class="layui-input form-div-width">
        </div>
    </div>
    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label form-div-left">备注描述:</label>
        <div class="layui-input-block form-div-left">
            <textarea id="remarkes" name="remarkes" placeholder="请输入内容" autocomplete="off"
                      class="layui-textarea form-div-width"></textarea>
        </div>
    </div>
    <!-- 更多表单结构排版请移步文档左侧【页面元素-表单】一项阅览 -->
</form>
</div>

</body>
<!-- //Body -->

</html>