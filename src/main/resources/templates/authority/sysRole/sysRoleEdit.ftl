<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>首页面</title>
    <#include "../../common/head.ftl">
    <script>
        $(function () {
            layui.use('element', function () {
                var element = layui.element;
                //一些事件触发
                element.on('tab(demo)', function (data) {
                });
            });
            layui.use('laydate', function () {
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                    elem: '#ctime' //指定元素
                });
            });
            layui.use('layer', function () {
                var layer = layui.layer;
            });

        })

        //添加 保存数据的
        function editSave(parentIndex) {
            var roleName = $("#roleName").val();
            var remarkes = $("#remarkes").val();
            if (roleName == "" || roleName == undefined) {
                layer.msg("请输入角色名称")
                return false;
            }
            if (roleName.indexOf(" ") != -1) {
                layer.msg("角色名称不能有空格！");
                return false;
            }
            $.ajax({
                type: "post",
                url: "${request.contextPath}/authority/sysRoleEditSave",
                data: {
                    "roleId": $("#roleId").val(),
                    "roleName": roleName,
                    "remarkes": remarkes
                },
                dataType: "json",
                success: function (result) {
                    if (result.code == 200) {
                        parent.layer.close(parentIndex);
                        parent.layer.msg(result.magess);
                        parent.getTableList(); //刷新表单
                    } else {
                        layer.msg(result.magess);
                    }
                }
            });
        }

    </script>

    <style>
        body {
            background-color: #FFFFFF;
        }

        .pcq-div-text {
            position: relative;
            padding: 9px 15px;
            line-height: 20px;
            font-size: 16px;
            color: #000;
            text-align: left;
        }

        .pcq-h2-title {
            font-size: 18px;
            font-weight: 600;
            color: #2e2e2e;
        }

        .pcq-nav-role {
            font: 12px verdana, arial, sans-serif; /* 设置文字大小和字体样式 */
            width: 100%;
        }

        .pcq-nav-role, .pcq-nav-role li {
            list-style: none; /* 将默认的列表符号去掉 */
            padding: 0; /* 将默认的内边距去掉 */
            margin: 0; /* 将默认的外边距去掉 */
            float: left; /* 往左浮动 */
            display: block;
        }

        .pcq-nav-role li a {
            display: inline-block; /* 将链接设为块级元素 */
            width: 140px; /* 设置宽度 */
            height: 30px; /* 设置高度 */
            margin-bottom: 16px;
            padding: 0 7px;
            margin-left: 16px;
            line-height: 30px; /* 设置行高，将行高和高度设置同一个值，可以让单行文本垂直居中 */
            text-align: center; /* 居中对齐文字 */
            background-color: rgb(235, 242, 247);
            color: #507999;
            text-decoration: none; /* 去掉下划线 */
            border: 1px solid #ebf2f7;
            border-radius: 4px;
        }

        .pcq-nav-role li a:hover {
            background-color: rgb(224, 233, 240); /* 变换背景色 */
        }

        .form-div-left {
            left: 100px;
        }

    </style>
</head>
<!-- //Head -->

<!-- Body -->

<body>
<div class="layui-collapse" style="padding:40px;height: 100%">
    <div class="layui-colla-item">
        <h2 class="layui-colla-title pcq-h2-title">角色基本信息</h2>
        <div class="layui-colla-content layui-show">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md6">
                    <div class="layui-panel">
                        <div style="padding:20px; ">
                            <form class="layui-form" id="addForm"> <!-- 提示：如果你不想用form，你可以换成div等任何一个普通元素 -->
                                <input hidden id="roleId" name="roleId" value="${sysRole.roleId!""}"/>
                                <div style="padding-top: 30px;"></div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label form-div-left">角色名称:</label>
                                    <div class="layui-input-block form-div-left">
                                        <input type="text" id="roleName" name="roleName" placeholder="请输入路径序号" autocomplete="off"
                                               value="${sysRole.roleName!""}" class="layui-input form-div-width">
                                    </div>
                                </div>
                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label form-div-left">备注描述:</label>
                                    <div class="layui-input-block form-div-left">
                                        <textarea id="remarkes" name="remarkes" placeholder="请输入内容" autocomplete="off"
                                            class="layui-textarea form-div-width">${sysRole.remarkes!""}</textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="layui-colla-item">
        <h2 class="layui-colla-title pcq-h2-title">当前拥有的路径</h2>
        <div class="layui-colla-content">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md6">
                    <div class="layui-panel">
                        <#if roleIndex == 1>
                            <div class="layui-card" style="margin-bottom: -2px;">
                                <div class="layui-card-header" style="padding: 0px 15px;overflow:hidden;height: 100%">
                                    <ul class="pcq-nav-role" id="thisRole">
                                       <#-- <#if sysRole.sysApp??>
                                            <#list sysRole.sysApp as app>
                                                <li class="layui-nav-item" >
                                                    <a title="${app.appName!''}" style="background-color: rgb(80, 121, 153);color: #fff;">${app.appPath!''}</a>
                                                </li>
                                            </#list>
                                        </#if>-->
                                    </ul>
                                </div>
                                <div class="layui-card-body" style="overflow:hidden">
                                    <ul class="pcq-nav-role">
                                        <#if sysAppList??>
                                            <#list sysAppList as app>
                                                <li title="<#if app.appPath?length gt 0>${app.appPath!''}<#else >无路径</#if>" class="layui-nav-item">
                                                    <a  href="javascript:addRoleApp('${app.appId!''}','${sysRole.roleId!""}')">${app.appName!''}</a>
                                                </li>
                                            </#list>
                                        </#if>
                                    </ul>
                                </div>
                            </div>
                        <#else>
                            <div style="padding:20px;overflow:hidden;height: 100%">
                                <ul class="pcq-nav-role">
                                    <#if sysRole.sysApp??>
                                        <#list sysRole.sysApp as app>
                                            <li title="<#if app.appPath?length gt 0>${app.appPath!''}<#else >无路径</#if>" class="layui-nav-item" >
                                                <a  style="background-color: rgb(80, 121, 153);color: #fff;">${app.appName!''}</a>
                                            </li>
                                        </#list>
                                    </#if>
                                </ul>
                            </div>
                        </#if>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<!-- //Body -->

</html>
<#-- 当前路径 角色  添加 删除  等操作js-->
<script>
    // 给当前路径 添加角色
    function addRoleApp(appId, roleId) {
        $.ajax({
            type: "post",
            url: "${request.contextPath}/authority/addSysAppRole",
            data: {
                "appId": appId,
                "roleId": roleId
            },
            dataType: "json",
            success: function (result) {
                if (result.code == 200) {
                    var data = result.obj;
                    $("#thisRole").empty();
                    for (var i = 0; i < data.length; i++) {
                        var liSon = $("<li class=\"layui-nav-item\"></li>");
                        var titles= data[i].appPath;
                        if (titles == "" || titles == undefined) titles = "无路径";
                        var aSon = $("<a title='"+titles+"' style=\"margin-top:10px;background-color: rgb(80, 121, 153);color: #fff;\">" + data[i].appName + "</a>");
                        var aiSon = $("<i class=\"layui-icon layui-icon-close\" onclick=\"delRoleApp('${sysRole.roleId!""}','" + data[i].appId + "')\"   style=\"float: right;\"></i>");
                        aSon.append(aiSon);
                        liSon.append(aSon);
                        $("#thisRole").append(liSon);
                    }
                    layer.msg(result.magess);
                } else {
                    layer.msg(result.magess);
                }
            }
        });
    }

    //给当前路径 删除 角色
    function delRoleApp(roleId, appId) {

        $.ajax({
            type: "post",
            url: "${request.contextPath}/authority/delSysAppRole",
            data: {
                "appId": appId,
                "roleId": roleId
            },
            dataType: "json",
            success: function (result) {
                if (result.code == 200) {
                    var data = result.obj;
                    $("#thisRole").empty();
                    for (var i = 0; i < data.length; i++) {
                        var liSon = $("<li class=\"layui-nav-item\"></li>");
                        var titles= data[i].appPath;
                        if (titles == "" || titles == undefined) titles = "无路径";
                        var aSon = $("<a title='"+titles+"' style=\"margin-top:10px;background-color: rgb(80, 121, 153);color: #fff;\">" + data[i].appName + "</a>");
                        var aiSon = $("<i class=\"layui-icon layui-icon-close\" onclick=\"delRoleApp('${sysRole.roleId!""}','" + data[i].appId + "')\"   style=\"float: right;\"></i>");
                        aSon.append(aiSon);
                        liSon.append(aSon);
                        $("#thisRole").append(liSon);
                    }
                    $("#thisRole").append();
                    layer.msg(result.magess);
                } else {
                    layer.msg(result.magess);
                }
            }
        });
    }


    //进入页面是进行 初始化显示自己的 角色
    function showUserRole() {
        $.ajax({
            type: "post",
            url: "${request.contextPath}/authority/findSysAppListTwo",
            data: {
                "roleId": "${sysRole.roleId!""}"
            },
            dataType: "json",
            success: function (result) {
                if (result.code == 200) {
                    var data = result.obj;
                    $("#thisRole").empty();
                    for (var i = 0; i < data.length; i++) {
                        var liSon = $("<li class=\"layui-nav-item\"></li>");
                        var titles= data[i].appPath;
                        if (titles == "" || titles == undefined) titles = "无路径";
                        var aSon = $("<a title='"+titles+"' style=\"margin-top:10px;background-color: rgb(80, 121, 153);color: #fff;\">" + data[i].appName + "</a>");
                        var aiSon = $("<i class=\"layui-icon layui-icon-close\" onclick=\"delRoleApp('${sysRole.roleId!""}','" + data[i].appId + "')\"   style=\"float: right;\"></i>");
                        aSon.append(aiSon);
                        liSon.append(aSon);
                        $("#thisRole").append(liSon);
                    }
                    $("#thisRole").append();
                } else {
                    layer.msg(result.magess);
                }
            }
        });
    }
    showUserRole();
</script>