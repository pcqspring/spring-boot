<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>首页面</title>
    <#include "../../common/head.ftl">
    <script>
        $(function () {
            layui.use('element', function () {
                var element = layui.element;
                //一些事件触发
                element.on('tab(demo)', function (data) {
                });
            });
            layui.use('laydate', function () {
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                    elem: '#ctime' //指定元素
                });
            });
            layui.use('layer', function () {
                var layer = layui.layer;
            });
            <#if sysApp.sysRole??>
                <#if sysApp.sysRole?size == 0>
                    layer.msg("当前路径,未查询到角色信息!");
                </#if>
                <#else>
                layer.msg("当前路径,未查询到角色信息!");
            </#if>
        })
    </script>
    <style>
        body {
            background-color: #FFFFFF;
        }

        .pcq-div-text {
            position: relative;
            padding: 9px 15px;
            line-height: 20px;
            font-size: 16px;
            color: #000;
            text-align: left;
        }

        .pcq-h2-title {
            font-size: 18px;
            font-weight: 600;
            color: #2e2e2e;
        }

        .pcq-nav-role {
            font: 12px verdana, arial, sans-serif; /* 设置文字大小和字体样式 */
            width: 100%;
        }

        .pcq-nav-role, .pcq-nav-role li {
            list-style: none; /* 将默认的列表符号去掉 */
            padding: 0; /* 将默认的内边距去掉 */
            margin: 0; /* 将默认的外边距去掉 */
            float: left; /* 往左浮动 */
            display: block;
        }

        .pcq-nav-role li a {
            display: inline-block; /* 将链接设为块级元素 */
            width: 140px; /* 设置宽度 */
            height: 30px; /* 设置高度 */
            margin-bottom: 16px;
            padding: 0 7px;
            margin-left: 16px;
            line-height: 30px; /* 设置行高，将行高和高度设置同一个值，可以让单行文本垂直居中 */
            text-align: center; /* 居中对齐文字 */
            background-color: rgb(235, 242, 247);
            color: #507999;
            text-decoration: none; /* 去掉下划线 */
            border: 1px solid #ebf2f7;
        }

        .pcq-nav-role li a:hover {
            background-color: rgb(224, 233, 240); /* 变换背景色 */
        }
    </style>
</head>
<!-- //Head -->

<!-- Body -->

<body>
<div class="layui-collapse" style="padding:40px;height: 100%">
    <div class="layui-colla-item">
        <h2 class="layui-colla-title pcq-h2-title">路径基本信息</h2>
        <div class="layui-colla-content layui-show">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md6">
                    <div class="layui-panel">
                        <div style="padding:20px; ">
                            <div class="layui-form-item">
                                <label class="layui-form-label form-div-left">上级路径名称:</label>
                                <div class=" form-div-left pcq-div-text" style="padding-top: 7px;">
                                    ${sysApp.appidParentName!''}
                                </div>
                            </div>
                          <#--  <div class="layui-form-item">
                                <label class="layui-form-label form-div-left">密码:</label>
                                <div class=" form-div-left pcq-div-text">
                                    ${sysApp.password!''}
                                </div>
                            </div>-->
                            <div class="layui-form-item">
                                <label class="layui-form-label form-div-left">当前路径名称:</label>
                                <div class=" form-div-left pcq-div-text">
                                    ${sysApp.appName!''}
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label form-div-left">当前路径:</label>
                                <div class=" form-div-left pcq-div-text">
                                    ${sysApp.appPath!''}
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label form-div-left">路径序号:</label>
                                <div class=" form-div-left pcq-div-text">
                                    ${sysApp.appOrder!''}
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label form-div-left">创建时间:</label>
                                <div class=" form-div-left pcq-div-text">
                                    ${sysApp.ctime!''}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="layui-colla-item">
        <h2 class="layui-colla-title pcq-h2-title">能查看当前路径的角色</h2>
        <div class="layui-colla-content">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md6">
                    <div class="layui-panel">
                        <div style="padding:20px;overflow:hidden;height: 100%">
                            <ul class="pcq-nav-role">
                                <#if sysApp.sysRole??>
                                    <#list sysApp.sysRole as role>
                                        <li class="layui-nav-item" >
                                            <a  style="background-color: rgb(80, 121, 153);color: #fff;">${role.roleName!''}</a>
                                        </li>
                                    </#list>
                                </#if>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<!-- //Body -->

</html>