<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>首页面</title>
    <#include "../../common/head.ftl">
    <script>
        var layuiTree;
        //获取tree
        $.ajax({
            type: "post",
            url: "${request.contextPath}/authority/getAppTreeList", //这里调用的 登录注册页面的 添加 路径
            data: {},
            async: false,
            dataType: "json",
            success: function (result) {
                if (result.code == 200) {
                    layuiTree = result.obj;
                } else {
                    layer.msg(result.magess);
                }
            }
        });
        layui.use(['tree', 'util'], function () {
            var tree = layui.tree
                , layer = layui.layer
                , util = layui.util

            //开启节点操作图标
            tree.render({
                elem: '#sysAppTree'
                , data: [layuiTree]
                , onlyIconControl: true
                //,edit: ['add', 'update', 'del'] //操作节点的图标
                , click: function (obj) {
                    //console.log(obj.data); //得到当前点击的节点数据
                    //console.log(obj.checked); //得到当前节点的展开状态：open、close、normal
                    //console.log(obj.elem); //得到当前节点元素
                    //获取被选中的 span 节点
                    var layuiTreeTxt = $(obj.elem.children("div").children("div").children("span:last"));
                    $("#asdasd").remove();
                    //创建子级节点 并且添加  “对勾 √” 样式
                    var spanSon = $("<span id='asdasd'></span>");
                    spanSon.css("content", ' ');
                    spanSon.css("position", "relative");
                    spanSon.css("display", "inline-block");
                    spanSon.css("width", "11px");
                    spanSon.css("height", "6px");
                    spanSon.css("border-width", "0 0 2px 2px");
                    spanSon.css("overflow", "hidden");
                    spanSon.css("border-color", "#ff0000");
                    spanSon.css("border-style", "solid");
                    spanSon.css("-webkit-transform", "rotate(-45deg)");
                    spanSon.css("transform", "rotate(-45deg)");
                    spanSon.css("left", "10px");
                    spanSon.css("top", "-5px");
                    //追加到被选中节点
                    layuiTreeTxt.append(spanSon);
                    var data = obj.data;
                    $("#treeId").val(data.id);
                    $("#treeName").val(data.title);
                }
            });
        });

        // 把当前页面被选中的数据 返回添加到上级节点种
        function addTreeSave(parentLayer, parentIndex, appidParentId, appidParentName) {
            appidParentId.val($("#treeId").val());
            appidParentName.val($("#treeName").val());
            parentLayer.close(parentIndex);
        }
    </script>
    <style>
        body {
            background-color: #FFFFFF;
        }

        .layui-tree-txt {
            /*display: inline-block;*/
            /*position: relative;*/
            /*height: 12px;*/
            /*width: 12px;*/
            /*border: 2px solid #aaa;*/
            /*border-radius: 50%;*/
        }

        /*
                .layui-tree-txt:active {
                }*/

    </style>
</head>
<!-- //Head -->

<!-- Body -->

<body>
<form class="layui-form" id="addForm"> <!-- 提示：如果你不想用form，你可以换成div等任何一个普通元素 -->
    <div style="padding: 20px;" id="sysAppTree"></div>
    <input hidden/>
    <input type="text" hidden id="treeId">
    <input type="text" hidden id="treeName">
    <!-- 更多表单结构排版请移步文档左侧【页面元素-表单】一项阅览 -->
</form>
</div>

</body>
<!-- //Body -->

</html>