<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>首页面</title>
    <#include "../../common/head.ftl">
    <script>
        /*  $(function () {
              layui.use('element', function () {
                  var element = layui.element;
                  //一些事件触发
                  element.on('tab(demo)', function (data) {
                  });
              });
              //Demo
              layui.use('form', function () {
                  var form = layui.form;
              });
              layui.use('laydate', function () {
                  var laydate = layui.laydate;
                  //执行一个laydate实例
                  laydate.render({
                      elem: '#ctime' //指定元素
                  });
              });
              layui.use('layer', function () {
                  var layer = layui.layer;
              });
              $(".layui-nav li").click(function () {
                  $(".layui-nav li").each(function (i) {
                      $(this).removeClass("layui-this");
                  });
                  $(this).addClass("layui-this");
              });
          })*/
        //添加 保存数据的
        function addSave(parentIndex) {
            var appidParentName = $("#appidParentName").val();
            var appidParent = $("#appidParent").val();
            if (appidParentName == "" || appidParentName == undefined) {
                layer.msg("请选择上一级路径")
                return false;
            }
            var appOrder = $("#appOrder").val();
            if (appOrder == "" || appOrder == undefined) {
                layer.msg("请输入路径序号")
                return false;
            }
            if (appOrder.indexOf(" ") != -1) {
                layer.msg("路径序号不能有空格！");
                return false;
            }
            var appName = $("#appName").val();
            if (appName == "" || appName == undefined) {
                layer.msg("请输入路径名称")
                return false;
            }
            if (appName.indexOf(" ") != -1) {
                layer.msg("路径名称不能有空格！");
                return false;
            }
            var appPath = $("#appPath").val();
        /*
            if (appPath == "" || appPath == undefined) {
                layer.msg("请输入路径地址")
                return false;
            }
            if (appPath.indexOf(" ") != -1) {
                layer.msg("路径地址不能有空格！");
                return false;
            }*/
            $.ajax({
                type: "post",
                url: "${request.contextPath}/authority/sysAppSave",
                data: {
                    "appOrder": appOrder,
                    "appidParentName": appidParentName,
                    "appName": appName,
                    "appPath": appPath,
                    "appidParent": appidParent
                },
                dataType: "json",
                success: function (result) {
                    if (result.code == 200) {
                        parent.layer.close(parentIndex);
                        parent.layer.msg(result.magess);
                        parent.getTableList(); //刷新表单
                    } else {
                        layer.msg(result.magess);
                    }
                }
            });
        }

        layui.use(['tree', 'util'], function () {
            var tree = layui.tree
                , parentLayer = parent.layui.layer
                , util = layui.util
            $("#appidParentName").click(function () {
                parentLayer.open({
                    type: 2,
                    anim: 5,
                    title: "选择第一级路径信息",
                    area: ['300px', '500px'],
                    content: '${request.contextPath}/authority/sysAppTree',
                    btn: ['确定', '取消'],
                    yes: function (index, layero) {
                        //getChildFrame 获取子页面
                        var body = parentLayer.getChildFrame('body', index);
                        //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                        var iframeWin = parent.window[layero.find('iframe')[0]['name']];
                        iframeWin.addTreeSave(parentLayer,index,$("#appidParent"),$("#appidParentName"));//执行iframe页的方法：iframeWin.method();
                        //按钮【按钮一】的回调
                    },
                    btn2: function (index, layero) {
                        //取消按钮 的回调
                        layer.close(index);
                    }
                });
            });
        });

    </script>
    <style>
        body {
            background-color: #FFFFFF;
        }
    </style>
</head>
<!-- //Head -->

<!-- Body -->

<body>
<form class="layui-form" id="addForm"> <!-- 提示：如果你不想用form，你可以换成div等任何一个普通元素 -->
    <div style="padding-top: 30px;"></div>
    <div class="layui-form-item">
        <label class="layui-form-label form-div-left">上一级路径:</label>
        <div class="layui-input-block form-div-left">
            <#--            <div id="appidParent" style="width: 400px;height: 400px"></div>-->
            <input type="text" readonly id="appidParent" name="appidParent"   autocomplete="off"   hidden>
            <input type="text" readonly id="appidParentName" name="appidParentName" placeholder="请选择上一级路径" autocomplete="off"
                   class="layui-input form-div-width">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label form-div-left">路径序号:</label>
        <div class="layui-input-block form-div-left">
            <input type="text" id="appOrder" name="appOrder" placeholder="请输入路径序号" autocomplete="off"
                   class="layui-input form-div-width">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label form-div-left">路径名称:</label>
        <div class="layui-input-block form-div-left">
            <input type="text" id="appName" name="appName" placeholder="请输入请求名称" autocomplete="off"
                   class="layui-input form-div-width">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label form-div-left">路径地址:</label>
        <div class="layui-input-block form-div-left">
            <input type="text" id="appPath" name="appPath" placeholder="请输入请求路径" autocomplete="off"
                   class="layui-input form-div-width">
        </div>
    </div>
    <!-- 更多表单结构排版请移步文档左侧【页面元素-表单】一项阅览 -->
</form>
</div>

</body>
<!-- //Body -->

</html>