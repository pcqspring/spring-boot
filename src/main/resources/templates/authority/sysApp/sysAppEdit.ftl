<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>首页面</title>
    <#include "../../common/head.ftl">
    <script>
        $(function () {
            layui.use('element', function () {
                var element = layui.element;
                //一些事件触发
                element.on('tab(demo)', function (data) {
                });
            });
            layui.use('laydate', function () {
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                    elem: '#ctime' //指定元素
                });
            });
            layui.use('layer', function () {
                var layer = layui.layer;
            });

        })

        //添加 保存数据的
        function editSave(parentIndex) {
            var appidParentName = $("#appidParentName").val();
            var appidParent = $("#appidParent").val();
            if (appidParentName == "" || appidParentName == undefined) {
                layer.msg("请选择上一级路径")
                return false;
            }
            var appOrder = $("#appOrder").val();
            if (appOrder == "" || appOrder == undefined) {
                layer.msg("请输入路径序号")
                return false;
            }
            if (appOrder.indexOf(" ") != -1) {
                layer.msg("路径序号不能有空格！");
                return false;
            }
            var appName = $("#appName").val();
            if (appName == "" || appName == undefined) {
                layer.msg("请输入路径名称")
                return false;
            }
            if (appName.indexOf(" ") != -1) {
                layer.msg("路径名称不能有空格！");
                return false;
            }
            var appPath = $("#appPath").val();
          /*
            if (appPath == "" || appPath == undefined) {
                layer.msg("请输入路径地址")
                return false;
            }
            if (appPath.indexOf(" ") != -1) {
                layer.msg("路径地址不能有空格！");
                return false;
            }*/
            $.ajax({
                type: "post",
                url: "${request.contextPath}/authority/sysAppEditSave",
                data: {
                    "appId": $("#appId").val(),
                    "appOrder": appOrder,
                    "appidParentName": appidParentName,
                    "appName": appName,
                    "appPath": appPath,
                    "appidParent": appidParent
                },
                dataType: "json",
                success: function (result) {
                    if (result.code == 200) {
                        parent.layer.close(parentIndex);
                        parent.layer.msg(result.magess);
                        parent.getTableList(); //刷新表单
                    } else {
                        layer.msg(result.magess);
                    }
                }
            });
        }

        layui.use(['tree', 'util'], function () {
            var tree = layui.tree
                , parentLayer = parent.layui.layer
                , util = layui.util
            $("#appidParentName").click(function () {
                parentLayer.open({
                    type: 2,
                    anim: 5,
                    title: "选择第一级路径信息",
                    area: ['300px', '500px'],
                    content: '${request.contextPath}/authority/sysAppTree',
                    btn: ['确定', '取消'],
                    yes: function (index, layero) {
                        //getChildFrame 获取子页面
                        var body = parentLayer.getChildFrame('body', index);
                        //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                        var iframeWin = parent.window[layero.find('iframe')[0]['name']];
                        iframeWin.addTreeSave(parentLayer,index,$("#appidParent"),$("#appidParentName"));//执行iframe页的方法：iframeWin.method();
                        //按钮【按钮一】的回调
                    },
                    btn2: function (index, layero) {
                        //取消按钮 的回调
                        layer.close(index);
                    }
                });
            });
        });

    </script>

    <style>
        body {
            background-color: #FFFFFF;
        }

        .pcq-div-text {
            position: relative;
            padding: 9px 15px;
            line-height: 20px;
            font-size: 16px;
            color: #000;
            text-align: left;
        }

        .pcq-h2-title {
            font-size: 18px;
            font-weight: 600;
            color: #2e2e2e;
        }

        .pcq-nav-role {
            font: 12px verdana, arial, sans-serif; /* 设置文字大小和字体样式 */
            width: 100%;
        }

        .pcq-nav-role, .pcq-nav-role li {
            list-style: none; /* 将默认的列表符号去掉 */
            padding: 0; /* 将默认的内边距去掉 */
            margin: 0; /* 将默认的外边距去掉 */
            float: left; /* 往左浮动 */
            display: block;
        }

        .pcq-nav-role li a {
            display: inline-block; /* 将链接设为块级元素 */
            width: 140px; /* 设置宽度 */
            height: 30px; /* 设置高度 */
            margin-bottom: 16px;
            padding: 0 7px;
            margin-left: 16px;
            line-height: 30px; /* 设置行高，将行高和高度设置同一个值，可以让单行文本垂直居中 */
            text-align: center; /* 居中对齐文字 */
            background-color: rgb(235, 242, 247);
            color: #507999;
            text-decoration: none; /* 去掉下划线 */
            border: 1px solid #ebf2f7;
            border-radius: 4px;
        }

        .pcq-nav-role li a:hover {
            background-color: rgb(224, 233, 240); /* 变换背景色 */
        }

        .form-div-left {
            left: 100px;
        }

    </style>
</head>
<!-- //Head -->

<!-- Body -->

<body>
<div class="layui-collapse" style="padding:40px;height: 100%">
    <div class="layui-colla-item">
        <h2 class="layui-colla-title pcq-h2-title">路径基本信息</h2>
        <div class="layui-colla-content layui-show">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md6">
                    <div class="layui-panel">
                        <div style="padding:20px; ">
                            <form class="layui-form" id="addForm"> <!-- 提示：如果你不想用form，你可以换成div等任何一个普通元素 -->
                                <input hidden id="appId" name="appId" value="${sysApp.appId!""}"/>
                                <div style="padding-top: 30px;"></div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label form-div-left">上一级路径:</label>
                                    <div class="layui-input-block form-div-left">
                                        <#--            <div id="appidParent" style="width: 400px;height: 400px"></div>-->
                                        <input type="text" readonly id="appidParent" name="appidParent" value="${sysApp.appidParent!""}"  autocomplete="off"   hidden>
                                        <input type="text" readonly id="appidParentName" name="appidParentName" placeholder="请选择上一级路径" autocomplete="off"
                                               value="${sysApp.appidParentName!""}"  class="layui-input form-div-width">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label form-div-left">路径序号:</label>
                                    <div class="layui-input-block form-div-left">
                                        <input type="text" id="appOrder" name="appOrder" placeholder="请输入路径序号" autocomplete="off"
                                               value="${sysApp.appOrder!""}" class="layui-input form-div-width">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label form-div-left">路径名称:</label>
                                    <div class="layui-input-block form-div-left">
                                        <input type="text" id="appName" name="appName" placeholder="请输入请求名称" autocomplete="off"
                                               value="${sysApp.appName!""}" class="layui-input form-div-width">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label form-div-left">路径地址:</label>
                                    <div class="layui-input-block form-div-left">
                                        <input type="text" id="appPath" name="appPath" placeholder="请输入请求路径" autocomplete="off"
                                               value="${sysApp.appPath!""}"   class="layui-input form-div-width">
                                    </div>
                                </div>
                               <#-- <div class="layui-form-item">
                                    <label class="layui-form-label form-div-left">创建时间:</label>
                                    <div class="layui-input-block form-div-left">
                                        <input type="text" readonly  autocomplete="off" value="${sysApp.ctime!""}"
                                               class="layui-input form-div-width">
                                    </div>
                                </div>-->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="layui-colla-item">
        <h2 class="layui-colla-title pcq-h2-title">当前拥有的角色</h2>
        <div class="layui-colla-content">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md6">
                    <div class="layui-panel">
                        <#if roleIndex == 1>
                            <div class="layui-card" style="margin-bottom: -2px;">
                                <div class="layui-card-header" style="padding: 0px 15px;overflow:hidden;height: 100%">
                                    <ul class="pcq-nav-role" id="thisRole">
                                        <#--<#if sysUser.sysRole??>
                                            <#list sysUser.sysRole as role>
                                                <li class="layui-nav-item">
                                                    <a style="margin-top:10px;background-color: rgb(80, 121, 153);color: #fff;">${role.roleName!''}
                                                        <i class="layui-icon layui-icon-close" onclick="delInfoRole()"
                                                           style="float: right;"></i></a>
                                                </li>
                                            </#list>
                                        </#if>-->
                                    </ul>
                                </div>
                                <div class="layui-card-body" style="overflow:hidden">
                                    <ul class="pcq-nav-role">
                                        <#if sysRoleList??>
                                            <#list sysRoleList as role>
                                                <li class="layui-nav-item">
                                                    <a href="javascript:addInfoRole('${sysApp.appId!""}','${role.roleId!''}')">${role.roleName!''}</a>
                                                </li>
                                            </#list>
                                        </#if>
                                    </ul>
                                </div>
                            </div>
                        <#else>
                            <div style="padding:20px;overflow:hidden;height: 100%">
                                <ul class="pcq-nav-role">
                                    <#if sysApp.sysRole??>
                                        <#list sysApp.sysRole as role>
                                            <li class="layui-nav-item">
                                                <a style="background-color: rgb(80, 121, 153);color: #fff;">${role.roleName!''}</a>
                                            </li>
                                        </#list>
                                    </#if>
                                </ul>
                            </div>
                        </#if>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<!-- //Body -->

</html>
<#-- 当前路径 角色  添加 删除  等操作js-->
<script>


    // 给当前路径 添加角色
    function addInfoRole(appId, roleId) {
        $.ajax({
            type: "post",
            url: "${request.contextPath}/authority/addSysRoleApp",
            data: {
                "appId": appId,
                "roleId": roleId
            },
            dataType: "json",
            success: function (result) {
                if (result.code == 200) {
                    var data = result.obj;
                    $("#thisRole").empty();
                    for (var i = 0; i < data.length; i++) {
                        var liSon = $("<li class=\"layui-nav-item\"></li>");
                        var aSon = $("<a style=\"margin-top:10px;background-color: rgb(80, 121, 153);color: #fff;\">" + data[i].roleName + "</a>");
                        var aiSon = $("<i class=\"layui-icon layui-icon-close\" onclick=\"delInfoRole('${sysApp.appId!""}','" + data[i].roleId + "')\"   style=\"float: right;\"></i>");
                        aSon.append(aiSon);
                        liSon.append(aSon);
                        $("#thisRole").append(liSon);
                    }
                    layer.msg(result.magess);
                } else {
                    layer.msg(result.magess);
                }
            }
        });
    }

    //给当前路径 删除 角色
    function delInfoRole(appId, roleId) {

        $.ajax({
            type: "post",
            url: "${request.contextPath}/authority/delSysRoleApp",
            data: {
                "appId": appId,
                "roleId": roleId
            },
            dataType: "json",
            success: function (result) {
                if (result.code == 200) {
                    var data = result.obj;
                    $("#thisRole").empty();
                    for (var i = 0; i < data.length; i++) {
                        var liSon = $("<li class=\"layui-nav-item\"></li>");
                        var aSon = $("<a style=\"margin-top:10px;background-color: rgb(80, 121, 153);color: #fff;\">" + data[i].roleName + "</a>");
                        var aiSon = $("<i class=\"layui-icon layui-icon-close\" onclick=\"delInfoRole('${sysApp.appId!""}','" + data[i].roleId + "')\"   style=\"float: right;\"></i>");
                        aSon.append(aiSon);
                        liSon.append(aSon);
                        $("#thisRole").append(liSon);
                    }
                    $("#thisRole").append();
                    layer.msg(result.magess);
                } else {
                    layer.msg(result.magess);
                }
            }
        });
    }


    //进入页面是进行 初始化显示自己的 角色
    function showUserRole() {
        $.ajax({
            type: "post",
            url: "${request.contextPath}/authority/findSysRoleListTwo",
            data: {
                "appId": "${sysApp.appId!""}"
            },
            dataType: "json",
            success: function (result) {
                if (result.code == 200) {
                    var data = result.obj;
                    $("#thisRole").empty();
                    for (var i = 0; i < data.length; i++) {
                        var liSon = $("<li class=\"layui-nav-item\"></li>");
                        var aSon = $("<a style=\"margin-top:10px;background-color: rgb(80, 121, 153);color: #fff;\">" + data[i].roleName + "</a>");
                        var aiSon = $("<i class=\"layui-icon layui-icon-close\" onclick=\"delInfoRole('${sysApp.appId!""}','" + data[i].roleId + "')\"   style=\"float: right;\"></i>");
                        aSon.append(aiSon);
                        liSon.append(aSon);
                        $("#thisRole").append(liSon);
                    }
                    $("#thisRole").append();
                } else {
                    layer.msg(result.magess);
                }
            }
        });
    }

    showUserRole();
</script>