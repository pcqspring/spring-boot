<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>首页面</title>
    <#include "../../common/head.ftl">
    <script>
        /*  $(function () {
              layui.use('element', function () {
                  var element = layui.element;
                  //一些事件触发
                  element.on('tab(demo)', function (data) {
                  });
              });
              layui.use('laydate', function () {
                  var laydate = layui.laydate;

                  //执行一个laydate实例
                  laydate.render({
                      elem: '#ctime' //指定元素
                  });
              });
              layui.use('layer', function () {
                  var layer = layui.layer;
              });
              $(".layui-nav li").click(function () {
                  $(".layui-nav li").each(function (i) {
                      $(this).removeClass("layui-this");
                  });
                  $(this).addClass("layui-this");
              });
          })*/

        //查询返回 列表页
        function getTableList() {
            layui.use('table', function () {
                var table = layui.table;
                //第一个实例
                table.render({
                    elem: '#tableList'
                    , height: 633
                    , url: '${request.contextPath}/authority/sysUserList' //数据接口
                    , contentType: 'application/json'
                    , cellMinWidth: 20 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                    , where: {
                        "data": {
                            "userName": $("input[name='userName']").val(),
                            "address": $("input[name='address']").val(),
                            "phone": $("input[name='phone']").val(),
                            "loginName": $("input[name='loginName']").val(),
                            "ctime": $("input[name='ctime']").val()
                        }
                    }
                    , request: {
                        pageName: 'current' //页码的参数名称，默认：page
                        , limitName: 'size' //每页数据量的参数名，默认：limit
                    }
                    , response: {
                        statusName: 'code' //规定数据状态的字段名称，默认：code
                        , statusCode: 200 //规定成功的状态码，默认：0
                    }
                    , method: "post"
                    , parseData: function (result) {  //result 返回值的结果
                        if (result.code == 200) {
                            var data = result.obj;
                            var layuiDate = {
                                "code": result.code,
                                "msg": result.magess,
                                "count": data.total,
                                "data": data.records
                            }; //改成分页的数据
                            return layuiDate;
                        } else {
                            //解析接口状态  //解析提示文本  //解析数据长度  //解析数据列表
                            return {"code": result.code, "msg": result.magess, "count": 0, "data": []} //改成分页的数据
                        }
                    }
                    , page: true //开启分页
                    , cols: [[ // layui 表头
                        {field: '', style: "height:50px", title: '', type: "checkbox", sort: true}
                        , {field: 'userId', title: 'ID', sort: true, fixed: 'left', hide: true, align: "center"}
                        , {field: 'userName', title: '用户名', width: 100, sort: true, align: "center"}
                        , {field: 'sex', title: '性别', width: 80, sort: true, align: "center"}
                        , {field: 'address', title: '城市', align: "center"}
                        , {field: 'phone', title: '手机号', width: 140, align: "center"}
                        , {field: 'loginName', title: '账号', sort: true, align: "center"}
                        , {field: 'ctime', title: '创建时间', width: 200, sort: true, align: "center"}
                        , {
                            //表格最后一栏如果是操作栏，可以删除和修改
                            field: 'opt',
                            title: '操作',
                            sort: true,
                            align: "center",
                            templet: function (data) {
                                var id = data.userId;
                                //administratorId
                                return  "<a class=\"layui-btn layui-btn-normal layui-btn-xs sc-a\" onclick=\"showInfo('" + id + "')\" lay-event=\"detail\">查看</a>" +
                                    "<a class=\"layui-btn layui-btn-normal layui-btn-xs sc-a\" onclick=\"editInfo('" + id + "')\" lay-event=\"edit\">编辑</a>" +
                                    "<a class=\"layui-btn layui-btn-danger layui-btn-xs sc-a\" onclick=\"delInfo('" + id + "')\"  lay-event=\"del\">删除</a>";
                            }

                        }
                    ]]
                });

            });
        }

        //进入页面就调用一下获取列表数据
        getTableList();

        //添加
        function addInfo() {
            //eg1
            layer.open({
                type: 2,
                anim: 5,
                title: "添加信息",
                area: ['400px', '550px'],
                content: '${request.contextPath}/authority/sysUserAdd'
                , btn: ['确定', '取消']
                , yes: function (index, layero) {
                    //getChildFrame 获取子页面
                    var body = layer.getChildFrame('body', index);
                    //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                    var iframeWin = window[layero.find('iframe')[0]['name']];
                    iframeWin.addSave(index);//执行iframe页的方法：iframeWin.method();
                    //按钮【按钮一】的回调
                }, btn2: function (index, layero) {
                    //取消按钮 的回调
                    layer.close(index);
                }
            });
        }


        //查看
        function showInfo(id) {
            //eg1
            layer.open({
                type: 2,
                anim: 5,
                title: "添加信息",
                area: ['800px', '650px'],
                content: '${request.contextPath}/authority/sysUserShow?userId=' + id
                , btn: ['关闭']
                , yes: function (index, layero) {
                    //取消按钮 的回调
                    layer.close(index);
                }
            });
        }

        //编辑
        function editInfo(id) {
            //eg1
            layer.open({
                type: 2,
                anim: 5,
                title: "编辑信息",
                area: ['800px', '650px'],
                content: '${request.contextPath}/authority/sysUserEdit?userId=' + id
                , btn: ['保存', '取消']
                , yes: function (index, layero) {
                    //getChildFrame 获取子页面
                    var body = layer.getChildFrame('body', index);
                    //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                    var iframeWin = window[layero.find('iframe')[0]['name']];
                    iframeWin.editSave(index);//执行iframe页的方法：iframeWin.method();
                    //按钮【按钮一】的回调
                }, btn2: function (index, layero) {
                    //取消按钮 的回调
                    layer.close(index);
                }
            });

        }

        //删除
        function delInfo(id) {
            layer.confirm('确定删除吗?', function (index) {
                layer.close(index);
                $.ajax({
                    type: "post",
                    url: "${request.contextPath}/authority/sysUserDel",
                    data: {
                        "userId": id
                    },
                    dataType: "json",
                    success: function (result) {
                        if (result.code == 200) {
                            layer.msg(result.magess);
                            getTableList();
                        } else {
                            layer.msg(result.magess);
                        }
                    }
                });
            });
        }

    </script>
    <style>
    </style>
</head>
<!-- //Head -->

<!-- Body -->

<body>
<input hidden id="administratorId" name="administratorId" value="${administratorId!""}"/>
<div class="body-div">
    <div class="layui-card divs-height">
        <div class="layui-card-header div-text-title">账号信息列表</div>
        <div class="layui-card-body " style="height: 94%;">
            <div class="layui-row layui-col-space15">
                <div>
                    <div class="layui-panel table-title-where">
                        <div style="padding: 10px;">
                            <form class="layui-form"> <!-- 提示：如果你不想用form，你可以换成div等任何一个普通元素 -->
                                <div class="layui-input-inline">
                                    <label class="layui-form-label">用户名</label>
                                    <div class="layui-input-inline">
                                        <input type="text" name="userName" required lay-verify="required"
                                               placeholder="请输入用户名" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-input-inline">
                                    <label class="layui-form-label">手机号</label>
                                    <div class="layui-input-inline">
                                        <input type="text" name="phone" required lay-verify="required"
                                               placeholder="请输入手机号" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-input-inline">
                                    <label class="layui-form-label">账号</label>
                                    <div class="layui-input-inline">
                                        <input type="text" name="loginName" required lay-verify="required"
                                               placeholder="请输入账号" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-input-inline">
                                    <label class="layui-form-label">创建时间</label>
                                    <div class="layui-input-inline">
                                        <input type="text" id="ctime" name="ctime" required
                                               lay-verify="required" placeholder="请选择创建时间" autocomplete="off"
                                               class="layui-input">
                                    </div>
                                </div>
                                <#--  <div class="layui-input-inline">
                                      <label class="layui-form-label">城市</label>
                                      <div class="layui-input-inline">
                                          <select name="address" lay-verify="required">
                                              <option value=""></option>
                                              <option value="0">北京</option>
                                              <option value="1">上海</option>
                                              <option value="2">广州</option>
                                              <option value="3">深圳</option>
                                              <option value="4">杭州</option>
                                          </select>
                                      </div>
                                  </div>-->
                                <div class="table-title-btn">
                                    <button type="button" onclick="getTableList()"
                                            class="layui-btn layui-btn-normal"><i
                                                class="layui-icon layui-icon-search"></i> 搜索
                                    </button>
                                    <button type="button" onclick="addInfo()" class="layui-btn layui-btn-normal"><i
                                                class="layui-icon layui-icon-add-1"></i> 添加
                                    </button>
                                </div>
                                <!-- 更多表单结构排版请移步文档左侧【页面元素-表单】一项阅览 -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div id="dataTable-overflow-y" style="min-height: 633px;">
                <table style="width: 100%" class="layui-table" id="tableList">
                </table>
            </div>
        </div>
    </div>
</div>
</body>
<!-- //Body -->

</html>