<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>首页面</title>
    <#include "../../common/head.ftl">
    <script>
        $(function () {
            layui.use('element', function () {
                var element = layui.element;
                //一些事件触发
                element.on('tab(demo)', function (data) {
                });
            });
            layui.use('laydate', function () {
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                    elem: '#ctime' //指定元素
                });
            });
            layui.use('layer', function () {
                var layer = layui.layer;
            });
        })

        //添加 保存数据的
        function addSave(parentIndex) {
            var loginName = $("#loginName").val();
            if (loginName == undefined || loginName == "") {
                layer.msg("账号不能为空！");
                return false;
            }
            var password = $("#password").val();
            if (password == undefined || password == "") {
                layer.msg("密码不能为空！");
                return false;
            }
            if (password.length < 6) {
                layer.msg("密码不能小于6位！");
                return false;
            }
            var password2 = $("#password2").val();
            if (password2 == undefined || password2 == "") {
                layer.msg("确认密码不能为空！");
                return false;
            }
            if (password != password2) {
                layer.msg("密码和确认密码不一致！");
                return false;
            }
            var userName = $("#userName").val();
            if (userName == undefined || userName == "") {
                layer.msg("姓名不能为空！");
                return false;
            }
            var address = $("#address").val();
            if (address == undefined || address == "") {
                layer.msg("地址不能为空！");
                return false;
            }
            var phone = $("#phone").val();
            if (phone == undefined || phone == "") {
                layer.msg("电话不能为空！");
                return false;
            }
            var emptyLoginName = loginName.indexOf(" ");
            if (emptyLoginName != -1) {
                layer.msg("账号不能有空格！");
                return false;
            }
            var emptyPassword = password.indexOf(" ");
            if (emptyPassword != -1) {
                layer.msg("密码不能有空格！");
                return false;
            }
            //判断真实姓名
            var users5 = /^[a-zA-Z\u4e00-\u9fa5]+$/;
            if (!users5.test(userName)) {
                top.layer.msg("姓名格式不正确！");
                return false;
            }
            var emptyUserName = userName.indexOf(" ");
            if (emptyUserName != -1) {
                layer.msg("姓名不能有空格！");
                return false;
            }
            var emptyAddress = address.indexOf(" ");
            if (emptyAddress != -1) {
                layer.msg("地址不能有空格！");
                return false;
            }
            //判断手机号
            var users4 = /^1[3456789]\d{9}$/;
            if (!users4.test(phone)) {
                top.layer.msg("电话号码格式不正确！");
                return false;
            }
            var emptyPhone = phone.indexOf(" ");
            if (emptyPhone != -1) {
                layer.msg("手机号不能有空格！");
                return false;
            }


            /* //判断身份证
             var users6 = /^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/;
             if(!users6.test(sjcodes)){
                 top.layer.msg( "身份证号格式不正确！");
                 return false;
             }*/


            var sex =$('input[name="sex"]:checked').val();
            /*var sex = 0;
            $("input[name='sex']").each(function (i) {
                if ($(this).attr("checked") == "checked") {
                    sex = $(this).val();
                }
            });*/
            $.ajax({
                type: "post",
                url: "${request.contextPath}/registerSave", //这里调用的 登录注册页面的 添加 路径
                data: {
                    "userName": userName,
                    "loginName": loginName,
                    "password": password,
                    "address": address,
                    "phone": phone,
                    "sex": sex
                },
                dataType: "json",
                success: function (result) {
                    if (result.code == 200) {
                        parent.layer.close(parentIndex);
                        parent.layer.msg(result.magess);
                        parent.getTableList(); //刷新表单
                    } else {
                        layer.msg(result.magess);
                    }
                }
            });
        }

    </script>
    <style>
        body {
            background-color: #FFFFFF;
        }
    </style>
</head>
<!-- //Head -->

<!-- Body -->

<body>
<form class="layui-form" id="addForm"> <!-- 提示：如果你不想用form，你可以换成div等任何一个普通元素 -->
    <div style="padding-top: 30px;"></div>
    <div class="layui-form-item">
        <label class="layui-form-label form-div-left">账号:</label>
        <div class="layui-input-block form-div-left">
            <input type="text" id="loginName" name="loginName" placeholder="请输入账号" autocomplete="off"
                   class="layui-input form-div-width">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label form-div-left">密码:</label>
        <div class="layui-input-block form-div-left">
            <input type="password" id="password" name="password" placeholder="请输入密码" autocomplete="off"
                   class="layui-input form-div-width">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label form-div-left">确认密码:</label>
        <div class="layui-input-block form-div-left">
            <input type="password" id="password2" name="password2" placeholder="请输入确认密码" autocomplete="off"
                   class="layui-input form-div-width">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label form-div-left">姓名:</label>
        <div class="layui-input-block form-div-left">
            <input type="text" id="userName" name="userName" placeholder="请输入姓名" autocomplete="off"
                   class="layui-input form-div-width">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label form-div-left">家庭地址:</label>
        <div class="layui-input-block form-div-left">
            <input type="text" id="address" name="address" placeholder="请输入家庭地址" autocomplete="off"
                   class="layui-input form-div-width">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label form-div-left">手机号码:</label>
        <div class="layui-input-block form-div-left">
            <input type="text" id="phone" name="phone" placeholder="请输入手机号码" autocomplete="off"
                   class="layui-input form-div-width">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label form-div-left">性别:</label>
        <div class="layui-input-block">
            <input type="radio" name="sex" id="sex1" value="0" title="男" checked>
            <input type="radio" name="sex" id="sex2" value="1" title="女">
        </div>
    </div>
</form>
</div>

</body>
<!-- //Body -->

</html>