<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>首页面</title>
    <#include "../../common/head.ftl">
    <script>
        $(function () {
            layui.use('element', function () {
                var element = layui.element;
                //一些事件触发
                element.on('tab(demo)', function (data) {
                });
            });
            layui.use('laydate', function () {
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                    elem: '#ctime' //指定元素
                });
            });
            layui.use('layer', function () {
                var layer = layui.layer;
            });

        })

        //添加 保存数据的
        function editSave(parentIndex) {
            var loginName = $("#loginName").val();
            if (loginName == undefined || loginName == "") {
                layer.msg("账号不能为空！");
                return false;
            }
            /*var password = $("#password").val();
            if (password == undefined || password == "") {
                layer.msg("密码不能为空！");
                return false;
            }
            var password2 = $("#password2").val();
            if (password2 == undefined || password2 == "") {
                layer.msg("确认密码不能为空！");
                return false;
            }
            if (password != password2) {
                layer.msg("密码和确认密码不一致！");
                return false;
            }*/
            var userName = $("#userName").val();
            if (userName == undefined || userName == "") {
                layer.msg("姓名不能为空！");
                return false;
            }
            var address = $("#address").val();
            if (address == undefined || address == "") {
                layer.msg("地址不能为空！");
                return false;
            }
            var phone = $("#phone").val();
            if (phone == undefined || phone == "") {
                layer.msg("电话不能为空！");
                return false;
            }
            var emptyLoginName = loginName.indexOf(" ");
            if (emptyLoginName != -1) {
                layer.msg("账号不能有空格！");
                return false;
            }
          /*  var emptyPassword = password.indexOf(" ");
            if (emptyPassword != -1) {
                layer.msg("密码不能有空格！");
                return false;
            }*/
            //判断真实姓名
            var users5 = /^[a-zA-Z\u4e00-\u9fa5]+$/;
            if (!users5.test(userName)) {
                top.layer.msg("姓名格式不正确！");
                return false;
            }
            var emptyUserName = userName.indexOf(" ");
            if (emptyUserName != -1) {
                layer.msg("姓名不能有空格！");
                return false;
            }
            var emptyAddress = address.indexOf(" ");
            if (emptyAddress != -1) {
                layer.msg("地址不能有空格！");
                return false;
            }
            //判断手机号
            var users4 = /^1[3456789]\d{9}$/;
            if (!users4.test(phone)) {
                top.layer.msg("电话号码格式不正确！");
                return false;
            }
            var emptyPhone = phone.indexOf(" ");
            if (emptyPhone != -1) {
                layer.msg("手机号不能有空格！");
                return false;
            }


            /* //判断身份证
             var users6 = /^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/;
             if(!users6.test(sjcodes)){
                 top.layer.msg( "身份证号格式不正确！");
                 return false;
             }*/


            var sex =$('input[name="sex"]:checked').val();
            /*
            var sex = 0;
            $("input[name='sex']").each(function (i) {
                if ($(this).attr("checked") == "checked") {
                    sex = $(this).val();
                }
            });*/
            $.ajax({
                type: "post",
                url: "${request.contextPath}/authority/sysUserEditSave", //这里调用的 登录注册页面的 添加 路径
                data: {
                    "userId": $("#userId").val(),
                    "userName": userName,
                    "loginName": loginName,
                    // "password": password,
                    "address": address,
                    "phone": phone,
                    "sex": sex
                },
                dataType: "json",
                success: function (result) {
                    if (result.code == 200) {
                        parent.layer.close(parentIndex);
                        parent.layer.msg(result.magess);
                        parent.getTableList(); //刷新表单
                    } else {
                        layer.msg(result.magess);
                    }
                }
            });
        }


    </script>

    <style>
        body {
            background-color: #FFFFFF;
        }

        .pcq-div-text {
            position: relative;
            padding: 9px 15px;
            line-height: 20px;
            font-size: 16px;
            color: #000;
            text-align: left;
        }

        .pcq-h2-title {
            font-size: 18px;
            font-weight: 600;
            color: #2e2e2e;
        }

        .pcq-nav-role {
            font: 12px verdana, arial, sans-serif; /* 设置文字大小和字体样式 */
            width: 100%;
        }

        .pcq-nav-role, .pcq-nav-role li {
            list-style: none; /* 将默认的列表符号去掉 */
            padding: 0; /* 将默认的内边距去掉 */
            margin: 0; /* 将默认的外边距去掉 */
            float: left; /* 往左浮动 */
            display: block;
        }

        .pcq-nav-role li a {
            display: inline-block; /* 将链接设为块级元素 */
            width: 140px; /* 设置宽度 */
            height: 30px; /* 设置高度 */
            margin-bottom: 16px;
            padding: 0 7px;
            margin-left: 16px;
            line-height: 30px; /* 设置行高，将行高和高度设置同一个值，可以让单行文本垂直居中 */
            text-align: center; /* 居中对齐文字 */
            background-color: rgb(235, 242, 247);
            color: #507999;
            text-decoration: none; /* 去掉下划线 */
            border: 1px solid #ebf2f7;
            border-radius: 4px;
        }

        .pcq-nav-role li a:hover {
            background-color: rgb(224, 233, 240); /* 变换背景色 */
        }

        .form-div-left {
            left: 100px;
        }

    </style>
</head>
<!-- //Head -->

<!-- Body -->

<body>
<div class="layui-collapse" style="padding:40px;height: 100%">
    <div class="layui-colla-item">
        <h2 class="layui-colla-title pcq-h2-title">个人基本信息</h2>
        <div class="layui-colla-content layui-show">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md6">
                    <div class="layui-panel">
                        <div style="padding:20px; ">
                            <form class="layui-form" id="addForm"> <!-- 提示：如果你不想用form，你可以换成div等任何一个普通元素 -->
                                <input hidden id="userId" name="userId" value="${sysUser.userId!""}"/>
                                <div style="padding-top: 30px;"></div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label form-div-left">账号:</label>
                                    <div class="layui-input-block form-div-left">
                                        <input type="text" id="loginName" name="loginName" placeholder="请输入账号"
                                               autocomplete="off" value="${sysUser.loginName!""}"
                                               class="layui-input form-div-width">
                                    </div>
                                </div>
                               <#-- <div class="layui-form-item">
                                    <label class="layui-form-label form-div-left">密码:</label>
                                    <div class="layui-input-block form-div-left">
                                        <input type="password" id="password" name="password" placeholder="请输入密码"
                                               autocomplete="off" value=""
                                               class="layui-input form-div-width">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label form-div-left">确认密码:</label>
                                    <div class="layui-input-block form-div-left">
                                        <input type="password" id="password2" name="password2" placeholder="请输入确认密码"
                                               autocomplete="off" value=""
                                               class="layui-input form-div-width">
                                    </div>
                                </div>-->
                                <div class="layui-form-item">
                                    <label class="layui-form-label form-div-left">姓名:</label>
                                    <div class="layui-input-block form-div-left">
                                        <input type="text" id="userName" name="userName" placeholder="请输入姓名"
                                               autocomplete="off" value="${sysUser.userName!""}"
                                               class="layui-input form-div-width">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label form-div-left">家庭地址:</label>
                                    <div class="layui-input-block form-div-left">
                                        <input type="text" id="address" name="address" placeholder="请输入家庭地址"
                                               autocomplete="off" value="${sysUser.address!""}"
                                               class="layui-input form-div-width">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label form-div-left">手机号码:</label>
                                    <div class="layui-input-block form-div-left">
                                        <input type="text" id="phone" name="phone" placeholder="请输入手机号码"
                                               autocomplete="off" value="${sysUser.phone!""}"
                                               class="layui-input form-div-width">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label form-div-left">性别:</label>
                                    <div class="layui-input-block form-div-left">
                                        <input type="radio" name="sex" id="sex1" value="0" title="男"
                                               <#if sysUser.sex?default("") == "0">checked</#if>>
                                        <input type="radio" name="sex" id="sex2" value="1" title="女"
                                               <#if sysUser.sex?default("") == "1">checked</#if>>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label form-div-left">创建时间:</label>
                                    <div class="layui-input-block form-div-left">
                                        <input type="text" readonly  autocomplete="off" value="${sysUser.ctime!""}"
                                               class="layui-input form-div-width">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="layui-colla-item">
        <h2 class="layui-colla-title pcq-h2-title">当前拥有的角色</h2>
        <div class="layui-colla-content">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md6">
                    <div class="layui-panel">
                        <#if roleIndex == 1>
                            <div class="layui-card" style="margin-bottom: -2px;">
                                <div class="layui-card-header" style="padding: 0px 15px;overflow:hidden;height: 100%">
                                    <ul class="pcq-nav-role" id="thisRole">
                                        <#--<#if sysUser.sysRole??>
                                            <#list sysUser.sysRole as role>
                                                <li class="layui-nav-item">
                                                    <a style="margin-top:10px;background-color: rgb(80, 121, 153);color: #fff;">${role.roleName!''}
                                                        <i class="layui-icon layui-icon-close" onclick="delInfoRole()"
                                                           style="float: right;"></i></a>
                                                </li>
                                            </#list>
                                        </#if>-->
                                    </ul>
                                </div>
                                <div class="layui-card-body" style="overflow:hidden">
                                    <ul class="pcq-nav-role">
                                        <#if sysRoleList??>
                                            <#list sysRoleList as role>
                                                <li class="layui-nav-item">
                                                    <a href="javascript:addInfoRole('${sysUser.userId!""}','${role.roleId!''}')">${role.roleName!''}</a>
                                                </li>
                                            </#list>
                                        </#if>
                                    </ul>
                                </div>
                            </div>
                        <#else>
                            <div style="padding:20px;overflow:hidden;height: 100%">
                                <ul class="pcq-nav-role">
                                    <#if sysUser.sysRole??>
                                        <#list sysUser.sysRole as role>
                                            <li class="layui-nav-item">
                                                <a style="background-color: rgb(80, 121, 153);color: #fff;">${role.roleName!''}</a>
                                            </li>
                                        </#list>
                                    </#if>
                                </ul>
                            </div>
                        </#if>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<!-- //Body -->

</html>
<#-- 当前用户 角色  添加 删除  等操作js-->
<script>


    // 给当前用户 添加角色
    function addInfoRole(userId, roleId) {
        $.ajax({
            type: "post",
            url: "${request.contextPath}/authority/addSysUserRole",
            data: {
                "userId": userId,
                "roleId": roleId
            },
            dataType: "json",
            success: function (result) {
                if (result.code == 200) {
                    var data = result.obj;
                    $("#thisRole").empty();
                    for (var i = 0; i < data.length; i++) {
                        var liSon = $("<li class=\"layui-nav-item\"></li>");
                        var aSon = $("<a style=\"margin-top:10px;background-color: rgb(80, 121, 153);color: #fff;\">" + data[i].roleName + "</a>");
                        var aiSon = $("<i class=\"layui-icon layui-icon-close\" onclick=\"delInfoRole('${sysUser.userId!""}','" + data[i].roleId + "')\"   style=\"float: right;\"></i>");
                        aSon.append(aiSon);
                        liSon.append(aSon);
                        $("#thisRole").append(liSon);
                    }
                    layer.msg(result.magess);
                } else {
                    layer.msg(result.magess);
                }
            }
        });
    }

    //给当前用户 删除 角色
    function delInfoRole(userId, roleId) {
        var thisUserId = "${sysUser.userId!""}";
        var thisAdministratorsId = "${administratorsId!""}";
        if (thisUserId == thisAdministratorsId) {
            layer.msg("您是超级管理员,取消权限失败！");
            return false;
        }

        $.ajax({
            type: "post",
            url: "${request.contextPath}/authority/delSysUserRole",
            data: {
                "userId": userId,
                "roleId": roleId
            },
            dataType: "json",
            success: function (result) {
                if (result.code == 200) {
                    var data = result.obj;
                    $("#thisRole").empty();
                    for (var i = 0; i < data.length; i++) {
                        var liSon = $("<li class=\"layui-nav-item\"></li>");
                        var aSon = $("<a style=\"margin-top:10px;background-color: rgb(80, 121, 153);color: #fff;\">" + data[i].roleName + "</a>");
                        var aiSon = $("<i class=\"layui-icon layui-icon-close\" onclick=\"delInfoRole('${sysUser.userId!""}','" + data[i].roleId + "')\"   style=\"float: right;\"></i>");
                        aSon.append(aiSon);
                        liSon.append(aSon);
                        $("#thisRole").append(liSon);
                    }
                    $("#thisRole").append();
                    layer.msg(result.magess);
                } else {
                    layer.msg(result.magess);
                }
            }
        });
    }


    //进入页面是进行 初始化显示自己的 角色
    function showUserRole() {
        $.ajax({
            type: "post",
            url: "${request.contextPath}/authority/findSysRoleList",
            data: {
                "userId": "${sysUser.userId!""}"
            },
            dataType: "json",
            success: function (result) {
                if (result.code == 200) {
                    var data = result.obj;
                    $("#thisRole").empty();
                    for (var i = 0; i < data.length; i++) {
                        var liSon = $("<li class=\"layui-nav-item\"></li>");
                        var aSon = $("<a style=\"margin-top:10px;background-color: rgb(80, 121, 153);color: #fff;\">" + data[i].roleName + "</a>");
                        var aiSon = $("<i class=\"layui-icon layui-icon-close\" onclick=\"delInfoRole('${sysUser.userId!""}','" + data[i].roleId + "')\"   style=\"float: right;\"></i>");
                        aSon.append(aiSon);
                        liSon.append(aSon);
                        $("#thisRole").append(liSon);
                    }
                    $("#thisRole").append();
                } else {
                    layer.msg(result.magess);
                }
            }
        });
    }

    showUserRole();
</script>