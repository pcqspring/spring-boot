<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>首页面</title>
    <#include "../common/head.ftl">
    <script>
        $(function () {
            layui.use('element', function () {
                var element = layui.element;
                //一些事件触发
                element.on('tab(demo)', function (data) {
                });
            });
            layui.use('laydate', function () {
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                    elem: '#ctime' //指定元素
                });
            });
            layui.use('layer', function () {
                var layer = layui.layer;
            });
            $(".layui-nav li").click(function () {
                $(".layui-nav li").each(function (i) {
                    $(this).removeClass("layui-this");
                });
                $(this).addClass("layui-this");
            });
        })

        function showView(url) {
            $.ajax({
                type: "post",
                url: '${request.contextPath}' + url,
                data: {},
                dataType: "html",
                success: function (result) {
                    $("#ifrime").empty();
                   /* setTimeout(function () {
                        $("#ifrime").html(result);
                    }, 200);*/
                    $("#ifrime").html(result);
                }
            });
        }


        function logOut() {
            // /logOut
            layer.confirm('确定退出登陆吗?', function (index) {
                //do something
                layer.close(index);
                $.ajax({
                    type: "post",
                    url: "${request.contextPath}/logOut",
                    data: {},
                    dataType: "json",
                    success: function (result) {
                        if (result.code == 200) {
                            layer.msg(result.magess);
                            setTimeout(function () {
                                window.location.reload();
                            }, 700);
                        } else {
                            layer.msg(result.magess);
                        }
                    }

                });
            });
        }
    </script>
    <script type="text/html" id="barTableList">
        <a class="layui-btn layui-btn-normal layui-btn-xs sc-a" lay-event="detail">查看</a>
        <a class="layui-btn layui-btn-normal layui-btn-xs sc-a" lay-event="edit">编辑</a>
        <a class="layui-btn layui-btn-danger layui-btn-xs sc-a" lay-event="del">删除</a>
    </script>
    <style>
        .custom-collapse .layui-colla-item {
            margin-bottom: 10px;
            border: none;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
            /*background-color: #fff;*/
            transition: all 0.3s ease;
        }
        .custom-collapse .layui-colla-item:hover {
            transform: translateY(-2px);
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.2);
        }

        /* 自定义样式 */
        .dark-search {
            display: flex;
            align-items: center;
        }
        .dark-search input[type="text"] {
            width: 74%;
            min-width: 308px;
            position: absolute;
            right: 90px;
            top: -18px;
            height: 36px;
            padding: 6px 12px;
            font-size: 14px;
            border: none;
            border-top-left-radius: 40px;
            border-bottom-left-radius: 40px;
            background-color: #343a40;
            color: #fff;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        }
        .dark-search button {
            width: 18%;
            min-width: 74px;
            position: absolute;
            right: 16px;
            top: -18px;
            height: 36px;
            padding: 0 16px;
            font-size: 14px;
            border: none;
            /*border-radius: 4px;*/
            border-top-right-radius: 40px;
            border-bottom-right-radius: 40px;
            background-color: #495057;
            color: #fff;
            cursor: pointer;
            transition: background-color 0.3s ease;
        }
        .dark-search button:hover {
            background-color: #343a40;
        }

        .ul-title-left {
            width: 57%;
            min-width: 908px;
            padding:  0px 0px 0px 17%;
            display: inline-block;
            border-top-right-radius: 0px;
            border-bottom-right-radius: 0px
        }

        .ul-title-right {
            width: 20%;
            min-width: 304px;
            display: inline-block;
            position: absolute;
            top: 0px;
            border-top-left-radius: 0px;
            border-bottom-left-radius: 0px
        }
        .icon-select{
            left: -6px;
            position: relative;
        }
    </style>
</head>
<!-- //Head -->

<!-- Body -->

<body>
<div class="body-div">
    <div class="body-div-top" style="background-color: #393D49;">
        <ul class="layui-nav ul-title-left" lay-filter="">
            <li class="layui-nav-item "><a href="${request.contextPath}/index">首页面</a></li>
            <li class="layui-nav-item layui-this"><a href="${request.contextPath}/authority/index">权限配置</a></li>
            <li class="layui-nav-item"><a href="#">大数据</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">解决方案</a>
                <dl class="layui-nav-child"> <!-- 二级菜单 -->
                    <dd><a href="">移动模块</a></dd>
                    <dd><a href="">后台模版</a></dd>
                    <dd><a href="">电商平台</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a href="#">社区</a></li>
        </ul>
        <div class="layui-nav dark-search" style="display: inline-block;width: 22%; ">
            <input type="text" placeholder="请输入关键字">
            <button type="button"><i class="layui-icon layui-icon-search icon-select"></i>搜索</button>
        </div>
        <ul class="layui-nav ul-title-right">
            <li class="layui-nav-item">
                <a href="#">控制台<span class="layui-badge">9</span></a>
            </li>
            <li class="layui-nav-item">
                <a href="javascript:;">${userName?default('')}<span class="layui-badge-dot"></span></a>
                <dl class="layui-nav-child"> <!-- 二级菜单 -->
                    <dd><a href="">个人信息</a></dd>
                    <dd><a href="">我的收藏</a></dd>
                    <dd><a href="#" onclick="logOut()">退出登录 <i class="layui-icon layui-icon-logout"></i></a></dd>
                </dl>
            </li>
        </ul>
    </div>
    <div class="body-div-bottom">
        <div class="body-div-left">
            <div class="layui-panel">
                <ul class="layui-menu" id="ul-lis">
                    <li class="layui-menu-item-group layui-menu-item-down custom-collapse layui-colla-item">
                        <div class="layui-menu-body-title">
                            权限配置 <i class="layui-icon layui-icon-up"></i>
                        </div>
                        <ul>
                            <li class="layui-menu-body-title custom-collapse layui-colla-item" onclick="showView('/authority/sysUserView')">用户配置</li>
                            <li class="layui-menu-body-title custom-collapse layui-colla-item" onclick="showView('/authority/sysRoleView')">角色配置</li>
                            <li class="layui-menu-body-title custom-collapse layui-colla-item" onclick="showView('/authority/sysAppView')">路径配置</li>
                        </ul>
                    </li>
                    <#--                    <li class="layui-menu-item-divider"></li>
                                        <li>
                                            <div class="layui-menu-body-title">首页面2</div>
                                        </li>-->
                </ul>
            </div>
        </div>
        <div id="ifrime" class="body-div-right">
<#--            background-image: url(/static/img/4494.jpg)-->
            <div style="position: absolute;height:100%;width: 100%; text-align: center;">
                <h1 style="position:absolute;top: 380px;left: 36%;color: #999">Welcome to the permissions page...</h1>
            </div>
        </div>
    </div>

</div>

</body>
<!-- //Body -->

</html>