package com.demo.config;

import com.alibaba.fastjson.JSONObject;
import com.demo.authority.model.SysApp;
import com.demo.authority.model.SysRole;
import com.demo.authority.model.SysUser;
import com.demo.authority.service.SysConfigService;
import com.demo.authority.utils.SysConfigUtil;
import com.demo.utils.RedisUtil;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public class LoginHandlerInterceptor implements HandlerInterceptor {


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 登录成功之后，应该有用户的session
        HttpSession session = request.getSession();
        SysUser loginUser = (SysUser) session.getAttribute("loginUser");
        if (loginUser == null) { //没有登录
            request.setAttribute("msg", "没有权限，请先登录");
            request.getRequestDispatcher("/login").forward(request, response);
            return false;
        }
        String requestURI = request.getRequestURI();
        log.info("============访问路径requestURI===============>{}", requestURI);
        requestURI = requestURI.replace(SysConfigUtil.getContextPath(),"");
        String[] requestURL = requestURI.split("/");
        //后台权限页面路径 检测  和 swagger文档页面
        boolean b = inspectUrl(requestURI, loginUser);
        //后台权限页面路径 检测 不通过进入if 检测是否是其他路径
        if (!b) {
            List<String> urlList = new ArrayList<>();
            //获取角色 通过角色 获取角色对应的 路径
            List<SysRole> sysRoleList = loginUser.getSysRole();
            //检测当前 （用户 的 角色） 可以使用的 路径地址

            b = inspectUrl(urlList, sysRoleList, requestURL[1]);
        }
        if (!b) {
            request.setAttribute("msg", "没有权限，返回首页面！");
            request.getRequestDispatcher("/login").forward(request, response);
            return b;
        }
        session.setMaxInactiveInterval(3600); //单位 秒  访问一下 刷新一下 session 生命周期
        return true;
    }

    //检测 后台权限配置页面 和 swagger文档页面
    private boolean inspectUrl(String requestURI, SysUser loginUser) {
        boolean b = false;
        //管理员角色id
        String configUserId = SysConfigUtil.getConfigUserId("role.administrators.id", "0000000000000000000000000000001");
        log.info("============管理员角色id===============>{}", configUserId);
        if (requestURI.indexOf("authority") != -1 || requestURI.indexOf("doc.html") != -1 ||
                requestURI.indexOf("webjars") != -1 ||  requestURI.indexOf("swagger") != -1) {
            List<SysRole> sysRoleList = loginUser.getSysRole();
            for (int i = 0; i < sysRoleList.size(); i++) {
                SysRole sysRole = sysRoleList.get(i);
                if (configUserId.equals(sysRole.getRoleId())) {
                    b = true;
                    break;
                }
            }
        }
        return b;
    }

    //检测 角色可使用的页面
    private boolean inspectUrl(List<String> urlList, List<SysRole> sysRoleList, String requestURI) {
       /* for (int i = 0; i < sysRoleList.size(); i++) {
            SysRole sysRole = sysRoleList.get(i); //获取角色
            List<SysApp> sysAppList = sysRole.getSysApp();// 通过角色 获取路径集合
            for (int j = 0; j < sysAppList.size(); j++) {
                SysApp sysApp = sysAppList.get(i);
                String appPath = sysApp.getAppPath();
          *//*      appPath = appPath.replace("/", "-");  // 把斜杠替换成  -
                appPath = appPath.substring(appPath.indexOf("-") + 1);//获取第一个 - 之后的字符串
                appPath = appPath.substring(0, appPath.indexOf("-"));//获取  第二个 - 之前的 字符串*//*
                urlList.add(appPath);  //获取路径放入路径集合
            }
        }*/
        boolean b = false;
        sysRoleList.stream()//获取角色
                .forEach(sysRole -> sysRole.getSysApp().stream()// 通过角色 获取路径集合
                        .forEach(sysApp -> urlList.add(sysApp.getAppPath())));//获取路径放入路径集合
        List<String> collect = urlList.stream().distinct().collect(Collectors.toList());
        for (int i = 0; i < collect.size(); i++) {
            String url = collect.get(i);
            if (url.indexOf(requestURI) != -1) {
                b = true;
                break;
            }
        }
        return b;
    }


    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }
}