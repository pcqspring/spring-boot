package com.demo.config;

import com.demo.authority.utils.SysConfigUtil;
import com.demo.handler.TraceInterceptor;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
public class MyMvcConfig implements WebMvcConfigurer {


    @Autowired
    private TraceInterceptor traceInterceptor;
    /**
     * springboot 无法直接访问静态资源，需要放开资源访问路径。
     * 添加静态资源文件，外部可以直接访问地址
     *
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //释放 静态资源路径 swagger  否则 访问不到 swagger2的 页面
        registry.addResourceHandler("/doc.html")
                .addResourceLocations("classpath:/META-INF/resources/");
      /*  registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");*/
        registry.addResourceHandler("/static/**")
                .addResourceLocations("classpath:/static/");
    }

    /**
     * 拦截器配置
     */
    //添加自定义拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
      /*  List<String> pathDefaultList = Arrays.asList("/login","/logOut", "/loginAdmin",//登陆页面
                "/registerAdmin", "/registerSave", //注册页面
                "/retrievePassword", "/retrieveSave",//找回密码页面
                //静态资源
                "/static/css/**", "/static/jquery/**", "/static/layui/**", "/static/js/**",
                "/static/fonts/**", "/static/img/**", "/static/bootstrap/**", "/public/**"
        );*/
        String pathDefault = SysConfigUtil.getConfigUserId("exclude.path.patterns", "/authority/getUserAppTreeList,/login,/logOut,/loginAdmin,/registerAdmin,/registerSave,/retrievePassword,/retrieveSave,/static/css/**,/static/jquery/**,/static/layui/**,/static/js/**,/static/fonts/**,/static/img/**,/static/bootstrap/**,/public/**");
        List<String> pathDefaultList = Arrays.asList(pathDefault.split(","));
        registry.addInterceptor(traceInterceptor).addPathPatterns("/**").order(1);
        registry.addInterceptor(new LoginHandlerInterceptor()).addPathPatterns("/**")
                //配置不拦截的路径  //  "/static/**",
                .excludePathPatterns(pathDefaultList);
    }


}
