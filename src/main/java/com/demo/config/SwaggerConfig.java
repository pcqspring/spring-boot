package com.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;
/*
@Configuration
@EnableSwagger2*/

/**
 * Knife4j配置类
 *
 */
@Configuration
@EnableSwagger2WebMvc
public class SwaggerConfig {

    @Value("${project.api}")
    private String xiangmu;
    @Value("${project.title}")
    private String title;
    //----------------------小组----------------------

    @Bean
    @Profile({"dev", "test", "show"})//设置要显示的swagger环境 我自己项目下 application.yml 中是 spring.profiles.active 是 dev
    public Docket createApi(Environment environment) {
        //设置要显示的swagger环境
       /* Profiles profiles = Profiles.of("dev"); //开发环境
        //通过 acceptsProfiles() 判断是否处在自己设定的环境当中
        boolean flag = environment.acceptsProfiles(profiles);*/
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("异步请求第一小组")
                .enable(true)//false 则不能在浏览器访问，true为默认。
                .select()
                // 路径配置
                .apis(RequestHandlerSelectors.basePackage(xiangmu))//指定扫描的包 切记 跟 MapperScan 不同 不支持  com.demo. 直接 com.demo 就行
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(title) //标题
                .description("我们的原则就是:异步请求，局部刷新！")//描述
                .contact(new Contact("肖海瑞和彭朝群", "http://localhost:9100/swagger-ui.html", "97972171@qq.com"))//添加 开发文档的作者信息 名字 和 地址  和 邮箱
                .version("1.0 version") //项目版本
                .license("Mr.Peng, Version 2.0")//写着玩 直接 写的 apache 路径//#许可证
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")//#许可证URL
                .build();
    }

    // 配置分组
    @Bean
    @Profile({"dev", "test", "show"})//设置要显示的swagger环境
    public Docket createApi2(Environment environment) {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("异步请求第二小组")
                .enable(true) //false 则不能在浏览器访问，true为默认。
                .select()
                // 路径配置
                .apis(RequestHandlerSelectors.basePackage("com.demo.login.controller"))//指定扫描的包 切记跟 MapperScan 不同 不支持  com.demo.*直接 com.demo 就行
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo2());
    }

    private ApiInfo apiInfo2() {
        return new ApiInfoBuilder()
                .title("Spring-Boot-Demo 后台API") //标题
                .description("我们的原则就是:异步请求，局部刷新！")//描述
                .contact(new Contact("王宇和张世龙", "http://localhost:9100/swagger-ui.html", "2160193949@qq.com"))//添加 开发文档的作者信息 名字 和 地址  和 邮箱
                .version("1.0 version") //项目版本
                .license("Mr.Peng, Version 2.0") //写着玩 直接 写的 apache 路径//#许可证
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")//#许可证URL
                .build();
    }


    //    @Api                   用于类，标识这个类是swagger的资源
    //    @ApiSupport(author = "xiaoymin@foxmail.com")用于Controller类，当前接口作者
    //    @ApiIgnore             用于类，忽略该 Controller，指不对当前类做扫描
    //    @ApiOperation          用于方法，描述 Controller类中的 method接口
    //    @ApiParam              用于参数，单个参数描述，与 @ApiImplicitParam不同的是，
    //                           他是写在参数左侧的。如（ @ApiParam(name="username",value="用户名")String username）
    //    @ApiModel              用于类，表示对类进行说明，用于参数用实体类接收
    //    @ApiProperty           用于方法，字段，表示对model属性的说明或者数据操作更改
    //    @ApiImplicitParam      用于方法，表示单独的请求参数
    //                           header-->请求参数的获取：@RequestHeader(代码中接收注解)
    //                           query-->请求参数的获取：@RequestParam(代码中接收注解)
    //                           path（用于restful接口）-->请求参数的获取：@PathVariable(代码中接收注解)
    //                           body-->请求参数的获取：@RequestBody(代码中接收注解)
    //                           form（不常用）
    //    @ApiImplicitParams     用于方法，包含多个 @ApiImplicitParam
    //    @ApiResponse           用于方法，描述单个出参信息
    //    @ApiResponses          用于方法，包含多个@ApiResponse
    //    @ApiError              用于方法，接口错误所返回的信息
}
