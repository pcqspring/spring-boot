package com.demo.utils;

import lombok.Data;
import lombok.ToString;

import java.util.List;
import java.util.Map;

@Data
@ToString
public class QueryPage {

    private Map<String,Object> data;
    private int current;
    private int size;  //
    //添加注释   
}
