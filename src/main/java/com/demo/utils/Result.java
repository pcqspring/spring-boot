package com.demo.utils;

import com.demo.enums.EnumResult;

public class Result<T> {

    private int code;  //状态码
    private String magess; //友好提示
    private T obj;

    public Result(int code, String magess, T obj) {
        this.code = code;
        this.magess = magess;
        this.obj = obj;
    }

    public Result(int code, String magess) {
        this.code = code;
        this.magess = magess;
    }

    public Result(EnumResult enumResult, T obj) {
        this.code = enumResult.getCode();
        this.magess = enumResult.getMagess();
        this.obj = obj;
    }

    public Result(EnumResult enumResult) {
        this.code = enumResult.getCode();
        this.magess = enumResult.getMagess();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMagess() {
        return magess;
    }

    public void setMagess(String magess) {
        this.magess = magess;
    }

    public T getObj() {
        return obj;
    }

    public void setObj(T obj) {
        this.obj = obj;
    }
}
