package com.demo.utils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.demo.authority.model.SysRole;
import com.demo.authority.model.SysUser;
import com.demo.authority.service.SysRoleService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;

public class SysUserUtils {

    //获取 当前登陆人的用户信息
    public static SysUser getSysUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        SysUser loginUser = (SysUser) session.getAttribute("loginUser");
        return loginUser;
    }

 /*
    public static void main(String[] args) {
        //去重复的长度存入 List
        List<Integer> qty = Arrays.asList(3, 4, 5);
        //模拟数据
        HashMap<String, Object> group = new HashMap<>();
        group.put("123a",1241);
        group.put("123b",12412);
        group.put("123c",124123);
        group.put("123d",124124);
    });
    List<List<String>> collect1 = qty.stream().map((index) -> {
            List<String> collect = group.entrySet().stream().map((entry) -> {
                String value = entry.getValue() + "";
                int length = value.length();
                if (index == length) {
                    String key = entry.getKey();
                    System.out.println("--------> integer=" + index + "  做处理的key=" + key);
                    return key;
                } else {
                    return "";
                }
            }).collect(Collectors.toList());
            return collect;
        }).collect(Collectors.toList());


*//*
        //去重复的lsit
        for (int i = 0; i < qty.size(); i++) {
            //10000条groupmap
            Integer integer = qty.get(i);
            Set<Map.Entry<String, Object>> entries = group.entrySet();

            for (Map.Entry<String, Object>entry:entries){
                String  value =  entry.getValue() +"";
                //长度相等做处理
                int length = value.length();
                if( integer == length){
                    String key = entry.getKey();
                    System.out.println("integer="+integer+"  做处理的key="+key);
                }
            }
        }*//*

     *//*   Set<Map.Entry<String, Object>> entries = group.entrySet();
        for (Map.Entry<String, Object>entry:entries){
            int value = (int) entry.getValue();
            String key = entry.getKey();
            System.out.println("key="+key+"     value="+value);
        }
*//*
    }*/

   /* @Autowired
    private static SysRoleService sysRoleService;

    public static void main(String[] args) {

        //1. Lambda 表达式
        new Thread(()->System.out.println("执行了 Run方法！！！")).start();
        //2.
        int cal = calNum(10,20,(a,b) -> a + b);
        System.out.println(cal);
        //3.
        int[] arr = {1,2,3,4,5,6,7,8,9,10};
        calNum2(arr,s -> s%2==0);


        add("小龙","小飞",(a,b) ->  {
            int i = arr[0];
            return a +"-----"+i+"----"+b;});
        calNum3(arr,s -> System.out.println(s));

       *//* SysUser sysUser = new SysUser();
        sysUser.setRoleId("0001");
        SysRole sysRole = typeConver(sysUser, sysUser1 -> sysRoleService.getById(sysUser1.getRoleId()));
        System.out.println(sysRole);*//*
    }

    //TestFunction 自定义函数接口
    public static void add(String str1, String str2, TestFunction testFunction){
        System.out.println(testFunction.longAndfei(str1,str2));
    }

    //IntBinaryOperator 传入两个 int类型 返回一个int类型 的函数接口
    public static int calNum(int a,int b ,IntBinaryOperator operator){
        return operator.applyAsInt(a,b);
    }

    //IntPredicate 传入一个 int类型 返回一个boolean类型 的函数接口
    public static void calNum2(int[] arr, IntPredicate intPredicate){
        for (int i : arr) {
            if(intPredicate.test(i)){
                System.out.println(i);
            }
        }
    }

    //IntConsumer 传入两个 int类型 无类型 的函数接口
    public static void calNum3(int[] arr, IntConsumer consumer){
        for (int i : arr) {
            consumer.accept(i);
        }
    }

    //Function<SysUser,R> 传入一个 T 类型 返回一个 R 类型 的函数接口
    public static <R> R typeConver(SysUser sysUser,Function<SysUser,R> function){
        return function.apply(sysUser);
    }
*/


}
