package com.demo.utils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
public class ConditionUtils<T> {

    private T t;


    public static <T> void addCondition(QueryWrapper<T> wrapper, Map<String, Object> map, String where, String mapKey, String func) {
        String value = (String) map.get(mapKey);
        where = where.toUpperCase();
        if(StringUtils.isNotEmpty(value)){
            switch (where){
                case "LIKE":
                    wrapper.like(func,value);
                    break;
                case "EQ":
                    wrapper.eq(func,value);
                    break;
                case "NE":
                    wrapper.ne(func,value);
                    break;
                case "GT":
                    wrapper.gt(func,value);
                    break;
                case "GE":
                    wrapper.ge(func,value);
                    break;
                case "LT":
                    wrapper.lt(func,value);
                    break;
                case "LE":
                    wrapper.le(func,value);
                    break;
                case "OR":
                    wrapper.gt(func,value);
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + where);
            }

        }
    }




}
