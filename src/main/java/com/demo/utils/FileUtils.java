package com.demo.utils;


import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileUtils {

    private static final Logger log = LoggerFactory.getLogger(FileUtils.class);

    public static final String[] imgSuffix = {".bmp",".jfif", ".jpg", ".jpeg", ".pdf", ".png", ".tiff", ".gif", ".pcx", ".tga", ".exif", ".fpx", ".svg", ".psb", ".psd", ".cdr", ".pcd", ".dxf", ".ufo", ".eps", ".ai", ".raw"};
    public static final String[] videoSuffix = {".avi", ".rmvb", ".rm", ".asf", ".divx", ".dat", ".flv", ".mpg", ".mpeg", ".mpe", ".mov", ".3gp", ".wmv", ".mp4", ".m4v", ".mkv", ".vob"};
    public static final String[] wordSuffix = {".doc", ".docx", ".dot", ".dotx", ".docm", ".dotm"};
    public static final String[] excelSuffix = {".xls", ".xlsb", ".xltx", ".xltm", ".xlt", ".xlam", ".xla", ".xlsx"};

    /**
     * @param imgSuffix 判断是否是 图片
     * @return 是则返回 true
     */
    public static boolean imgSuffix(String imgSuffix) {
        for (int i = 0; i < FileUtils.imgSuffix.length; i++)
            if (FileUtils.imgSuffix[i].equals(imgSuffix))
                return true;
        return false;
    }

    /**
     * @param videoSuffix 判断是否是 视频
     * @return 是则返回 true
     */
    public static boolean videoSuffix(String videoSuffix) {
        for (int i = 0; i < FileUtils.videoSuffix.length; i++)
            if (FileUtils.videoSuffix[i].equals(videoSuffix))
                return true;
        return false;
    }

    /**
     * @param wordSuffix 判断是否是 Word
     * @return 是则返回 true
     */
    public static boolean wordSuffix(String wordSuffix) {
        for (int i = 0; i < FileUtils.wordSuffix.length; i++)
            if (FileUtils.wordSuffix[i].equals(wordSuffix))
                return true;
        return false;
    }

    /**
     * @param excelSuffix 判断是否是 Excel
     * @return 是则返回 true
     */
    public static boolean excelSuffix(String excelSuffix) {
        for (int i = 0; i < FileUtils.excelSuffix.length; i++)
            if (FileUtils.excelSuffix[i].equals(excelSuffix))
                return true;
        return false;
    }

    /**
     * @param fileNameSuffix 根据后缀名判断创建文件Url
     * @return 返回  一个例如    ”Image/yyyy-MM-dd”格式
     */
    public static String suffixFileName(String fileNameSuffix) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String format = sdf.format(new Date());
        log.info("文件后缀名=====================>{}", fileNameSuffix);
        //通过后缀名判断是否是图片
        if (FileUtils.imgSuffix(fileNameSuffix))
            return "Image/" + format;
        //通过后缀名判断是否是视频
        if (FileUtils.videoSuffix(fileNameSuffix))
            return "Video/" + format;
        //通过后缀名判断是否是视频
        if (FileUtils.wordSuffix(fileNameSuffix))
            return "Word/" + format;
        //通过后缀名判断是否是视频
        if (FileUtils.excelSuffix(fileNameSuffix))
            return "Excel/" + format;
        //都不属于就创建一个 File文件
        return "File/" + format;
    }

    /**
     * @param file             上传的文件
     * @param filePath         上传到的 地址   例如:"D:/uploadFile/abcdfiles/Image/yyyy-MM-dd" (最后一个不需要添加斜杠)
     * @param rootPathFileName 文件上传的 根路径文件名 例如:"BFA7FD01000000005A0F9BA10000000C.jpg"
     * @return 返回 字节大小 String
     */
    public static String fileUpload(MultipartFile file, String filePath, String rootPathFileName) {
        // 上传(大文件)时切记不要,调用 file.getBytes() 否侧会直接计算大小,会出现JVM溢出
//        log.info("文件上传到的 文件夹地址==========>{}", filePath);
        log.info("上传存放在文件夹地址下的新文件名称=>{}", rootPathFileName);
        if (file.isEmpty()) {
            log.info("=========> file为空 <==========");
            return null;
        }
        //创建开始
        File targetFile = new File(filePath);
        //判断文件是否存在
        if (!targetFile.exists()) {
            targetFile.mkdirs();
        }
        //创建输出流
        FileOutputStream outputStream = null;
        long size = 0l; //获取 文件字节大小
        InputStream is = null;
        try {
            // 文件夹下创建文件
            outputStream = new FileOutputStream(filePath + "/" + rootPathFileName);
            //获取输入流，其中有上传 文件的内容
            is = file.getInputStream();
            //将输入流中的数据写入输出流中
            int len = -1;
            byte[] buf = new byte[4096];
            // read() 从该输入流读取一个字节的数据。边读边写  上传到本地 指定路径下
            while ((len = is.read(buf)) != -1) {
                size += len;
                //  不要打印因为这是循环
                outputStream.write(buf, 0, len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    //刷新缓冲
                    outputStream.flush();
                    //关闭
                    outputStream.close();
                    is.close();
                    log.info("======>刷新缓冲 关闭输出流 上传结束 <======");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        log.info("上传完毕 文件字节大小=============>{}", size);
        return size + "";
    }

    /**
     * @param fileUrl 检测 当前Url 是否是 "/"  结尾 不是则 拼接 "/"
     * @return
     */
    public static String showUrl(String fileUrl) {
        return !fileUrl.endsWith("/") ? fileUrl + "/" : fileUrl;
    }


    /**
     * 下载文件
     *
     * @param response
     * @param fileUrl    文件的全部根路径  例如 ："D:/uploadFile/abcdfiles/Image/yyyy-MM-dd/BFA7FD01000000005A0F9BA10000000C.jpg"
     * @param fileSuffix 文件后缀名
     */
    public static boolean downloadFile(HttpServletResponse response, String fileUrl, String fileSuffix) {
        boolean b = false;
        InputStream is = null;
        OutputStream os = null;
        try {
//            log.info("下载文件的全部根路径=============>{}", fileUrl);
//            log.info("文件后缀名=====================>{}", fileSuffix);
            File file = new File(fileUrl);
            response.reset();
            response.setContentType("application/octet-stream;charset=UTF-8;");
            response.setHeader("Content-Disposition", (0 == 0 ? "" : "attachment;") +
                    "filename=" + new String((System.currentTimeMillis() + fileSuffix).getBytes(StandardCharsets.UTF_8), "ISO8859-1"));
            is = new FileInputStream(file);
            os = response.getOutputStream();
            int bytesRead = 0;
            byte[] buffer = new byte[2048];
            while ((bytesRead = is.read(buffer, 0, 2048)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            b = true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != is) is.close();
                if (null != os) os.close();
                os.flush();
                log.info("==> 刷新缓冲 关闭输出流  下载结束 <==");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return b;
    }

    /**
     * 看图片
     *
     * @param response
     * @param fileUrl    文件的全部根路径  例如 ："D:/uploadFile/abcdfiles/Image/yyyy-MM-dd/BFA7FD01000000005A0F9BA10000000C.jpg"
     * @param fileSuffix 文件后缀名
     */
    public static boolean showImgage(HttpServletResponse response, String fileUrl, String fileSuffix) {
        boolean b = false;
        response.setHeader("Content-type", fileSuffix);
        FileInputStream fis = null;
        ServletOutputStream sos = null;
        try {
//            log.info("查看图片的全部根路径=============>{}", fileUrl);
//            log.info("文件后缀名=====================>{}", fileSuffix);
            File file = new File(fileUrl);
            fis = new FileInputStream(file);
            sos = response.getOutputStream();
            byte[] buf = new byte[2048];
            int len = 0;
            while ((len = fis.read(buf)) != -1) {
                sos.write(buf, 0, len);
            }
            b = true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fis) fis.close();
                if (null != sos) sos.close();
                log.info("======> 刷新缓冲 关闭输出流 看图片方法结束 <======");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return b;
    } 
}
