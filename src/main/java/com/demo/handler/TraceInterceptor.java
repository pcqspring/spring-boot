package com.demo.handler;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import lombok.extern.log4j.Log4j2;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author xcw
 * @date 2022/5/22 14:50
 * @description 日志追踪ID拦截器
 */
@Log4j2
@Component
public class TraceInterceptor implements HandlerInterceptor {

    private static final String TRACE_ID_KEY = "traceId";

    private Snowflake snowflake = IdUtil.getSnowflake(1, 1);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String traceId = snowflake.nextIdStr();
        MDC.put(TRACE_ID_KEY, traceId);
        log.info("TraceInterceptor_traceId={}", traceId);
        return true;
    }


    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        MDC.clear();
    }
}
