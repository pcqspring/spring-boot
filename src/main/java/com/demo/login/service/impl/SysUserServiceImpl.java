package com.demo.login.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.authority.mapper.SysConfigMapper;
import com.demo.authority.mapper.SysUserRoleMapper;
import com.demo.authority.model.SysUser;
import com.demo.authority.model.SysUserRole;
import com.demo.authority.service.SysConfigService;
import com.demo.authority.service.SysUserRoleService;
import com.demo.enums.EnumResult;
import com.demo.login.mapper.SysUserMapper;
import com.demo.login.service.SysUserService;
import com.demo.utils.GUID;
import com.demo.utils.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Slf4j
@Service("loginSysUserService")
@Transactional
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Autowired
    @Qualifier(value = "loginSysUserMapper")
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysConfigService scService;  //新建用户时候 默认为 普通用户角色
    @Autowired
    private SysUserRoleService surService; // 中间表
    @Autowired
    private SysUserRoleMapper surMapper; // 中间表

    @Override
    public Result saveSysUserInfo(SysUser sysUser) {
        if (sysUser != null) {
            //注册账号  需要 先查看时候是否存在该账号  无则进入
            QueryWrapper<SysUser> qw = new QueryWrapper<>();
            qw.eq("LOGIN_NAME", sysUser.getLoginName());
            List<SysUser> sysUsers = sysUserMapper.selectList(qw);
            if (sysUsers.size() != 0)
                return new Result(EnumResult.REGISTER_NO);
            //账号密码不能为空
            if (StringUtils.isEmpty(sysUser.getLoginName()) || StringUtils.isEmpty(sysUser.getPassword())) {
                return new Result(EnumResult.REGISTER_NO_EMPTY);
            }
            //添加用戶信息
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sysUser.setCtime(sdf.format(new Date()));
            sysUser.setRstate("0");
            sysUser.setUserId(new GUID().toString());
            String password = sysUser.getPassword();
            //MD5加密
            try {
                password = DigestUtils.md5DigestAsHex(password.getBytes("utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            sysUser.setPassword(password);
            int insert = sysUserMapper.insert(sysUser);
            if (insert == 1) { //0000000000000000000000000000002 为 普通角色 id
                String roleId = scService.getConfigUserId("role.ordinary.id", "0000000000000000000000000000002");
                SysUserRole sysUserRole = new SysUserRole();
                sysUserRole.setUserId(sysUser.getUserId());
                sysUserRole.setRoleId(roleId);
                int i = surService.saveSysUserRole(sysUserRole);
                if (i == 1)
                    return new Result(EnumResult.REGISTER_OK);
            }
            return new Result(500, "注册失败请稍后重试");
        }
        return new Result(500, "未填写注册信息");
    }

    @Override
    public Result udpateSysUserInfo(SysUser sysUser) {
        if (sysUser != null) {
            QueryWrapper<SysUser> qw = new QueryWrapper<>();
            qw.eq("LOGIN_NAME", sysUser.getLoginName())
                    .eq("PHONE", sysUser.getPhone());
            //修改密码 查询账号是否存在
            List<SysUser> sysUsers = sysUserMapper.selectList(qw);
            if (sysUsers.size() == 0)
                return new Result(EnumResult.RETRIEVE_NO);
            SysUser newUser = sysUsers.get(0);
            //修改 用戶信息
            String password = sysUser.getPassword();
            //MD5加密
            try {
                password = DigestUtils.md5DigestAsHex(password.getBytes("utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            newUser.setPassword(password);
            int insert = sysUserMapper.updateById(newUser);
            if (insert == 1) {
                return new Result(EnumResult.RETRIEVE_OK);
            } else {
                return new Result(500, "修改失败请稍后重试");
            }
        }
        return new Result(500, "未填写修改信息");
    }

    /**
     * 登录查询数据库
     *
     * @param sysUser
     * @return
     */
    @Override
    public SysUser selectUserInfo(SysUser sysUser) {
        String password = sysUser.getPassword();
        //MD5加密
        try {
            password = DigestUtils.md5DigestAsHex(password.getBytes("utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        sysUser.setPassword(password);
        QueryWrapper<SysUser> qw = new QueryWrapper<>();
        qw.eq("LOGIN_NAME", sysUser.getLoginName())
                .eq("PASSWORD", sysUser.getPassword());
        List<SysUser> sysUsers = sysUserMapper.selectList(qw);
        if (sysUsers.size() == 0)
            return null;
        return sysUsers.get(0);
    }

    @Override
    public void addInfo() {
        SysUser sysUser = new SysUser();
        sysUser.setUserId("asdasdasdsdf");

        int insert = sysUserMapper.insert(sysUser);
        if (insert > 0) log.info("=======1=========>成功");
        else log.info("======1====>失败");

        int a = 0;
        int v = 0 / 0;
        SysUserRole sysUserRole = new SysUserRole();
        sysUserRole.setRoleId("asdasdasd");
        sysUserRole.setUserId(sysUser.getUserId());
        int insert1 = surMapper.insert(sysUserRole);
        if (insert > 0) log.info("=========2=======>成功");
        else log.info("======2====>失败");

    }
}
