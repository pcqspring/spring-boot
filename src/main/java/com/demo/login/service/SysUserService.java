package com.demo.login.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.authority.model.SysUser;
import com.demo.utils.Result;

public interface SysUserService extends IService<SysUser> {


   Result saveSysUserInfo(SysUser sysUser);

   Result udpateSysUserInfo(SysUser sysUser);

   SysUser selectUserInfo(SysUser sysUser);

    void addInfo();
}
