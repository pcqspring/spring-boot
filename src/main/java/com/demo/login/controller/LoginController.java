package com.demo.login.controller;

import com.demo.authority.model.SysRole;
import com.demo.authority.model.SysUser;
import com.demo.authority.service.SysConfigService;
import com.demo.authority.service.SysRoleService;
import com.demo.authority.service.SysUserRoleService;
import com.demo.enums.EnumResult;
import com.demo.login.service.SysUserService;
import com.demo.utils.RedisUtil;
import com.demo.utils.Result;
import com.demo.utils.SysUserUtils;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.util.List;

@Controller
@Slf4j
@Api(tags = "LoginController", value = "登录页面")
public class LoginController {


    @Resource
    private RedisUtil redisUtil;

    @Autowired
    @Qualifier(value = "loginSysUserService")
    private SysUserService sysUserSerivce;

    @Autowired
    private SysRoleService srService;

    @Autowired
    private SysConfigService scService;


    /**
     * 登陆后的首页面 index
     *
     * @param request
     * @param response
     * @return
     */
    @ApiIgnore
    @RequestMapping(value = {"", "/", "/index"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String index(
            HttpServletRequest request, HttpServletResponse response) {
        SysUser sysUser = SysUserUtils.getSysUser(request);
        String userName = sysUser.getUserName();
        List<SysRole> sysRoleList = sysUser.getSysRole();
        String administratorsId = scService.getConfigUserId("role.administrators.id", "0000000000000000000000000000001");
        request.setAttribute("administratorsId", administratorsId);
        request.setAttribute("userName", userName);
        request.setAttribute("sysRoleList", sysRoleList);
        return "/login/index";
    }


    /**
     * 登录页面
     *
     * @param request
     * @param response
     * @return
     */
    @ApiIgnore
    @RequestMapping(value = "/login", method = {RequestMethod.POST, RequestMethod.GET})
    public String login(
            HttpServletRequest request, HttpServletResponse response) {
        //检测 如果登录 则不让他登录第二次
        Object loginUser = request.getSession().getAttribute("loginUser");
        if (loginUser == null)   //没有登录
            return "/login/loginShow";
        SysUser sysUser = SysUserUtils.getSysUser(request);
        String userName = sysUser.getUserName();
        List<SysRole> sysRoleList = sysUser.getSysRole();
        String administratorsId = scService.getConfigUserId("role.administrators.id", "0000000000000000000000000000001");
        request.setAttribute("administratorsId", administratorsId);
        request.setAttribute("userName", userName);
        request.setAttribute("sysRoleList", sysRoleList);
        return "/login/index";
    }

    /**
     * 登录页面提交
     *
     * @param loginName
     * @param password
     * @param request
     * @param response
     * @param session
     * @return
     */
    @ApiOperation(value = "登录页面提交", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "loginName", value = "登录账号", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "password", value = "登录密码", dataType = "String", paramType = "query")
    })
    /*@ApiResponses({
            @ApiResponse(code = 200, message = "登录成功！", response = Result.class),
            @ApiResponse(code = 500, message = "登录失败！", response = Result.class)
    })*/
    @RequestMapping(value = "/loginAdmin", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Result<EnumResult> loginAdmin(@RequestParam(value = "loginName", defaultValue = "") String loginName,
                                         @RequestParam(value = "password", defaultValue = "") String password,
                                         HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        // 登录是 获取 账号密码 查询数据库
        if (StringUtils.isNotEmpty(loginName) && StringUtils.isNotEmpty(password)) {
            SysUser sysUser = new SysUser();
            sysUser.setLoginName(loginName);
            sysUser.setPassword(password);
            SysUser userInfo = sysUserSerivce.selectUserInfo(sysUser);
            if (userInfo != null) {
                userInfo.setPassword(""); //设置用户密码为空 否则会 被人盗取
                // 通过用户 获取 当前登陆人的权限 通过角色 获取 对应的 路径path 集合
                List<SysRole> sysRoleList = srService.findSysRoleList(userInfo.getUserId());
                userInfo.setSysRole(sysRoleList);
                //存入session
                session.setAttribute("loginUser", userInfo);
                session.setMaxInactiveInterval(3600);
                log.info("<=================登陆成功！=================>");
                return new Result(EnumResult.LOGIN_OK);
            }
        }
        log.info("<=================登陆失败,账号密码错误或者账户不存在！=================>");
//            request.setAttribute("msg", "账号密码错误或者账户不存在！");
        return new Result(EnumResult.LOGIN_NO);
    }


    /**
     * 注册页面
     *
     * @param request
     * @param response
     * @return
     */
    @ApiIgnore
    @RequestMapping(value = "/registerAdmin", method = {RequestMethod.POST, RequestMethod.GET})
    public String registerAdmin(
            HttpServletRequest request, HttpServletResponse response) {
        log.info("..............注册页面..............");
        return "/login/registerAdmin";
    }

    /**
     * 注册保存
     *
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "注册保存", httpMethod = "POST")
    @ApiImplicitParam(name = "sysUser", value = "实体类", dataType = "SysUser", paramType = "query")
/*    @ApiResponses({
            @ApiResponse(code = 200, message = "添加成功！", response = Result.class),
            @ApiResponse(code = 500, message = "添加失败！", response = Result.class)
    })*/
    @RequestMapping(value = "/registerSave", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Result<SysUser> registerSave(
            SysUser sysUser,
            HttpServletRequest request, HttpServletResponse response) {
        log.info("..............添加账号..............");
        Result<SysUser> result = sysUserSerivce.saveSysUserInfo(sysUser);
        return result;
    }


    /**
     * 找回密码 页面
     *
     * @param request
     * @param response
     * @return
     */
    @ApiIgnore
    @RequestMapping(value = "/retrievePassword", method = {RequestMethod.POST, RequestMethod.GET})
    public String retrievePassword(
            HttpServletRequest request, HttpServletResponse response) {
        log.info("..............找回密码..............");
        return "/login/retrievePassword";
    }

    /**
     * 找回密码 保存
     *
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "找回密码保存", httpMethod = "POST")
    @ApiImplicitParam(name = "sysUser", value = "实体类", dataType = "SysUser", paramType = "query")
/*    @ApiResponses({
            @ApiResponse(code = 200, message = "修改成功！", response = Result.class),
            @ApiResponse(code = 500, message = "修改失败！", response = Result.class)
    })*/
    @RequestMapping(value = "/retrieveSave", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Result<SysUser> retrieveSave(
            SysUser sysUser,
            HttpServletRequest request, HttpServletResponse response) {
        log.info("..............更新密码..............");
        Result<SysUser> result = sysUserSerivce.udpateSysUserInfo(sysUser);
        return result;
    }


    /**
     * 退出登录 清空session
     *
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "退出登录", httpMethod = "POST")
    @RequestMapping(value = "/logOut", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Result logOut(
            HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        log.info("..............退出登录..............");
        try {
            session.setMaxInactiveInterval(0);
            session.removeAttribute("loginUser");
//            session.invalidate();
            return new Result(EnumResult.LOGOUT_OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Result(EnumResult.LOGOUT_NO);
    }


    @ApiOperation(value = "测试事务", httpMethod = "POST")
    @RequestMapping(value = "addInfo")
    public Result<EnumResult> addInfo() {

        // sysUserSerivce.addInfo();
        return new Result<>(EnumResult.INSERT_OK);
    }


}
