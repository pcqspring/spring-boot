package com.demo.technology.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;

@Data
@TableName("tb_technology_stack")
@Alias("TbTechnologyStackBean")
@ApiModel("技术栈基本信息表")//技术栈详情信息表
public class TbTechnologyStackBean implements Serializable {


    @ApiModelProperty(dataType = "String",name = "rid", value = "技术栈基本信息表id")
    @TableId
    private String rid;
    @ApiModelProperty(dataType = "String",name = "technicalCode", value = "技术栈编码")
    @TableField("TECHNICAL_CODE")
    private String technicalCode;
    @ApiModelProperty(dataType = "String",name = "technicalName", value = "技术栈名称")
    @TableField("TECHNICAL_NAME")
    private String technicalName;
    @ApiModelProperty(dataType = "String",name = "technicalType", value = "技术类型(1.前端,2.后端,.3数据库4.运维)")
    @TableField("TECHNICAL_TYPE")
    private String technicalType;
    @ApiModelProperty(dataType = "String",name = "cuserName", value = "创建作者")
    @TableField("CUSER_NAME")
    private String cuserName;
    @ApiModelProperty(dataType = "String",name = "ctime", value = "创建时间")
    @TableField("CTIME")
    private String ctime;
    @ApiModelProperty(dataType = "String",name = "rstate", value = "是否删除  (0使用中,1刪除注銷)")
    @TableField("RSTATE")
    private String rstate;
    @ApiModelProperty(dataType = "String",name = "remarkes", value = "备注")
    @TableField("REMARKES")
    private String remarkes;
}
