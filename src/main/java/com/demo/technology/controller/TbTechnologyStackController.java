package com.demo.technology.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.demo.authority.model.SysApp;
import com.demo.authority.utils.PathAuthority;
import com.demo.enums.EnumResult;
import com.demo.technology.model.TbTechnologyStackBean;
import com.demo.technology.service.TbTechnologyStackService;
import com.demo.utils.QueryPage;
import com.demo.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@Slf4j
@RequestMapping("/technology")
@Api(tags = "TbTechnologyStackController", value = "技术栈基本信息路径")
public class TbTechnologyStackController {

    @Autowired
    private TbTechnologyStackService tbTechnologyStackService;


    /**
     * 技术栈基本信息页面
     *
     * @param request
     * @param response
     * @return
     */
    @ApiIgnore
    @RequestMapping(value = "/technologyStackView", method = {RequestMethod.POST, RequestMethod.GET})
    public String sysRoleShow(
            HttpServletRequest request, HttpServletResponse response) {
        return "technology/technologyStack/technologyStackView";
    }

    /**
     * 技术栈基本信息列表页面
     *
     * @param queryPage
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "技术栈基本信息列表页面",httpMethod = "POST")
    @ApiImplicitParam(name = "queryPage", value = "QueryPage分页实体类", dataType = "QueryPage")
    @RequestMapping(value = "/technologyStackList", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Result<Page<TbTechnologyStackBean>> technologyStackList(@RequestBody QueryPage queryPage, HttpServletRequest request, HttpServletResponse response) {
        try {
            Page<TbTechnologyStackBean> technologyStackList = tbTechnologyStackService.technologyStackList(queryPage);
            return new Result<>(EnumResult.SELECT_OK, technologyStackList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Result<>(EnumResult.SELECT_NO);
    }

}
