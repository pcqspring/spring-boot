package com.demo.technology.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.technology.mapper.TbTechnologyStackMapper;
import com.demo.technology.model.TbTechnologyStackBean;
import com.demo.technology.service.TbTechnologyStackService;
import com.demo.utils.QueryPage;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Optional;

@Service
@Transactional
@Slf4j
public class TbTechnologyStackServiceImpl extends ServiceImpl<TbTechnologyStackMapper, TbTechnologyStackBean> implements TbTechnologyStackService {

    @Autowired
    private TbTechnologyStackMapper tbTechnologyStackMapper;

    @Override
    public Page<TbTechnologyStackBean> technologyStackList(QueryPage queryPage) {
        QueryWrapper<TbTechnologyStackBean> queryWrapper = new QueryWrapper<>();
        Map<String, Object> dataInfo = queryPage.getData();
        //条件对象不为 null 则进入 拼接 where 条件查询
        Optional.ofNullable(dataInfo).ifPresent(data ->{
            queryWrapper.lambda().like(StringUtils.isNotEmpty((String) data.get("technicalName")),
                            TbTechnologyStackBean::getTechnicalName,data.get("technicalName"))
                    .like(StringUtils.isNotEmpty((String) data.get("ctime")),
                            TbTechnologyStackBean::getCtime,data.get("ctime"));
        });
        queryWrapper.eq("RSTATE", "0").orderByDesc("CTIME");
        //查询并分页
        Page<TbTechnologyStackBean> tbTechnologyStackBeanPage = tbTechnologyStackMapper.selectPage(new Page<>(queryPage.getCurrent(), queryPage.getSize()), queryWrapper);
        return tbTechnologyStackBeanPage;
    }
}
