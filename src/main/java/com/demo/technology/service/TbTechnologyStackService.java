package com.demo.technology.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.technology.model.TbTechnologyStackBean;
import com.demo.utils.QueryPage;

public interface TbTechnologyStackService extends IService<TbTechnologyStackBean> {
    Page<TbTechnologyStackBean> technologyStackList(QueryPage queryPage);
}
