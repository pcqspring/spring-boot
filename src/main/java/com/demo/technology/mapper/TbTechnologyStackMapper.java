package com.demo.technology.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.technology.model.TbTechnologyStackBean;

public interface TbTechnologyStackMapper extends BaseMapper<TbTechnologyStackBean> {
}
