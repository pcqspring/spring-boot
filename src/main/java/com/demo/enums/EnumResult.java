package com.demo.enums;

public enum EnumResult {

    LOGIN_OK(200, "登陆成功！"),
    LOGIN_NO(500, "账号密码错误或者账户不存在！"),
    LOGOUT_OK(200, "已退登录！"),
    LOGOUT_NO(500, "退出登录失败啦 ~>.<~ ！"),
    REGISTER_OK(200, "注册账号成功！"),
    REGISTER_NO(500, "注册账号失败账户存在！"),
    REGISTER_NO_EMPTY(500, "账号密码不能为空！"),
    RETRIEVE_OK(200, "修改密码成功！"),
    RETRIEVE_NO(500, "修改失败,账号或手机号不正确！"),
    SELECT_OK(200, "查询成功！"),
    SELECT_NO(500, "查询失败！"),
    UPDATE_OK(200, "修改成功！"),
    UPDATE_NO(500, "修改失败！"),
    INSERT_OK(200, "添加成功！"),
    INSERT_NO(500, "添加失败！"),
    DELETE_OK(200, "删除成功！"),
    DELETE_NO(500, "删除失败！"),
    PATH_NO(404, "找不到路径！"),
    PATH_UPLOAD(200, "上传成功！");
    private int code;
    private String magess;

    public int getCode() {
        return code;
    }

    public String getMagess() {
        return magess;
    }

    private EnumResult(int code, String magess) {
        this.code = code;
        this.magess = magess;
    }
}
