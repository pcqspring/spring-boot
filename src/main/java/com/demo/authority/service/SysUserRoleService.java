package com.demo.authority.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.authority.model.SysRole;
import com.demo.authority.model.SysUserRole;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface SysUserRoleService extends IService<SysUserRole> {


    int saveSysUserRole(SysUserRole sysUserRole);


    int deleteSysUserRole(SysUserRole sysUserRole);

    List<SysUserRole> getAdministrators(String userId, String roleId);
}
