package com.demo.authority.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.authority.model.SysApp;
import com.demo.authority.model.SysRole;
import com.demo.utils.QueryPage;

import java.util.List;

public interface SysRoleService extends IService<SysRole> {
    Page<SysRole> getSysRoleList(QueryPage queryPage);

    int saveSysRole(SysRole sysRole);

    List<SysRole> findSysRoleList();

    List<SysRole> findSysRoleList(String userId);

    List<SysRole> findSysRoleListTwo(String appId);

    SysRole findById(String roleId);

    List<SysApp> findSysAppListTwo(String roleId);

    int sysRoleDel(String roleId);

    int sysRoleEditSave(SysRole sysRole);
}
