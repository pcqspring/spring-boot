package com.demo.authority.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.authority.model.SysUser;
import com.demo.utils.QueryPage;

public interface SysUserService extends IService<SysUser> {

    Page<SysUser> findSysUserList(QueryPage queryPage);

    SysUser findById(String userId);

    int sysUserDel(String userId);

    int sysUserEditSave(SysUser sysUser);
}
