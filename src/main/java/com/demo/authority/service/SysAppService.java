package com.demo.authority.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.authority.model.SysApp;
import com.demo.utils.LayuiTree;
import com.demo.utils.QueryPage;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface SysAppService extends IService<SysApp> {

    Page<SysApp> getSysAppList(QueryPage queryPage);

    int saveSysApp(SysApp sysApp);

    List<SysApp> findSysAppList();

    List<SysApp> findSysAppList(String appId);

    SysApp findById(String appId);

    int sysAppEditSave(SysApp sysApp);

    int sysAppDel(String appId);

    LayuiTree getAppTreeList();

    LayuiTree getUserAppTreeList(HttpServletRequest request);
}
