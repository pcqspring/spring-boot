package com.demo.authority.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.authority.model.SysConfig;

public interface SysConfigService extends IService<SysConfig> {


    String getConfigUserId(String configCode, String configValue);


}
