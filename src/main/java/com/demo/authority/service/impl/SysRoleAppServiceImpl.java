package com.demo.authority.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.authority.mapper.SysAppMapper;
import com.demo.authority.mapper.SysRoleAppMapper;
import com.demo.authority.mapper.SysRoleMapper;
import com.demo.authority.model.SysApp;
import com.demo.authority.model.SysRole;
import com.demo.authority.model.SysRoleApp;
import com.demo.authority.service.SysRoleAppService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@Transactional
public class SysRoleAppServiceImpl extends ServiceImpl<SysRoleAppMapper, SysRoleApp> implements SysRoleAppService {

    @Autowired
    private SysRoleAppMapper sysRoleAppMapper;

    @Autowired
    private SysAppMapper sysAppMapper;
    @Autowired
    private SysRoleMapper sysRoleMapper;

    /**
     * 通过 角色id 查询路径
     *
     * @param sysRole
     * @return
     */
    @Override
    public List<SysApp> getSysApp(SysRole sysRole) {
        List<SysApp> sysAppList = new ArrayList<>();
        try {
            QueryWrapper<SysRoleApp> sysRoleAppWapper = new QueryWrapper<>();
            if (sysRole != null) {
                sysRoleAppWapper.eq("ROLE_ID", sysRole.getRoleId());
                List<SysRoleApp> sysRoleAppList = sysRoleAppMapper.selectList(sysRoleAppWapper);
                if(sysRoleAppList != null){
                    //获取 路径appid获取 数据集合
                    for (int j = 0; j < sysRoleAppList.size(); j++) {
                        SysRoleApp sysRoleApp = sysRoleAppList.get(j);
                        QueryWrapper<SysApp> sysAppWapper = new QueryWrapper<>();
                        sysAppWapper.eq("APP_ID", sysRoleApp.getAppId());
                        SysApp sysApp = sysAppMapper.selectOne(sysAppWapper);
                        sysAppList.add(sysApp);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sysAppList;
    }

    /**
     * 通过 路径id 查询角色
     *
     * @param sysApp
     * @return
     */
    @Override
    public List<SysRole> getSysApp(SysApp sysApp) {
        List<SysRole> sysRoleList = new ArrayList<>();
        try {
            //中间表构造器
            QueryWrapper<SysRoleApp> sysRoleAppWapper = new QueryWrapper<>();
            if (sysApp != null) {
                sysRoleAppWapper.lambda().eq(SysRoleApp::getAppId, sysApp.getAppId());
                List<SysRoleApp> sysRoleAppList = sysRoleAppMapper.selectList(sysRoleAppWapper);
                //获取 路径appid获取 数据集合
                for (int j = 0; j < sysRoleAppList.size(); j++) {
                    SysRoleApp sysRoleApp = sysRoleAppList.get(j);
                    QueryWrapper<SysRole> sysRoleWapper = new QueryWrapper<>();
                    sysRoleWapper.lambda().eq(SysRole::getRoleId, sysRoleApp.getRoleId());
                    SysRole sysRole = sysRoleMapper.selectOne(sysRoleWapper);
                    sysRoleList.add(sysRole);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sysRoleList;
    }


    /**
     * 添加
     *
     * @param sysRoleApp
     * @return
     */
    @Override
    public int saveSysRoleApp(SysRoleApp sysRoleApp) {
        // 传入 不为null 则添加权限
        try {
            if (sysRoleApp != null) {
                if (StringUtils.isNotEmpty(sysRoleApp.getRoleId()) && StringUtils.isNotEmpty(sysRoleApp.getAppId())) {
                    //如果有角色添加失败
                    QueryWrapper<SysRoleApp> surWrapper = new QueryWrapper<>();
                    surWrapper.eq("APP_ID", sysRoleApp.getAppId()).eq("ROLE_ID", sysRoleApp.getRoleId());
                    SysRoleApp sysRoleApp1 = sysRoleAppMapper.selectOne(surWrapper);
                    if (sysRoleApp1 == null) {
                        return sysRoleAppMapper.insert(sysRoleApp);
                    } else {
                        return -1;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 删除
     *
     * @param sysRoleApp
     * @return
     */
    @Override
    public int deleteSysRoleApp(SysRoleApp sysRoleApp) {
        // 传入 不为null 则添加权限
        try {
            if (sysRoleApp != null) {
                if (StringUtils.isNotEmpty(sysRoleApp.getRoleId()) && StringUtils.isNotEmpty(sysRoleApp.getAppId())) {
                    //如果有角色添加失败               QueryWrapper<SysRoleApp> surWrapper =
                    int i = sysRoleAppMapper.delete(new QueryWrapper<SysRoleApp>()
                            .lambda().eq(SysRoleApp::getAppId, sysRoleApp.getAppId())
                            .eq(SysRoleApp::getRoleId, sysRoleApp.getRoleId()));
                    if (i > 0)
                        return i;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }


}
