package com.demo.authority.service.impl;


import cn.hutool.core.lang.func.Func1;
import com.baomidou.mybatisplus.core.conditions.query.Query;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.authority.mapper.SysRoleMapper;
import com.demo.authority.mapper.SysUserMapper;
import com.demo.authority.mapper.SysUserRoleMapper;
import com.demo.authority.model.SysRole;
import com.demo.authority.model.SysUser;
import com.demo.authority.model.SysUserRole;
import com.demo.authority.service.SysRoleService;
import com.demo.authority.service.SysUserRoleService;
import com.demo.authority.service.SysUserService;
import com.demo.utils.ConditionUtils;
import com.demo.utils.QueryPage;
import com.demo.utils.SysUserUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.io.UnsupportedEncodingException;
import java.nio.file.Watchable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Autowired
    private SysRoleService sysRoleService;

    /**
     * 查询 用户 信息列表页面
     *
     * @param queryPage
     * @return
     */
    @Override
    public Page<SysUser> findSysUserList(QueryPage queryPage) {
        //创建条件构造器
        QueryWrapper<SysUser> su = new QueryWrapper<>();
        Map<String, Object> map = queryPage.getData();
        //条件 不为 null  则 进入 拼接 where 条件
        Optional.ofNullable(map).ifPresent(data -> {

            ConditionUtils.addCondition(su,data,"LIKE","userName","USER_NAME");
            ConditionUtils.addCondition(su,data,"LIKE","phone","PHONE");
            ConditionUtils.addCondition(su,data,"eq","loginName","LOGIN_NAME");
            ConditionUtils.addCondition(su,data,"LIKE","ctime","CTIME");
          /* su.lambda()
                    .like(StringUtils.isNotEmpty((String)data.get("userName")),SysUser::getUserName,data.get("userName"))
                    .like(StringUtils.isNotEmpty((String)data.get("phone")),SysUser::getPhone,data.get("phone"))
                    .like(StringUtils.isNotEmpty((String)data.get("loginName")),SysUser::getLoginName,data.get("loginName"))
                    .like(StringUtils.isNotEmpty((String)data.get("ctime")),SysUser::getCtime,data.get("ctime"));*/

        });
       // if (data != null) {
            /*data.forEach((key, value) -> {
                if (StringUtils.isNotEmpty((String) value)) {
                    switch (key) {
                        case "userName":
                            su.lambda().like(SysUser::getUserName, value);
                            break;
                        case "phone":
                            su.lambda().like(SysUser::getPhone, value);
                            break;
                        case "loginName":
                            su.lambda().like(SysUser::getLoginName, value);
                            break;
                        case "ctime":
                            su.lambda().like(SysUser::getCtime, value);
                            break;
                    }
                }
            });*/


    /*        String userName = (String) data.get("userName");
            String phone = (String) data.get("phone");
            String loginName = (String) data.get("loginName");
            String ctime = (String) data.get("ctime");

            if (StringUtils.isNotBlank(userName)) su.like("USER_NAME", userName);
            if (StringUtils.isNotBlank(phone)) su.like("PHONE", phone);
            if (StringUtils.isNotBlank(loginName)) su.like("LOGIN_NAME", loginName);
            if (StringUtils.isNotBlank(ctime)) su.like("CTIME", ctime);*/

        //}
        su.eq("RSTATE", "0").orderByAsc("CTIME");
        //分页查询数据
        Page<SysUser> sysUserPage = sysUserMapper.selectPage(new Page<>(queryPage.getCurrent(), queryPage.getSize()), su);
        List<SysUser> records = sysUserPage.getRecords();
        //records 数据集  通过 lambda表达式 遍历替换  0和 1的值
        List<SysUser> collect = records.stream().map((r) -> {
            if ("0".equals(r.getSex())) r.setSex("男");
            else r.setSex("女");
            return r;
        }).collect(Collectors.toList());
        sysUserPage.setRecords(collect);
        return sysUserPage;
    }

    /**
     * 查询用户个人信息
     *
     * @param userId
     * @return
     */
    @Override
    public SysUser findById(String userId) {
        try {
            if (StringUtils.isNotEmpty(userId)) {
                //获取基本信息
                SysUser sysUser = sysUserMapper.selectById(userId);
                List<SysRole> sysRoleList = sysRoleService.findSysRoleList(userId);
                sysUser.setSysRole(sysRoleList);
                //sysRoleList.clear();
                return sysUser;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new SysUser();
    }


    /**
     * 通过id 逻辑删除该用户
     *
     * @param userId
     * @return
     */
    @Override
    public int sysUserDel(String userId) {
        QueryWrapper<SysUser> suWrapper = new QueryWrapper<>();
        try {
            suWrapper.eq("USER_ID", userId);
            SysUser sysUser = sysUserMapper.selectOne(suWrapper);
            if (sysUser != null) {
                sysUser.setRstate("1");
                int i = sysUserMapper.updateById(sysUser);
                if (i > 0)
                    return i;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 编辑保存用户数据
     *
     * @param sysUser
     * @return
     */
    @Override
    public int sysUserEditSave(SysUser sysUser) {
        QueryWrapper<SysUser> suWapper = new QueryWrapper<>();
        try {
            if (sysUser != null) {
                if (StringUtils.isNotEmpty(sysUser.getUserId())) {
                    SysUser userInfo = sysUserMapper.selectById(sysUser.getUserId());
                    BeanUtils.copyProperties(sysUser, userInfo);
                    /*String password = userInfo.getPassword();
                    //MD5加密
                    try {
                        password = DigestUtils.md5DigestAsHex(password.getBytes("utf-8"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    userInfo.setPassword(password);*/
                    int i = sysUserMapper.updateById(userInfo);
                    if (i > 0)
                        return i;
                }
            }
        } catch (BeansException e) {
            e.printStackTrace();
        }
        return 0;
    }








}
