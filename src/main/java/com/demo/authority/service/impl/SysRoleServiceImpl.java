package com.demo.authority.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.authority.mapper.SysAppMapper;
import com.demo.authority.mapper.SysRoleAppMapper;
import com.demo.authority.mapper.SysRoleMapper;
import com.demo.authority.mapper.SysUserRoleMapper;
import com.demo.authority.model.*;
import com.demo.authority.service.SysRoleAppService;
import com.demo.authority.service.SysRoleService;
import com.demo.utils.DateUtils;
import com.demo.utils.GUID;
import com.demo.utils.QueryPage;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
@Slf4j
@Transactional
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    @Autowired
    private SysRoleMapper srMapper;
    @Autowired
    private SysUserRoleMapper surMapper;

    @Autowired
    private SysRoleAppService sysRoleAppService;
    @Autowired
    private SysRoleAppMapper sraMapper;
    @Autowired
    private SysAppMapper saMapper;

    /**
     * 查询 角色 数据
     *
     * @param queryPage
     * @return
     */
    @Override
    public Page<SysRole> getSysRoleList(QueryPage queryPage) {
        QueryWrapper<SysRole> srWrapper = new QueryWrapper<>();
        Map<String, Object> data = queryPage.getData();
        //条件对象不为 null 则进入 拼接 where 条件查询
        if (data != null) {
            String roleName = (String) data.get("roleName");
            String ctime = (String) data.get("ctime");
            if (StringUtils.isNotBlank(roleName)) srWrapper.like("ROLENAME", roleName);
            if (StringUtils.isNotBlank(ctime)) srWrapper.like("CTIME", ctime);
        }
        srWrapper.eq("RSTATE", "0").orderByAsc("CTIME");
        //查询并分页
        Page<SysRole> sysRolePage = srMapper.selectPage(new Page<>(queryPage.getCurrent(), queryPage.getSize()), srWrapper);
        return sysRolePage;
    }


    /**
     * 添加角色 信息表
     * 数据
     *
     * @param sysRole
     * @return
     */
    @Override
    public int saveSysRole(SysRole sysRole) {
        try {
            QueryWrapper<SysRole> srWapper = new QueryWrapper<>();
            srWapper.eq("ROLENAME", sysRole.getRoleName());
            SysRole roleInfo = srMapper.selectOne(srWapper);
            //角色名不存在则添加
            if (roleInfo == null) {
                sysRole.setRoleId(new GUID().toString());
                sysRole.setRstate("0");
                sysRole.setCtime(DateUtils.getCtime());
                return srMapper.insert(sysRole);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 获取全部的 部门角色
     *
     * @return
     */
    @Override
    public List<SysRole> findSysRoleList() {
        QueryWrapper<SysRole> sysRoleQueryWrapper = new QueryWrapper<>();
        sysRoleQueryWrapper
                //.ne("ROLE_ID", "0000000000000000000000000000001")
                .orderByAsc("CTIME");
        List<SysRole> SysRoleList = srMapper.selectList(sysRoleQueryWrapper);
        return SysRoleList;
    }


    /**
     * 通过 用户id 获取 对应的全部角色
     *
     * @param userId
     * @return
     */
    @Override
    public List<SysRole> findSysRoleList(String userId) {
        List<SysRole> sysRoleList = new ArrayList<>();
        try {
            if (StringUtils.isNotEmpty(userId)) {
                QueryWrapper<SysUserRole> sysUserRoleWrapper = new QueryWrapper<>();
                sysUserRoleWrapper.eq("USER_ID", userId);
                //通过角色ID 获取中间表集合
                List<SysUserRole> sysUserRoleList = surMapper.selectList(sysUserRoleWrapper);
                if(sysUserRoleList != null) {
                    //通过 中间表 获取 用户的 部门角色信息
                    for (int i = 0; i < sysUserRoleList.size(); i++) {
                        SysUserRole sysUserRole = sysUserRoleList.get(i);
                        QueryWrapper<SysRole> sysRoleWrapper = new QueryWrapper<>();
                        sysRoleWrapper.eq("ROLE_ID", sysUserRole.getRoleId());
                        //获取角色信息 集合
                        SysRole sysRole = srMapper.selectOne(sysRoleWrapper);
                        //获取角色id 对应的 角色路径集合
                        if(sysRole != null) {
                            List<SysApp> sysAppList = sysRoleAppService.getSysApp(sysRole);
                            sysRole.setSysApp(sysAppList);
                            sysRoleList.add(sysRole);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sysRoleList;
    }

    /**
     * 通过 路径id 获取 对应的全部角色
     *
     * @param appId
     * @return
     */
    @Override
    public List<SysRole> findSysRoleListTwo(String appId) {
        List<SysRole> sysRoleList = new ArrayList<>();
        try {
            if (StringUtils.isNotEmpty(appId)) {
                QueryWrapper<SysRoleApp> sysRoleAppWrapper = new QueryWrapper<>();
                sysRoleAppWrapper.eq("APP_ID", appId);
                //通过路径id 获取 中间表信息集合
                List<SysRoleApp> sysUserRoleList = sraMapper.selectList(sysRoleAppWrapper);
                //通过 中间表id 循环 获取 路径的 部门角色信息
                for (int i = 0; i < sysUserRoleList.size(); i++) {
                    SysRoleApp sysRoleApp = sysUserRoleList.get(i);
                    QueryWrapper<SysRole> sysRoleWrapper = new QueryWrapper<>();
                    sysRoleWrapper.eq("ROLE_ID", sysRoleApp.getRoleId());
                    //获取角色信息 集合
                    SysRole sysRole = srMapper.selectOne(sysRoleWrapper);
                    //获取角色id 对应的 角色路径集合
                    List<SysApp> sysAppList = sysRoleAppService.getSysApp(sysRole);
                    sysRole.setSysApp(sysAppList);
                    sysRoleList.add(sysRole);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sysRoleList;
    }

    @Override
    public SysRole findById(String roleId) {
        SysRole sysRole = new SysRole();
        try {
            sysRole = srMapper.selectById(roleId);
            //获取角色id 对应的 角色路径集合
            List<SysApp> sysAppList = sysRoleAppService.getSysApp(sysRole);
            sysRole.setSysApp(sysAppList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sysRole;
    }

    /**
     * 通过 角色id 获取 对应的全部路径
     *
     * @param roleId
     * @return
     */
    @Override
    public List<SysApp> findSysAppListTwo(String roleId) {
        List<SysApp> sysAppList = new ArrayList<>();
        try {
            if (StringUtils.isNotEmpty(roleId)) {
                QueryWrapper<SysRoleApp> sysRoleAppWrapper = new QueryWrapper<>();
                sysRoleAppWrapper.eq("ROLE_ID", roleId);
                //通过路径id 获取 中间表信息集合
                List<SysRoleApp> sysUserRoleList = sraMapper.selectList(sysRoleAppWrapper);
                //通过 中间表id 循环 获取 路径的 部门角色信息
                for (int i = 0; i < sysUserRoleList.size(); i++) {
                    SysRoleApp sysRoleApp = sysUserRoleList.get(i);
                    QueryWrapper<SysApp> sysRoleWrapper = new QueryWrapper<>();
                    sysRoleWrapper.eq("APP_ID", sysRoleApp.getAppId());
                    //获取角色信息 集合
                    SysApp sysApp = saMapper.selectOne(sysRoleWrapper);
                    //获取角色id 对应的 角色路径集合
                    List<SysRole> sysRoleList = sysRoleAppService.getSysApp(sysApp);
                    sysApp.setSysRole(sysRoleList);
                    sysAppList.add(sysApp);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sysAppList;
    }

    /**
     * 编辑保存角色数据
     *
     * @param sysRole
     * @return
     */
    @Override
    public int sysRoleEditSave(SysRole sysRole) {
        try {
            if (sysRole != null) {
                if (StringUtils.isNotEmpty(sysRole.getRoleId())) {
                    SysRole roleInfo = srMapper.selectById(sysRole.getRoleId());
                    BeanUtils.copyProperties(sysRole, roleInfo);
                    int i = srMapper.updateById(roleInfo);
                    if (i > 0)
                        return i;
                }
            }
        } catch (BeansException e) {
            e.printStackTrace();
        }
        return 0;
    }


    /**
     * 通过id 逻辑删除该角色
     *
     * @param roleId
     * @return
     */
    @Override
    public int sysRoleDel(String roleId) {
        QueryWrapper<SysRole> suWrapper = new QueryWrapper<>();
        try {
            suWrapper.eq("ROLE_ID", roleId);
            SysRole sysRole = srMapper.selectOne(suWrapper);
            if (sysRole != null) {
                sysRole.setRstate("1");
                int i = srMapper.updateById(sysRole);
                if (i > 0)
                    return i;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}