package com.demo.authority.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.Query;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.authority.mapper.SysAppMapper;
import com.demo.authority.mapper.SysRoleAppMapper;
import com.demo.authority.mapper.SysUserMapper;
import com.demo.authority.model.SysApp;
import com.demo.authority.model.SysRole;
import com.demo.authority.model.SysUser;
import com.demo.authority.service.SysAppService;
import com.demo.authority.service.SysConfigService;
import com.demo.authority.service.SysRoleAppService;
import com.demo.authority.service.SysUserService;
import com.demo.utils.DateUtils;
import com.demo.utils.GUID;
import com.demo.utils.LayuiTree;
import com.demo.utils.QueryPage;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
@Slf4j
@Transactional
public class SysAppServiceImpl extends ServiceImpl<SysAppMapper, SysApp> implements SysAppService {

    @Autowired
    private SysAppMapper sysAppMapper;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysConfigService sConfigService;

    @Autowired
    private SysRoleAppMapper sysRoleAppMapper;

    @Autowired
    private SysRoleAppService sysRoleAppService;


    /**
     * 分页查询
     *
     * @param queryPage
     * @return
     */
    @Override
    public Page<SysApp> getSysAppList(QueryPage queryPage) {
        QueryWrapper<SysApp> srWrapper = new QueryWrapper<>();
        Map<String, Object> data = queryPage.getData();
        //条件对象不为 null 则进入 拼接 where 条件查询
        if (data != null) {
            String appName = (String) data.get("appName");
            String ctime = (String) data.get("ctime");
            if (StringUtils.isNotBlank(appName)) srWrapper.like("APP_NAME", appName);
            if (StringUtils.isNotBlank(ctime)) srWrapper.like("CTIME", ctime);
        }
        srWrapper.eq("RSTATE", "0").orderByDesc("CTIME");
        //查询并分页
        Page<SysApp> SysAppPage = sysAppMapper.selectPage(new Page<>(queryPage.getCurrent(), queryPage.getSize()), srWrapper);
        return SysAppPage;
    }

    /**
     * 添加
     *
     * @param sysApp
     * @return
     */
    @Override
    public int saveSysApp(SysApp sysApp) {
        try {
            QueryWrapper<SysApp> srWapper = new QueryWrapper<>();
            srWapper.eq("APP_ID", sysApp.getAppId());
            SysApp appInfo = sysAppMapper.selectOne(srWapper);
            //角色名不存在则添加
            if (appInfo == null) {
                sysApp.setAppId(new GUID().toString());
                sysApp.setRstate("0");
                sysApp.setCtime(DateUtils.getCtime());
                return sysAppMapper.insert(sysApp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 查询全部
     *
     * @return
     */
    @Override
    public List<SysApp> findSysAppList() {
        QueryWrapper<SysApp> SysAppQueryWrapper = new QueryWrapper<>();
        SysAppQueryWrapper
                //.ne("ROLE_ID", "0000000000000000000000000000001")
                .orderByAsc("CTIME");
        List<SysApp> SysAppList = sysAppMapper.selectList(SysAppQueryWrapper);
        return SysAppList;
    }


    @Override
    public List<SysApp> findSysAppList(String appId) {
        List<SysApp> sysAppList = new ArrayList<>();
//        try {
//            if (StringUtils.isNotEmpty(appId)) {
//                QueryWrapper<SysRoleApp> sysRoleAppWrapper = new QueryWrapper<>();
//                sysRoleAppWrapper.eq("APP_ID", appId);
//                //获取路径ID集合
//                List<SysRoleApp> sysRoleAppList = sysRoleAppMapper.selectList(sysRoleAppWrapper);
//                //获取 路径的 部门角色信息
//                for (int i = 0; i < sysRoleAppList.size(); i++) {
//                    SysRoleApp sysRoleApp = sysRoleAppList.get(i);
//                    QueryWrapper<SysApp> sysRoleWrapper = new QueryWrapper<>();
//                    sysRoleWrapper.eq("ROLE_ID", sysRoleApp.getRoleId());
//                    //获取角色信息 集合
//                    SysApp sysApp = sysAppMapper.selectOne(sysRoleWrapper);
//                    //获取路径id 对应的 路径路径集合
////                    List<SysApp> sysAppList = sysRoleAppService.getSysApp(sysApp);
////                    sysApp.setSysApp(sysAppList);
//                    sysAppList.add(sysApp);
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        return sysAppList;
    }

    /**
     * 根据id查询
     *
     * @param appId
     * @return
     */
    @Override
    public SysApp findById(String appId) {
        try {
            if (StringUtils.isNotEmpty(appId)) {
                //获取基本信息
                SysApp sysApp = sysAppMapper.selectById(appId);
                //通过路径id 获取 当前对应的角色
                List<SysRole> sysRoleList = sysRoleAppService.getSysApp(sysApp);
                sysApp.setSysRole(sysRoleList);
                return sysApp;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new SysApp();
    }

    /**
     * 修改保存
     *
     * @param sysApp
     * @return
     */
    @Override
    public int sysAppEditSave(SysApp sysApp) {
        QueryWrapper<SysApp> suWapper = new QueryWrapper<>();
        try {
            if (sysApp != null) {
                if (StringUtils.isNotEmpty(sysApp.getAppId())) {
                    SysApp appInfo = sysAppMapper.selectById(sysApp.getAppId());
                    BeanUtils.copyProperties(sysApp, appInfo);
                    int i = sysAppMapper.updateById(appInfo);
                    if (i > 0)
                        return i;
                }
            }
        } catch (BeansException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 删除
     *
     * @param appId
     * @return
     */
    @Override
    public int sysAppDel(String appId) {
        QueryWrapper<SysApp> suWrapper = new QueryWrapper<>();
        try {
            suWrapper.eq("APP_ID", appId);
            SysApp sysApp = sysAppMapper.selectOne(suWrapper);
            if (sysApp != null) {
                sysApp.setRstate("1");
                int i = sysAppMapper.updateById(sysApp);
                if (i > 0)
                    return i;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 获取 树结构
     *
     * @return
     */
    @Override
    public LayuiTree getUserAppTreeList(HttpServletRequest request) {
        // 登录成功之后，应该有用户的session
        HttpSession session = request.getSession();
        SysUser loginUser = (SysUser) session.getAttribute("loginUser");
        SysUser sysUser = sysUserService.findById(loginUser.getUserId());
        List<SysRole> sysRoleList = sysUser.getSysRole();
        List<String> urlIdList = new ArrayList<>();
        sysRoleList.stream()//获取角色
                .forEach(sysRole -> sysRole.getSysApp().stream()// 通过角色 获取路径集合
                        .forEach(sysApp -> urlIdList.add(sysApp.getAppId())));//获取路径放入路径集合
        List<String> urlIds = urlIdList.stream().distinct().collect(Collectors.toList());
        //获取顶级id
        String persentTreeId = sConfigService.getConfigUserId("app.persentTree.id", "00000000000000000000000000TREE1");
        LayuiTree layuiTree = new LayuiTree();
        QueryWrapper<SysApp> sysAppWrapper = new QueryWrapper<>();
        sysAppWrapper.lambda().eq(SysApp::getAppId, persentTreeId).orderByAsc(SysApp::getAppOrder);
        //查询结果
        SysApp sysApp = sysAppMapper.selectOne(sysAppWrapper);
        layuiTree.setId(persentTreeId);          //顶级点id
        layuiTree.setTitle(sysApp.getAppName()); //顶级点名称
        layuiTree.setUrl(sysApp.getAppPath());
        List<LayuiTree> sysAppTreeList = getUserSysAppTreeList(layuiTree,urlIds);//顶级节点
        boolean b = false;
        for (int i = 0; i < urlIds.size(); i++) {
            if(urlIds.get(i).equals(persentTreeId)){
                b = true;
                break;
            }
        }
        if(b) {
            layuiTree.setChildren(sysAppTreeList); //数据传入顶级节点 集合
        }
        return layuiTree;
    } /**
     * 获取 树结构
     *
     * @return
     */
    @Override
    public LayuiTree getAppTreeList() {
        //获取顶级id
        String persentTreeId = sConfigService.getConfigUserId("app.persentTree.id", "00000000000000000000000000TREE1");
        LayuiTree layuiTree = new LayuiTree();
        QueryWrapper<SysApp> sysAppWrapper = new QueryWrapper<>();
        sysAppWrapper.lambda().eq(SysApp::getAppId, persentTreeId).orderByAsc(SysApp::getAppOrder);
        //查询结果
        SysApp sysApp = sysAppMapper.selectOne(sysAppWrapper);
        layuiTree.setId(persentTreeId);          //顶级点id
        layuiTree.setTitle(sysApp.getAppName()); //顶级点名称
        layuiTree.setUrl(sysApp.getAppPath());
        List<LayuiTree> sysAppTreeList = getSysAppTreeList(layuiTree);//顶级节点
        layuiTree.setChildren(sysAppTreeList); //数据传入顶级节点 集合
        return layuiTree;
    }

    /**
     * 递归调用  查询 tree树
     *
     * @param layuiTree //传入节点
     * @return
     */
    public List<LayuiTree> getSysAppTreeList(LayuiTree layuiTree) {
        QueryWrapper<SysApp> sysAppWrapper = new QueryWrapper<>();
        sysAppWrapper.lambda().eq(SysApp::getAppidParent, layuiTree.getId()).orderByAsc(SysApp::getAppOrder);
        //查询结果
        List<SysApp> sysAppList = sysAppMapper.selectList(sysAppWrapper);
        //创建 子节点集合
        List<LayuiTree> childrenList = new ArrayList<>();
        //结果 不为null 进入
        if (sysAppList != null) {
            //通过 id 查询 结果
            for (int i = 0; i < sysAppList.size(); i++) {
                SysApp sysApp = sysAppList.get(i);      //获取 集合数据
                LayuiTree sonTree = new LayuiTree();    //创建子节点
                sonTree.setId(sysApp.getAppId());       //子节点id
                sonTree.setTitle(sysApp.getAppName());  //子节点名称
                sonTree.setUrl(sysApp.getAppPath());
                List<LayuiTree> sonTreeList = getSysAppTreeList(sonTree);//子节点的 子节点集合F
                sonTree.setChildren(sonTreeList);       //子节点集合
                childrenList.add(sonTree);//收集 子节点
            }
        }
        return childrenList;
    }

    /**
     * 递归调用  查询 tree树
     *
     * @param layuiTree //传入节点
     * @return
     */
    public List<LayuiTree> getUserSysAppTreeList(LayuiTree layuiTree,List<String> urlIds) {
        QueryWrapper<SysApp> sysAppWrapper = new QueryWrapper<>();
        sysAppWrapper.lambda().eq(SysApp::getAppidParent, layuiTree.getId()).orderByAsc(SysApp::getAppOrder);
        //查询结果
        List<SysApp> sysAppList = sysAppMapper.selectList(sysAppWrapper);
        //创建 子节点集合
        List<LayuiTree> childrenList = new ArrayList<>();
        //结果 不为null 进入
        if (sysAppList != null) {
            //通过 id 查询 结果
            for (int i = 0; i < sysAppList.size(); i++) {
                SysApp sysApp = sysAppList.get(i);      //获取 集合数据
                String appId = sysApp.getAppId();
                LayuiTree sonTree = new LayuiTree();    //创建子节点
                sonTree.setId(appId);       //子节点id
                sonTree.setTitle(sysApp.getAppName());  //子节点名称
                sonTree.setUrl(sysApp.getAppPath());
                List<LayuiTree> sonTreeList = getUserSysAppTreeList(sonTree,urlIds);//子节点的 子节点集合F
                boolean b = false;
                for (int j = 0; j < urlIds.size(); j++) {
                    if(urlIds.get(j).equals(appId)){
                        b = true;
                        break;
                    }
                }
                sonTree.setChildren(sonTreeList);       //子节点集合
                if(b) {
                    childrenList.add(sonTree);//收集 子节点
                }
            }
        }
        return childrenList;
    }

}
