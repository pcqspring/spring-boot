package com.demo.authority.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.authority.mapper.SysRoleMapper;
import com.demo.authority.mapper.SysUserRoleMapper;
import com.demo.authority.model.SysUserRole;
import com.demo.authority.service.SysUserRoleService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@Slf4j
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {

    @Autowired
    private SysUserRoleMapper surMapper;

    @Autowired
    private SysRoleMapper srMapper;


    /**
     * 添加权限
     *
     * @param sysUserRole
     * @return
     */
    @Override
    public int saveSysUserRole(SysUserRole sysUserRole) {
        // 传入 不为null 则添加权限
        try {
            if (sysUserRole != null) {
                if (StringUtils.isNotEmpty(sysUserRole.getRoleId()) && StringUtils.isNotEmpty(sysUserRole.getUserId())) {
                    //如果有角色添加失败
                    QueryWrapper<SysUserRole> surWrapper = new QueryWrapper<>();
                    surWrapper.eq("USER_ID", sysUserRole.getUserId()).eq("ROLE_ID", sysUserRole.getRoleId());
                    SysUserRole sysUserRole1 = surMapper.selectOne(surWrapper);
                    if (sysUserRole1 == null) {
                        return surMapper.insert(sysUserRole);
                    } else {
                        return -1;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 删除 当前用户的 角色
     *
     * @param sysUserRole
     * @return
     */
    @Override
    public int deleteSysUserRole(SysUserRole sysUserRole) {
        // 传入 不为null 则添加权限
        try {
            if (sysUserRole != null) {
                if (StringUtils.isNotEmpty(sysUserRole.getRoleId()) && StringUtils.isNotEmpty(sysUserRole.getUserId())) {
                    //如果有角色添加失败
                    QueryWrapper<SysUserRole> surWrapper = new QueryWrapper<>();
                    surWrapper.eq("USER_ID", sysUserRole.getUserId()).eq("ROLE_ID", sysUserRole.getRoleId());
                    int i = surMapper.delete(surWrapper);
                    if (i > 0)
                        return i;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 查询当前用户是否有部门角色
     *
     * @return
     */
    @Override
    public List<SysUserRole> getAdministrators(String userId, String roleId) {
        QueryWrapper<SysUserRole> sysUserRoleWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(userId) && StringUtils.isNotEmpty(roleId)) {
            sysUserRoleWrapper.
                    eq("USER_ID", userId)
                    .eq("ROLE_ID", roleId) ;
            List<SysUserRole> sysUserRoleList = surMapper.selectList(sysUserRoleWrapper);
            return sysUserRoleList;
        }
        return new ArrayList<>();
    }

}
