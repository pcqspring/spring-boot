package com.demo.authority.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.authority.mapper.SysConfigMapper;
import com.demo.authority.model.SysConfig;
import com.demo.authority.service.SysConfigService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@Transactional
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements SysConfigService {

    @Autowired
    private SysConfigMapper scMapper;


    /**
     * 通过 config code  查询配置表 获取 配置 值
     *
     * @param configCode
     * @param configValue
     * @return
     */
    @Override
    public String getConfigUserId(String configCode, String configValue) {
        //创建条件构造器
        QueryWrapper<SysConfig> scWapper = new QueryWrapper<>();
        //如果 条件为空则返回 configValue
        if (StringUtils.isEmpty(configCode))
            return configValue;
        scWapper.eq("CONFIG_CODE", configCode);
        SysConfig sysConfig = scMapper.selectOne(scWapper);
        //如果 查询返回的对象 为null 则 也返回 configValue
        if (sysConfig != null)
            return sysConfig.getConfigValue();
        return configValue;
    }
}
