package com.demo.authority.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.authority.model.SysApp;
import com.demo.authority.model.SysRole;
import com.demo.authority.model.SysRoleApp;

import java.util.List;

public interface SysRoleAppService extends IService<SysRoleApp> {


    List<SysApp> getSysApp(SysRole sysRole);
    List<SysRole> getSysApp(SysApp sysApp);

    int saveSysRoleApp(SysRoleApp sysRoleApp);

    int deleteSysRoleApp(SysRoleApp sysRoleApp);
}
