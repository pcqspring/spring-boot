package com.demo.authority.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@TableName("sys_app") //页面 左侧菜单 路径表
@ApiModel("路径表")
public class SysApp implements Serializable {

    @ApiModelProperty(name = "appId", value = "路径表id")
    @TableId
    private String appId;

    @ApiModelProperty(name = "appidParent", value = "上一级app路径节点id")
    @TableField("APPID_PARENT")
    private String appidParent;

    @ApiModelProperty(name = "appidParentName", value = "上一级app路径节点名称")
    @TableField("APPID_PARENT_NAME")
    private String appidParentName;

    @ApiModelProperty(name = "appName", value = "路径名称")
    @TableField("APP_NAME")
    private String appName;

    @ApiModelProperty(name = "appPath", value = "请求路径")
    @TableField("APP_PATH")
    private String appPath;

    @ApiModelProperty(name = "appOrder", value = "路径序号", example = "0")
    @TableField("APP_ORDER")
    private Integer appOrder;

    @ApiModelProperty(name = "ctime", value = "创建时间")
    @TableField("CTIME")
    private String ctime;

    @ApiModelProperty(name = "rstate", value = "是否删除  (0使用中,1刪除注銷)")
    @TableField("RSTATE")
    private String rstate;

    @ApiModelProperty(name = "sysRole", value = " 角色表关联集合")
    @TableField(exist = false) //：表示该属性不为数据库表字段，但又是必须使用的。
    private List<SysRole> sysRole;
}
