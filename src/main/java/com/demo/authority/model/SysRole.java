package com.demo.authority.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@TableName("sys_role") //角色权限表
@ApiModel("角色权限表")
public class SysRole implements Serializable {

    @ApiModelProperty(name = "roleId", value = "角色id")
    @TableId
    private String roleId;

    @ApiModelProperty(name = "roleName", value = "角色名称")
    @TableField("ROLENAME")
    private String roleName;

    @ApiModelProperty(name = "ctime", value = "创建时间")
    @TableField("CTIME")
    private String ctime;

    @ApiModelProperty(name = "rstate", value = "是否删除 (0使用中,1刪除注銷)")
    @TableField("RSTATE")
    private String rstate;

    @ApiModelProperty(name = "remarkes", value = "备注")
    @TableField("REMARKES")
    private String remarkes;

    @ApiModelProperty(name = "sysApp", value = " 路径表关联集合")
    @TableField(exist = false) //：表示该属性不为数据库表字段，但又是必须使用的。
    private List<SysApp> sysApp;

}
