package com.demo.authority.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@TableName("sys_user_role")// 用户 角色 中间表  ==》用户 可以有多个角色所以  多对多关系
@ApiModel("用户、角色中间表")
public class SysUserRole implements Serializable {

    @ApiModelProperty(name = "userId", value = "用户id")
    @TableField("USER_ID")
    private String userId;

    @ApiModelProperty(name = "roleId", value = "角色id")
    @TableField("ROLE_ID")
    private String roleId;
}
