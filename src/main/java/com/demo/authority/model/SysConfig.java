package com.demo.authority.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@TableName("sys_config") //配置表
@ApiModel("配置表")
public class SysConfig implements Serializable {

    @ApiModelProperty(name = "configId", value = "配置表id")
    @TableId
    private String configId;

    @ApiModelProperty(name = "configCode", value = "编码code")
    @TableField("CONFIG_CODE")
    private String configCode;

    @ApiModelProperty(name = "configName", value = "名称")
    @TableField("CONFIG_NAME")
    private String configName;

    @ApiModelProperty(name = "configValue", value = "值")
    @TableField("CONFIG_VALUE")
    private String configValue;

    @ApiModelProperty(name = "ctrime", value = "创建时间")
    @TableField("CTRIME")
    private String ctrime;

    @ApiModelProperty(name = "remarkes", value = "备注")
    @TableField("REMARKES")
    private String remarkes;


}
