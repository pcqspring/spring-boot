package com.demo.authority.model;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_user") //用户基本信息表
@ApiModel("用户基本信息表")
public class SysUser implements Serializable {
    @ApiModelProperty(name = "userId", value = "用户基本信息表id")
    @TableId
    private String userId;

    @ApiModelProperty(name = "userName", value = "姓名")
    @TableField("USER_NAME")
    private String userName;

    @ApiModelProperty(name = "sex", value = "性别")
    @TableField("SEX")
    private String sex;

    @ApiModelProperty(name = "address", value = "地址")
    @TableField("ADDRESS")
    private String address;

    @ApiModelProperty(name = "phone", value = "电话")
    @TableField("PHONE")
    private String phone;

    @ApiModelProperty(name = "loginName", value = "用户账号")
    @TableField("LOGIN_NAME")
    private String loginName;

    @ApiModelProperty(name = "password", value = "用户密码")
    @TableField("PASSWORD")
    private String password;

    @ApiModelProperty(name = "roleId", value = "角色id")
    @TableField("ROLE_ID")
    private String roleId;

    @ApiModelProperty(name = "roleName", value = "角色名称")
    @TableField("ROLE_NAME")
    private String roleName;

    @ApiModelProperty(name = "ctime", value = "创建时间")
    @TableField("CTIME")
    private String ctime;

    @ApiModelProperty(name = "rstate", value = "是否注销 (0使用中,1刪除注銷)")
    @TableField("RSTATE")
    private String rstate;

    @ApiModelProperty(name = "sysRole", value = " 角色表关联集合")
    @TableField(exist = false) //：表示该属性不为数据库表字段，但又是必须使用的。
    private List<SysRole> sysRole;

}
