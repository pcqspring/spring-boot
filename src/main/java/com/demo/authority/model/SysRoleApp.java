package com.demo.authority.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@TableName("sys_role_app") // 角色路径 中间表  ==》角色 可以有多个路径所以  多对多关系
@ApiModel("角色、路径中间表")
public class SysRoleApp implements Serializable {

    @ApiModelProperty(name = "roleId", value = "角色id")
    @TableField("ROLE_ID")
    private String roleId;

    @ApiModelProperty(name = "APP_ID",value = "路径id")
    @TableField("APP_ID")
    private String appId;

}
