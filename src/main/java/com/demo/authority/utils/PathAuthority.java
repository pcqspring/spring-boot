package com.demo.authority.utils;

//当前包下 路径工具类
public class PathAuthority {


    public static final String AUTHORITY = "/authority";


    public static final String SYS_ROLE = AUTHORITY + "/sysRole";
    public static final String SYS_USER = AUTHORITY + "/sysUser";
    public static final String SYS_APP = AUTHORITY + "/sysApp";
}
