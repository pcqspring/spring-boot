package com.demo.authority.utils;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.demo.authority.mapper.SysConfigMapper;
import com.demo.authority.model.SysConfig;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class SysConfigUtil {

    @Autowired
    private SysConfigMapper sysConfigMapper;

    private static SysConfigUtil configUtil;

    @Value("${server.servlet.context-path}")
    private String contextPath;

    @PostConstruct
    public void init(){
        configUtil = this;
        configUtil.sysConfigMapper = this.sysConfigMapper;
        configUtil.contextPath = this.contextPath;
    }

    public static String getContextPath(){
        return configUtil.contextPath;
    }

    public static String getConfigUserId(String configCode, String configValue) {
        //创建条件构造器
        QueryWrapper<SysConfig> scWapper = new QueryWrapper<>();
        //如果 条件为空则返回 configValue
        if (StringUtils.isEmpty(configCode))
            return configValue;
        scWapper.eq("CONFIG_CODE", configCode);
        SysConfig sysConfig = configUtil.sysConfigMapper.selectOne(scWapper);
        //如果 查询返回的对象 为null 则 也返回 configValue
        if (sysConfig != null)
            return sysConfig.getConfigValue();
        return configValue;
    }
}