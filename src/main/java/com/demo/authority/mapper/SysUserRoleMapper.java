package com.demo.authority.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.authority.model.SysUserRole;

public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
}
