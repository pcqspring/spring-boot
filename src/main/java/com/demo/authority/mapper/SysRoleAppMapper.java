package com.demo.authority.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.authority.model.SysRoleApp;

public interface SysRoleAppMapper extends BaseMapper<SysRoleApp> {
}
