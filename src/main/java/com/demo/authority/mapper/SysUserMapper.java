package com.demo.authority.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.authority.model.SysUser;

public interface SysUserMapper extends BaseMapper<SysUser> {
}
