package com.demo.authority.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.authority.model.SysRole;
import com.demo.utils.QueryPage;

public interface SysRoleMapper extends BaseMapper<SysRole> {
}
