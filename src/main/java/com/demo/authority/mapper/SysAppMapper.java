package com.demo.authority.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.authority.model.SysApp;

public interface SysAppMapper extends BaseMapper<SysApp> {

}
