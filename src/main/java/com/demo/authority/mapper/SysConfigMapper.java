package com.demo.authority.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.authority.model.SysConfig;

public interface SysConfigMapper extends BaseMapper<SysConfig> {
}
