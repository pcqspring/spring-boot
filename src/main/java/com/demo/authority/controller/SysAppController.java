package com.demo.authority.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.demo.authority.model.SysApp;
import com.demo.authority.model.SysRole;
import com.demo.authority.model.SysUser;
import com.demo.authority.model.SysUserRole;
import com.demo.authority.service.*;
import com.demo.authority.utils.PathAuthority;
import com.demo.enums.EnumResult;
import com.demo.utils.LayuiTree;
import com.demo.utils.QueryPage;
import com.demo.utils.Result;
import com.demo.utils.SysUserUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@Slf4j
@RequestMapping("/authority")
@Api(tags = "SysAppController", value = "路径配置")
public class SysAppController {

    @Autowired
    private SysAppService sysAppService;

    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private SysUserRoleService sysUserRoleService;

    @Autowired
    private SysRoleAppService sysRoleAppService;

    @Autowired
    private SysConfigService sysConfigService;

    /**
     * 路径页面
     *
     * @param request
     * @param response
     * @return
     */
    @ApiIgnore
    @RequestMapping(value = "/sysAppView", method = {RequestMethod.POST, RequestMethod.GET})
    public String sysAppShow(
            HttpServletRequest request, HttpServletResponse response) {
        return PathAuthority.SYS_APP + "/sysAppView";
    }

    /**
     * 路劲列表页面
     *
     * @param queryPage
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "路径列表页面")
    @ApiImplicitParam(name = "queryPage", value = "QueryPage分页实体类", dataType = "QueryPage")
    @RequestMapping(value = "/sysAppList", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Result<Page<SysApp>> sysAppList(@RequestBody QueryPage queryPage, HttpServletRequest request, HttpServletResponse response) {
        try {
            Page<SysApp> sysAppList = sysAppService.getSysAppList(queryPage);
            return new Result<>(EnumResult.SELECT_OK, sysAppList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Result<>(EnumResult.SELECT_NO);
    }


    /**
     * 查看路径基本信息
     *
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "查看路径基本信息", httpMethod = "POST")
    @ApiImplicitParam(dataType = "String",name = "appId", value = "路径id")
    @RequestMapping(value = "/sysAppShow", method = {RequestMethod.POST, RequestMethod.GET})
    public String sysAppShow(
            @RequestParam(value = "appId", defaultValue = "", required = true) String appId,
            HttpServletRequest request, HttpServletResponse response) {

        //个人信息数据
        SysApp sysApp = sysAppService.findById(appId);
        request.setAttribute("sysApp", sysApp);
        return PathAuthority.SYS_APP + "/sysAppShow";
    }


    /**
     * 路径添加页面
     *
     * @param request
     * @param response
     * @return
     */
    @ApiIgnore
    @RequestMapping(value = "/sysAppAdd", method = {RequestMethod.POST, RequestMethod.GET})
    public String sysRoleAdd(
            HttpServletRequest request, HttpServletResponse response) {

        return PathAuthority.SYS_APP + "/sysAppAdd";
    }

    /**
     * 路径添加保存
     *
     * @param appPath
     * @param appOrder
     * @param appName
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "路径添加保存")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String",name = "appidParent", value = "上一级路径"),
            @ApiImplicitParam(dataType = "String",name = "appidParentName", value = "上一级路径名称"),
            @ApiImplicitParam(dataType = "String",name = "appPath", value = "路径地址"),
            @ApiImplicitParam(dataType = "String",name = "appName", value = "路径名称"),
            @ApiImplicitParam(dataType = "Integer",name = "appOrder", value = "路径序号")
    })
    @RequestMapping(value = "/sysAppSave", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Result<EnumResult> sysAppSave(
            @RequestParam(value = "appidParent", defaultValue = "") String appidParent,
            @RequestParam(value = "appidParentName", defaultValue = "") String appidParentName,
            @RequestParam(value = "appPath", defaultValue = "") String appPath,
            @RequestParam(value = "appOrder", defaultValue = "") Integer appOrder,
            @RequestParam(value = "appName", defaultValue = "") String appName,
            HttpServletRequest request, HttpServletResponse response) {
        SysApp sysApp = new SysApp();
        sysApp.setAppPath(appPath);
        sysApp.setAppidParent(appidParent);
        sysApp.setAppidParentName(appidParentName);
        sysApp.setAppOrder(appOrder);
        sysApp.setAppName(appName);
        try {
            int i = sysAppService.saveSysApp(sysApp);
            if (i != 0) {
                return new Result<>(EnumResult.INSERT_OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Result<>(EnumResult.INSERT_NO);
    }


    /**
     * 通过 路径id 获取 对应的全部路径
     *
     * @param appId
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "路径id获取对应的全部路径")
    @ApiImplicitParam(dataType = "String",name = "appId", value = "路径Id")
    @RequestMapping(value = "/findsysAppList", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Result<List<SysApp>> findsysAppList(
            @RequestParam(value = "appId", defaultValue = "") String appId,
            HttpServletRequest request, HttpServletResponse response) {
        try {
            List<SysApp> sysAppList = sysAppService.findSysAppList(appId);
            if (sysAppList != null)
                if (sysAppList.size() > 0)
                    return new Result<>(EnumResult.SELECT_OK, sysAppList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Result<>(500, "路径信息查询失败！");
    }

    /**
     * 编辑信息
     *
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "编辑信息", httpMethod = "POST")
    @ApiImplicitParam(name = "appId", value = "用户id", dataType = "String")
    @RequestMapping(value = "/sysAppEdit", method = {RequestMethod.POST, RequestMethod.GET})
    public String sysAppEdit(
            @RequestParam(value = "appId", defaultValue = "", required = true) String appId,
            HttpServletRequest request, HttpServletResponse response) {

        //路径数据
        SysApp sysApp = sysAppService.findById(appId);
        //全部的角色数据
        List<SysRole> sysRoleList = sysRoleService.findSysRoleList();
        int roleIndex = 0;
        //获取登录 的 角色 数据集 查看是否是管理员
        SysUser loginUser = SysUserUtils.getSysUser(request);
        String administratorsId = sysConfigService.getConfigUserId("role.administrators.id", "0000000000000000000000000000001");
        if (loginUser != null) {
            List<SysUserRole> sysUserRoleList = sysUserRoleService.getAdministrators(loginUser.getUserId(), administratorsId);
            if (sysUserRoleList.size() > 0)
                roleIndex = 1;
        }
        request.setAttribute("sysApp", sysApp);
        request.setAttribute("administratorsId", administratorsId);
        request.setAttribute("roleIndex", roleIndex);
        request.setAttribute("sysRoleList", sysRoleList);
        return PathAuthority.SYS_APP + "/sysAppEdit";
    }

    /**
     * 编辑保存用户基本信息
     *
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "编辑保存路径信息", httpMethod = "POST")
    @ApiImplicitParam(name = "sysApp", value = "实体类", dataType = "sysApp")
    @RequestMapping(value = "/sysAppEditSave", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Result<EnumResult> sysAppEditSave(SysApp sysApp, HttpServletRequest request, HttpServletResponse response) {
        int i = sysAppService.sysAppEditSave(sysApp);
        if (i > 0)
            return new Result<>(EnumResult.UPDATE_OK);
        return new Result(EnumResult.UPDATE_NO);
    }

    /**
     * 逻辑删除用户信息
     *
     * @param appId
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "逻辑删除路径信息", httpMethod = "POST")
    @ApiImplicitParam(name = "appId", value = "路径id", dataType = "String")
    @RequestMapping(value = "sysAppDel")
    @ResponseBody
    public Result<EnumResult> sysAppDel(
            @RequestParam(value = "appId", defaultValue = "", required = true) String appId,
            HttpServletRequest request, HttpServletResponse response) {
        if("00000000000000000000000000TREE1".equals(appId)){
            return new Result(500,"顶级目录不可删除!");
        }
        int i = sysAppService.sysAppDel(appId);
        if (i > 0)
            return new Result(EnumResult.DELETE_OK);

        return new Result(EnumResult.DELETE_NO);
    }

    /**
     * 路径树页面
     *
     * @param request
     * @param response
     * @return
     */
    @ApiIgnore
    @RequestMapping(value = "/sysAppTree", method = {RequestMethod.POST, RequestMethod.GET})
    public String sysAppTree(
            HttpServletRequest request, HttpServletResponse response) {
        return PathAuthority.SYS_APP + "/sysAppTree";
    }

    /**
     * 路径树页面
     *
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "路径树页面", httpMethod = "POST")
    @RequestMapping(value = "/getAppTreeList", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Result<LayuiTree> getAppTreeList(
            HttpServletRequest request, HttpServletResponse response) {
        //获取Tree
        LayuiTree appTreeList = sysAppService.getAppTreeList();
        if (appTreeList != null)
            return new Result<>(EnumResult.SELECT_OK, appTreeList);
        return new Result<>(EnumResult.SELECT_NO);
    }
    /**
     * 当前登录人可查看到的路径树页面
     *
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "当前登录人路径树页面", httpMethod = "POST")
    @RequestMapping(value = "/getUserAppTreeList", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Result<LayuiTree> getUserAppTreeList(
            HttpServletRequest request, HttpServletResponse response) {
        //获取Tree
        LayuiTree appTreeList = sysAppService.getUserAppTreeList(request);
        if (appTreeList != null)
            return new Result<>(EnumResult.SELECT_OK, appTreeList);
        return new Result<>(EnumResult.SELECT_NO);
    }

}
