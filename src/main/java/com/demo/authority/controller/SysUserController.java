package com.demo.authority.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.demo.authority.model.SysRole;
import com.demo.authority.model.SysUser;
import com.demo.authority.model.SysUserRole;
import com.demo.authority.service.SysConfigService;
import com.demo.authority.service.SysRoleService;
import com.demo.authority.service.SysUserRoleService;
import com.demo.authority.service.SysUserService;
import com.demo.authority.utils.PathAuthority;
import com.demo.enums.EnumResult;
import com.demo.utils.QueryPage;
import com.demo.utils.Result;
import com.demo.utils.SysUserUtils;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@Slf4j
@RequestMapping("/authority")
@Api(tags = "SysUserController", value = "用户配置")
public class SysUserController {


    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysUserRoleService sysUserRoleService;
    @Autowired
    private SysRoleService sysRoleService;
    @Autowired
    private SysConfigService sysConfigService;

    /**
     * 用户页面
     *
     * @param request
     * @param response
     * @return
     */
    @ApiIgnore
    @RequestMapping(value = "/sysUserView", method = {RequestMethod.POST, RequestMethod.GET})
    public String sysRoleShow(
            HttpServletRequest request, HttpServletResponse response) {
        String administratorId = sysConfigService.getConfigUserId("role.administrators.id", "0000000000000000000000000000001");
        request.setAttribute("administratorId", administratorId);
        return PathAuthority.SYS_USER + "/sysUserView";
    }

    /**
     * 用户列表页面
     *
     * @param queryPage
     * @param request
     * @param response
     * @return
     */
    @ApiOperation("用户列表页面")
    @ApiImplicitParam(dataType = "QueryPage",name = "queryPage", value = "QueryPage分页实体类",  paramType = "query")
/*    @ApiResponses({
            @ApiResponse(code = 200, message = "查询成功！", response = QueryPage.class),
            @ApiResponse(code = 500, message = "查询失败！", response = Result.class)
    })*/
    @RequestMapping(value = "/sysUserList", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Result<Page<SysUser>> sysUserList(
            @RequestBody QueryPage queryPage,
            HttpServletRequest request, HttpServletResponse response) {
        if (queryPage != null) {
            Page<SysUser> sysUserList = sysUserService.findSysUserList(queryPage);
            return new Result<>(EnumResult.SELECT_OK, sysUserList);
        }
        return new Result<>(EnumResult.SELECT_NO);
    }


    /**
     * 用户添加页面
     *
     * @param request
     * @param response
     * @return
     */
    @ApiIgnore
    @RequestMapping(value = "/sysUserAdd", method = {RequestMethod.POST, RequestMethod.GET})
    public String sysUserAdd(
            HttpServletRequest request, HttpServletResponse response) {
        return PathAuthority.SYS_USER + "/sysUserAdd";
    }

    /**
     * 查看用户基本信息
     *
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "查看用户基本信息", httpMethod = "POST")
    @ApiImplicitParam(dataType = "String",name = "userId", value = "用户id",  paramType = "query")
    @RequestMapping(value = "/sysUserShow", method = {RequestMethod.POST, RequestMethod.GET})
    public String sysUserShow(
            @RequestParam(value = "userId", defaultValue = "", required = true) String userId,
            HttpServletRequest request, HttpServletResponse response) {

        //个人信息数据
        SysUser sysUser = sysUserService.findById(userId);
        request.setAttribute("sysUser", sysUser);
        return PathAuthority.SYS_USER + "/sysUserShow";
    }


    /**
     * 编辑用户基本信息
     *
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "编辑用户基本信息", httpMethod = "POST")
    @ApiImplicitParam(dataType = "String",name = "userId", value = "用户id",   paramType = "query")
    @RequestMapping(value = "/sysUserEdit", method = {RequestMethod.POST, RequestMethod.GET})
    public String sysUserEdit(
            @RequestParam(value = "userId", defaultValue = "", required = true) String userId,
            HttpServletRequest request, HttpServletResponse response) {

        //个人信息数据
        SysUser sysUser = sysUserService.findById(userId);
        //全部的角色数据
        List<SysRole> sysRoleList = sysRoleService.findSysRoleList();
        int roleIndex = 0;
        //获取用户 的 角色 数据集 查看是否是管理员
        SysUser loginUser = SysUserUtils.getSysUser(request);
        String administratorsId = sysConfigService.getConfigUserId("role.administrators.id", "0000000000000000000000000000001");
        if (loginUser != null) {
            List<SysUserRole> sysUserRoleList = sysUserRoleService.getAdministrators(loginUser.getUserId(), administratorsId);
            if (sysUserRoleList.size() > 0)
                roleIndex = 1;
        }
        request.setAttribute("sysUser", sysUser);
        request.setAttribute("administratorsId", administratorsId);
        request.setAttribute("roleIndex", roleIndex);
        request.setAttribute("sysRoleList", sysRoleList);
        return PathAuthority.SYS_USER + "/sysUserEdit";
    }

    /**
     * 编辑保存用户基本信息
     *
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "编辑保存用户基本信息", httpMethod = "POST")
    @ApiImplicitParam(dataType = "SysUser",name = "sysUser", value = "实体类",  paramType = "query")
    @RequestMapping(value = "/sysUserEditSave", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Result<EnumResult> sysUserEditSave(
            SysUser sysUser,
            HttpServletRequest request, HttpServletResponse response) {
        int i = sysUserService.sysUserEditSave(sysUser);
        if (i > 0)
            return new Result<>(EnumResult.UPDATE_OK);
        return new Result(EnumResult.UPDATE_NO);
    }

    /**
     * 逻辑删除用户信息
     *
     * @param userId
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "逻辑删除用户信息", httpMethod = "POST")
    @ApiImplicitParam(dataType = "String",name = "userId", value = "用户id",   paramType = "query")
    @RequestMapping(value = "sysUserDel")
    @ResponseBody
    public Result<EnumResult> sysUserDel(
            @RequestParam(value = "userId", defaultValue = "", required = true) String userId,
            HttpServletRequest request, HttpServletResponse response) {
        if("0000000000000000000000000000001".equals(userId)){
            return new Result(500,"管理员用户不可删除!");
        }
        int i = sysUserService.sysUserDel(userId);
        if (i > 0)
            return new Result(EnumResult.DELETE_OK);

        return new Result(EnumResult.DELETE_NO);
    }


}
