package com.demo.authority.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.demo.authority.model.SysApp;
import com.demo.authority.model.SysRole;
import com.demo.authority.model.SysUser;
import com.demo.authority.model.SysUserRole;
import com.demo.authority.service.SysAppService;
import com.demo.authority.service.SysConfigService;
import com.demo.authority.service.SysRoleService;
import com.demo.authority.service.SysUserRoleService;
import com.demo.authority.utils.PathAuthority;
import com.demo.enums.EnumResult;
import com.demo.utils.QueryPage;
import com.demo.utils.Result;
import com.demo.utils.SysUserUtils;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Controller
@Slf4j
@RequestMapping("/authority")
@Api(tags = "SysRoleController", value = "角色配置")
public class SysRoleController {

    @Autowired
    private SysRoleService srService;
    @Autowired
    private SysAppService sysAppService;

    @Autowired
    private SysUserRoleService sysUserRoleService;
    @Autowired
    private SysConfigService sysConfigService;
    /**
     * 角色页面
     *
     * @param request
     * @param response
     * @return
     */
    @ApiIgnore
    @RequestMapping(value = "/sysRoleView", method = {RequestMethod.POST, RequestMethod.GET})
    public String sysRoleShow(
            HttpServletRequest request, HttpServletResponse response) {
        return PathAuthority.SYS_ROLE + "/sysRoleView";
    }

    /**
     * 角色列表页面
     *
     * @param queryPage
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "角色列表页面")
    @ApiImplicitParam(name = "queryPage", value = "QueryPage分页实体类", dataType = "QueryPage", paramType = "query")
/*    @ApiResponses({
            @ApiResponse(code = 200, message = "查询成功！", response = QueryPage.class),
            @ApiResponse(code = 500, message = "查询失败！", response = Result.class)
    })*/
    @RequestMapping(value = "/sysRoleList", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Result<Page<SysRole>> sysRoleList(
            @RequestBody QueryPage queryPage,
            HttpServletRequest request, HttpServletResponse response) {
        try {
            Page<SysRole> sysRoleList = srService.getSysRoleList(queryPage);
            return new Result<>(EnumResult.SELECT_OK, sysRoleList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Result<>(EnumResult.SELECT_NO);
    }


    /**
     * 角色添加页面
     *
     * @param request
     * @param response
     * @return
     */
    @ApiIgnore
    @RequestMapping(value = "/sysRoleAdd", method = {RequestMethod.POST, RequestMethod.GET})
    public String sysRoleAdd(
            HttpServletRequest request, HttpServletResponse response) {

        return PathAuthority.SYS_ROLE + "/sysRoleAdd";
    }

    /**
     * 角色添加保存
     *
     * @param roleName 角色名
     * @param remarkes 备注
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "角色添加保存")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "roleName", value = "角色名称", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "remarkes", value = "备注", dataType = "String", paramType = "query")
    })
    @RequestMapping(value = "/sysRoleSave", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Result<EnumResult> sysRoleSave(
            @RequestParam(value = "roleName", defaultValue = "") String roleName,
            @RequestParam(value = "remarkes", defaultValue = "") String remarkes,
            HttpServletRequest request, HttpServletResponse response) {
        SysRole sysRole = new SysRole();
        sysRole.setRoleName(roleName);
        sysRole.setRemarkes(remarkes);
        try {
            int i = srService.saveSysRole(sysRole);
            if (i != 0)
                return new Result<>(EnumResult.INSERT_OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Result<>(EnumResult.INSERT_NO);
    }


    /**
     * 通过 用户id 获取 对应的全部角色
     *
     * @param userId
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "用户id获取对应的全部角色")
    @ApiImplicitParam(name = "userId", value = "用户Id", dataType = "String", paramType = "query")
    @RequestMapping(value = "/findSysRoleList", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Result<List<SysRole>> findSysRoleList(
            @RequestParam(value = "userId", defaultValue = "") String userId,
            HttpServletRequest request, HttpServletResponse response) {
        try {
            List<SysRole> sysRoleList = srService.findSysRoleList(userId);
            if (sysRoleList != null)
                if (sysRoleList.size() > 0)
                    return new Result<>(EnumResult.SELECT_OK, sysRoleList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Result<>(500, "未查询角色信息！");
    }

    /**
     * 通过 路径id 获取 对应的全部角色
     *
     * @param appId
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "路径id获取对应的全部角色")
    @ApiImplicitParam(name = "appId", value = "用户Id", dataType = "String", paramType = "query")
    @RequestMapping(value = "/findSysRoleListTwo", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Result<List<SysRole>> findSysRoleListTwo(
            @RequestParam(value = "appId", defaultValue = "") String appId,
            HttpServletRequest request, HttpServletResponse response) {
        try {
            List<SysRole> sysRoleList = srService.findSysRoleListTwo(appId);
            if (sysRoleList != null)
                //if (sysRoleList.size() > 0)
                    return new Result<>(EnumResult.SELECT_OK, sysRoleList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Result<>(500, "当前路径,未查询到角色信息!");
    }


    /**
     * 通过 角色id 获取 对应的全部路径
     *
     * @param roleId
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "角色id获取对应的全部路径")
    @ApiImplicitParam(name = "roleId", value = "角色Id", dataType = "String", paramType = "query")
    @RequestMapping(value = "/findSysAppListTwo", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Result<List<SysApp>> findSysAppListTwo(
            @RequestParam(value = "roleId", defaultValue = "") String roleId,
            HttpServletRequest request, HttpServletResponse response) {
        try {
            List<SysApp> sysAppList = srService.findSysAppListTwo(roleId);
            if (sysAppList != null)
                //if (sysAppList.size() > 0)
                    return new Result<>(EnumResult.SELECT_OK, sysAppList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Result<>(500, "当前角色,未查询到路径信息!");
    }

    /**
     * 查看角色基本信息
     *
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "查看角色基本信息", httpMethod = "POST")
    @ApiImplicitParam(dataType = "String",name = "roleId", value = "角色id",  paramType = "query",required = true)
    @RequestMapping(value = "/sysRoleShow", method = {RequestMethod.POST, RequestMethod.GET})
    public String sysRoleShow(
            @RequestParam(value = "roleId", defaultValue = "", required = true) String roleId,
            HttpServletRequest request, HttpServletResponse response) {
        //角色信息数据
        SysRole sysRole = srService.findById(roleId);
        request.setAttribute("sysRole", sysRole);
        return PathAuthority.SYS_ROLE + "/sysRoleShow";
    }


    /**
     * 编辑角色基本信息
     *
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "编辑角色基本信息", httpMethod = "POST")
    @ApiImplicitParam(dataType = "String",name = "roleId", value = "用户id",   paramType = "query")
    @RequestMapping(value = "/sysRoleEdit", method = {RequestMethod.POST, RequestMethod.GET})
    public String sysUserEdit(
            @RequestParam(value = "roleId", defaultValue = "", required = true) String roleId,
            HttpServletRequest request, HttpServletResponse response) {
        //角色信息数据
        SysRole sysRole = srService.findById(roleId);
        request.setAttribute("sysRole", sysRole);
        //获取全部的路径数据
        List<SysApp> sysAppList = sysAppService.list();
        request.setAttribute("sysAppList", sysAppList);
        int roleIndex = 0;
        //获取用户 的 角色 数据集 查看是否是管理员
        SysUser loginUser = SysUserUtils.getSysUser(request);
        String administratorsId = sysConfigService.getConfigUserId("role.administrators.id", "0000000000000000000000000000001");
        if (loginUser != null) {
            List<SysUserRole> sysUserRoleList = sysUserRoleService.getAdministrators(loginUser.getUserId(), administratorsId);
            if (sysUserRoleList.size() > 0)
                roleIndex = 1;
        }
        request.setAttribute("administratorsId", administratorsId);
        request.setAttribute("roleIndex", roleIndex);
        return PathAuthority.SYS_ROLE + "/sysRoleEdit";
    }


    /**
     * 编辑保存角色基本信息
     *
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "编辑保存角色基本信息", httpMethod = "POST")
    @ApiImplicitParam(dataType = "SysRole",name = "sysRole", value = "实体类",  paramType = "query")
    @RequestMapping(value = "/sysRoleEditSave", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Result<EnumResult> sysRoleEditSave(
            SysRole sysRole,
            HttpServletRequest request, HttpServletResponse response) {
        int i = srService.sysRoleEditSave(sysRole);
        if (i > 0)
            return new Result<>(EnumResult.UPDATE_OK);
        return new Result(EnumResult.UPDATE_NO);
    }

    /**
     * 逻辑删除角色信息
     *
     * @param roleId
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "逻辑删除角色信息", httpMethod = "POST")
    @ApiImplicitParam(dataType = "String",name = "roleId", value = "角色id",   paramType = "query")
    @RequestMapping(value = "sysRoleDel")
    @ResponseBody
    public Result<EnumResult> sysRoleDel(
            @RequestParam(value = "roleId", defaultValue = "", required = true) String roleId,
            HttpServletRequest request, HttpServletResponse response) {
        if("0000000000000000000000000000001".equals(roleId)){
            return new Result(500,"管理员角色不可删除!");
        }
        int i = srService.sysRoleDel(roleId);
        if (i > 0)
            return new Result(EnumResult.DELETE_OK);

        return new Result(EnumResult.DELETE_NO);
    }

}
