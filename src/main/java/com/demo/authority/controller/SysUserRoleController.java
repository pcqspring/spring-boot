package com.demo.authority.controller;


import com.demo.authority.model.SysRole;
import com.demo.authority.model.SysUser;
import com.demo.authority.model.SysUserRole;
import com.demo.authority.service.SysConfigService;
import com.demo.authority.service.SysRoleService;
import com.demo.authority.service.SysUserRoleService;
import com.demo.authority.service.SysUserService;
import com.demo.enums.EnumResult;
import com.demo.utils.Result;
import com.demo.utils.SysUserUtils;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@Slf4j
@RequestMapping("/authority")
@Api(tags = "SysUserRoleController", value = "用户角色中间表操作")
public class SysUserRoleController {

    @Autowired
    private SysUserRoleService surService;

    @Autowired
    private SysUserService suService;
    @Autowired
    private SysRoleService srService;

    @Autowired
    private SysConfigService sysConfigService;

    /**
     * 给用户添加绑定角色
     *
     * @param userId
     * @param roleId
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "给用户添加绑定角色", httpMethod = "POST")
    @RequestMapping(value = "/addSysUserRole", method = {RequestMethod.POST})
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String",name = "userId", value = "用户id" ),
            @ApiImplicitParam(dataType = "String",name = "roleId", value = "角色id" )
    })
    @ResponseBody
    public Result<List<SysRole>> addSysUserRole(
            @RequestParam(value = "userId", defaultValue = "") String userId,
            @RequestParam(value = "roleId", defaultValue = "") String roleId,
            HttpServletRequest request, HttpServletResponse response) {
        if (StringUtils.isNotEmpty(userId) && StringUtils.isNotEmpty(roleId)) {
            SysUserRole sysUserRole = new SysUserRole();
            sysUserRole.setUserId(userId);
            sysUserRole.setRoleId(roleId);
            int i = surService.saveSysUserRole(sysUserRole);
            if (i > 0) {
                List<SysRole> sysRoleList = srService.findSysRoleList(userId);
                return new Result<>(EnumResult.INSERT_OK, sysRoleList);
            }
            if (i == -1)
                return new Result<>(500, "不可以重复添加~！");
        }
        return new Result<>(EnumResult.INSERT_NO);
    }


    /**
     * 删除用户的角色
     *
     * @param userId
     * @param roleId
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "删除用户的角色", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String",name = "userId", value = "用户id"  ),
            @ApiImplicitParam(dataType = "String",name = "roleId", value = "角色id"  )
    })
    @RequestMapping(value = "/delSysUserRole")
    @ResponseBody
    public Result<List<SysRole>> delSysUserRole(
            @RequestParam(value = "userId", defaultValue = "") String userId,
            @RequestParam(value = "roleId", defaultValue = "") String roleId,
            HttpServletRequest request, HttpServletResponse response) {
        if (StringUtils.isNotEmpty(userId) && StringUtils.isNotEmpty(roleId)) {
            SysUserRole sysUserRole = new SysUserRole();
            sysUserRole.setUserId(userId);
            sysUserRole.setRoleId(roleId);
            int i = surService.deleteSysUserRole(sysUserRole);
            if (i > 0) {
                List<SysRole> sysRoleList = srService.findSysRoleList(userId);
                return new Result<>(200, "取消成功", sysRoleList);
            }
        }
        return new Result<>(500, "取消失败");
    }

}
