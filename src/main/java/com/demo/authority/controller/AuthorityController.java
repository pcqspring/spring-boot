package com.demo.authority.controller;

import com.demo.authority.model.SysRole;
import com.demo.authority.model.SysUser;
import com.demo.authority.service.SysConfigService;
import com.demo.authority.service.SysRoleService;
import com.demo.authority.utils.PathAuthority;
import com.demo.utils.SysUserUtils;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@Slf4j
@RequestMapping("/authority")
public class AuthorityController {
 
    @Autowired
    private SysConfigService sysConfigService;

    /**
     * 权限页面
     *
     * @param request
     * @param response
     * @return
     */
    @ApiIgnore
    @RequestMapping(value = "/index", method = {RequestMethod.POST, RequestMethod.GET})
    public String getIndex(
            HttpServletRequest request, HttpServletResponse response) {
        SysUser sysUser = SysUserUtils.getSysUser(request);
        String userName = sysUser.getUserName();
        List<SysRole> sysRoleList = sysUser.getSysRole();
        String administratorsId = sysConfigService.getConfigUserId("role.administrators.id", "0000000000000000000000000000001");
        request.setAttribute("administratorsId", administratorsId);
        request.setAttribute("userName", userName);
        request.setAttribute("sysRoleList", sysRoleList);
        log.info(".................权限页面................");
        return PathAuthority.AUTHORITY + "/index";
    }

}
