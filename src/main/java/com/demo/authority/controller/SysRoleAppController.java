package com.demo.authority.controller;

import com.demo.authority.model.SysApp;
import com.demo.authority.model.SysRole;
import com.demo.authority.model.SysRoleApp;
import com.demo.authority.service.SysAppService;
import com.demo.authority.service.SysRoleAppService;
import com.demo.authority.service.SysRoleService;
import com.demo.enums.EnumResult;
import com.demo.utils.Result;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@Slf4j
@RequestMapping("/authority")
public class SysRoleAppController {
    
    @Autowired
    private SysRoleAppService sraService;

    @Autowired
    private SysAppService saService;
    
    @Autowired
    private SysRoleService srService;
    
    /**
     * 给路径添加绑定角色
     *
     * @param appId
     * @param roleId
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "给路径添加绑定角色", httpMethod = "POST")
    @RequestMapping(value = "/addSysRoleApp", method = {RequestMethod.POST})
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String",name = "appId", value = "路径id", paramType = "query"),
            @ApiImplicitParam(dataType = "String",name = "roleId", value = "角色id", paramType = "query")
    })
    @ResponseBody
    public Result<List<SysRole>> addSysRoleApp(
            @RequestParam(value = "appId", defaultValue = "") String appId,
            @RequestParam(value = "roleId", defaultValue = "") String roleId,
            HttpServletRequest request, HttpServletResponse response) {
        if (StringUtils.isNotEmpty(appId) && StringUtils.isNotEmpty(roleId)) {
            SysRoleApp sysRoleApp = new SysRoleApp();
            sysRoleApp.setAppId(appId);
            sysRoleApp.setRoleId(roleId);
            int i = sraService.saveSysRoleApp(sysRoleApp);
            if (i > 0) {
                List<SysRole> sysRoleList = srService.findSysRoleListTwo(appId);
                return new Result<>(EnumResult.INSERT_OK, sysRoleList);
            }
            if (i == -1) {
                return new Result<>(500, "不可以重复添加~！");
            }
        }
        return new Result<>(EnumResult.INSERT_NO);
    }

    /**
     * 给角色添加绑定路径
     *
     * @param appId
     * @param roleId
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "给角色添加绑定路径", httpMethod = "POST")
    @RequestMapping(value = "/addSysAppRole", method = {RequestMethod.POST})
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String",name = "appId", value = "路径id", paramType = "query"),
            @ApiImplicitParam(dataType = "String",name = "roleId", value = "角色id", paramType = "query")
    })
    @ResponseBody
    public Result<List<SysApp>> addSysAppRole(
            @RequestParam(value = "appId", defaultValue = "") String appId,
            @RequestParam(value = "roleId", defaultValue = "") String roleId,
            HttpServletRequest request, HttpServletResponse response) {
        if (StringUtils.isNotEmpty(appId) && StringUtils.isNotEmpty(roleId)) {
            SysRoleApp sysRoleApp = new SysRoleApp();
            sysRoleApp.setAppId(appId);
            sysRoleApp.setRoleId(roleId);
            int i = sraService.saveSysRoleApp(sysRoleApp);
            if (i > 0) {
                List<SysApp> sysAppList = srService.findSysAppListTwo(roleId);
                return new Result<>(EnumResult.INSERT_OK, sysAppList);
            }
            if (i == -1) {
                return new Result<>(500, "不可以重复添加~！");
            }
        }
        return new Result<>(EnumResult.INSERT_NO);
    }


    /**
     * 删除路径的角色
     *
     * @param appId
     * @param roleId
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "删除路径的角色", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String",name = "appId", value = "路径id"  ),
            @ApiImplicitParam(dataType = "String",name = "roleId", value = "角色id"  )
    })
    @RequestMapping(value = "/delSysRoleApp")
    @ResponseBody
    public Result<List<SysRole>> delSysRoleApp(
            @RequestParam(value = "appId", defaultValue = "") String appId,
            @RequestParam(value = "roleId", defaultValue = "") String roleId,
            HttpServletRequest request, HttpServletResponse response) {
        if (StringUtils.isNotEmpty(appId) && StringUtils.isNotEmpty(roleId)) {
            SysRoleApp sysRoleApp = new SysRoleApp();
            sysRoleApp.setAppId(appId);
            sysRoleApp.setRoleId(roleId);
            int i = sraService.deleteSysRoleApp(sysRoleApp);
            if (i > 0) {
                List<SysRole> sysRoleList = srService.findSysRoleListTwo(appId);
                return new Result<>(200, "取消成功", sysRoleList);
            }
        }
        return new Result<>(500, "取消失败");
    }


    /**
     * 删除角色的路径
     *
     * @param appId
     * @param roleId
     * @param request
     * @param response
     * @return
     */
    @ApiOperation(value = "删除角色的路径", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String",name = "appId", value = "路径id"  ),
            @ApiImplicitParam(dataType = "String",name = "roleId", value = "角色id"  )
    })
    @RequestMapping(value = "/delSysAppRole")
    @ResponseBody
    public Result<List<SysApp>> delSysAppRole(
            @RequestParam(value = "appId", defaultValue = "") String appId,
            @RequestParam(value = "roleId", defaultValue = "") String roleId,
            HttpServletRequest request, HttpServletResponse response) {
        if (StringUtils.isNotEmpty(appId) && StringUtils.isNotEmpty(roleId)) {
            SysRoleApp sysRoleApp = new SysRoleApp();
            sysRoleApp.setAppId(appId);
            sysRoleApp.setRoleId(roleId);
            int i = sraService.deleteSysRoleApp(sysRoleApp);
            if (i > 0) {
                List<SysApp> sysAppList = srService.findSysAppListTwo(roleId);
                return new Result<>(200, "取消成功", sysAppList);
            }
        }
        return new Result<>(500, "取消失败");
    }
}
