/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50731
Source Host           : localhost:3306
Source Database       : springbootdemo

Target Server Type    : MYSQL
Target Server Version : 50731
File Encoding         : 65001

Date: 2022-05-21 21:52:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_app
-- ----------------------------
DROP TABLE IF EXISTS `sys_app`;
CREATE TABLE `sys_app` (
  `APP_ID` varchar(64) NOT NULL COMMENT 'id',
  `APPID_PARENT` varchar(64) DEFAULT NULL COMMENT '上一级app路径节点id',
  `ROLE_ID` varchar(255) DEFAULT NULL COMMENT '角色id',
  `APP_PATH` varchar(255) DEFAULT NULL COMMENT '请求路径',
  `CTIME` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `RSTATE` varchar(2) DEFAULT NULL COMMENT '是否删除  (0使用中,1刪除注銷)',
  PRIMARY KEY (`APP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_app
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `CONFIG_ID` varchar(64) NOT NULL COMMENT 'id',
  `CONFIG_CODE` varchar(255) DEFAULT NULL COMMENT 'code',
  `CONFIG_NAME` varchar(255) DEFAULT NULL COMMENT '名称',
  `CONFIG_VALUE` varchar(255) DEFAULT NULL COMMENT '值',
  `CTRIME` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `REMARKES` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`CONFIG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('role.administrators.id', 'role.administrators.id', '管理员角色id', '0000000000000000000000000000001', '2022-05-20 22:34:23', '管理员权限');
INSERT INTO `sys_config` VALUES ('role.ordinary.id', 'role.ordinary.id', '普通用户角色id', '0000000000000000000000000000002', '2022-05-20 22:32:23', '创建用户默认为普通用户');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `ROLE_ID` varchar(64) NOT NULL COMMENT '角色id',
  `ROLENAME` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `CTIME` varchar(100) DEFAULT NULL COMMENT '创建时间',
  `RSTATE` varchar(10) DEFAULT NULL COMMENT '是否删除  (0使用中,1刪除注銷)',
  `REMARKES` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('0000000000000000000000000000001', '管理员', '2022-05-14 20:12:00', '0', '管理员');
INSERT INTO `sys_role` VALUES ('0000000000000000000000000000002', '普通用户', '2022-05-14 20:14:00', '0', '仅使用该系统');
INSERT INTO `sys_role` VALUES ('BFA7CE01FFFFFFFFDFFFB2A200000001', '程序员', '2022-05-20 13:46:23', '0', '测试写代码');
INSERT INTO `sys_role` VALUES ('BFA7CE01FFFFFFFFE00255B500000002', '架构师', '2022-05-20 13:48:10', '0', '搭建项目配置环境');
INSERT INTO `sys_role` VALUES ('BFA7CE01FFFFFFFFE002ED6E00000006', '老板', '2022-05-20 13:49:55', '0', '数钱');

-- ----------------------------
-- Table structure for sys_role_app
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_app`;
CREATE TABLE `sys_role_app` (
  `ROLE_ID` varchar(64) NOT NULL COMMENT '角色id',
  `APP_ID` varchar(64) DEFAULT NULL COMMENT '路径页面id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_role_app
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `USER_ID` varchar(64) NOT NULL COMMENT 'id',
  `USER_NAME` varchar(255) DEFAULT NULL COMMENT '姓名',
  `SEX` varchar(10) DEFAULT NULL COMMENT '性别',
  `ADDRESS` varchar(500) DEFAULT NULL COMMENT '地址',
  `PHONE` varchar(255) DEFAULT NULL COMMENT '电话',
  `LOGIN_NAME` varchar(50) DEFAULT NULL COMMENT '用户账号',
  `PASSWORD` varchar(50) DEFAULT NULL COMMENT '用户密码',
  `ROLE_ID` varchar(64) DEFAULT NULL COMMENT '角色id',
  `ROLE_NAME` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `CTIME` varchar(100) DEFAULT NULL COMMENT '创建时间',
  `RSTATE` varchar(255) DEFAULT NULL COMMENT '是否注销  (0使用中,1刪除注銷)',
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('0000000000000000000000000000001', '管理员', '0', '数据库', '110', 'admin', 'admin', null, null, '2022-05-21 21:30:36', '0');
INSERT INTO `sys_user` VALUES ('BFA7CE01FFFFFFFFE0FD129000000001', '彭朝群', '0', '河北省保定市曲阳县燕赵村燕赵镇', '15033739120', 'pcq', 'asd12300', null, null, '2022-05-20 23:01:40', '0');
INSERT INTO `sys_user` VALUES ('BFA7CE01FFFFFFFFE2227AF100000003', '王宇', '0', '河北省石家庄市桥西区人民学府小区', '10086', 'wy', '123456', null, null, '2022-05-20 23:42:32', '0');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `USER_ID` varchar(64) NOT NULL COMMENT '用户id',
  `ROLE_ID` varchar(64) NOT NULL COMMENT '角色id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('BFA7CE01FFFFFFFFE2227AF100000003', '0000000000000000000000000000002');
INSERT INTO `sys_user_role` VALUES ('BFA7CE01FFFFFFFFE0FD129000000001', '0000000000000000000000000000002');
INSERT INTO `sys_user_role` VALUES ('0000000000000000000000000000001', '0000000000000000000000000000002');
INSERT INTO `sys_user_role` VALUES ('0000000000000000000000000000001', '0000000000000000000000000000001');
